<?php

namespace Common\Model;

abstract class RelationModel extends BasicModel {

    protected $tablePrefix = 'pay_';

    protected function _initialize() {
        parent::_initialize();
    }
    
    protected $_validate = array(
    		array('relation_contract_id','require','【合同编号】不能为空',self::MUST_VALIDATE,'both'),
    );

    protected $_auto = array();
    
    protected $_scope = array();
    
    protected $_map = array(
        'id' => 'relation_id',
        'contract_id' => 'relation_contract_id',
        'neturl' => 'relation_neturl',
    	'scenes' => 'relation_scenes',
        'status' => 'relation_status',
    );
    
    protected $_link = array();

}
