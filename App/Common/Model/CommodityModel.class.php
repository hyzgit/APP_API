<?php

namespace Common\Model;

abstract class CommodityModel extends BasicModel
{
	protected $tablePrefix = 'pay_';

	protected function _initialize()
	{
		parent::_initialize();
	}

	protected $_validate = array(
			array('commodity_commodity_type','require','【商品类型】不能为空',self::MUST_VALIDATE,'both'),
			array('commodity_stages_num','require','【分期数】不能为空',self::MUST_VALIDATE,'both'),
			array('commodity_stages_num','number','【分期数】必须为数值',self::MUST_VALIDATE,'both'),
			array('commodity_total_money','require','【商品总金额】不能为空',self::MUST_VALIDATE,'both'),
			array('commodity_total_money', 'currency','【商品总金额】输入格式有误', self::MUST_VALIDATE,'both'),
			array('commodity_down_payment','require','【首付值】不能为空',self::MUST_VALIDATE,'both'),
			array('commodity_down_payment', 'currency','【首付值】输入格式有误', self::MUST_VALIDATE,'both'),
	);

	protected $_auto = array();

	protected $_scope = array();

	protected $_map = array(
			'id' =>'commodity_id',
			'commodity_type'=>'commodity_commodity_type',
			'total_money'=>'commodity_total_money',
			'stages_num'=>'commodity_stages_num',
			'down_payment' => 'commodity_down_payment',
			'reduce_price' => 'commodity_reduce_price',
			'order'=>'commodity_order',
			'status'=>'commodity_status',
	);

	protected $_link = array();
}