<?php

namespace Common\Model;

abstract class CancelModel extends BasicModel {

    protected $tablePrefix = 'pay_';

    protected function _initialize() {
        parent::_initialize();
    }

    protected $_auto = array();
    protected $_scope = array();
    protected $_map = array(
        'id' => 'cancel_id',
		'contract_id' => 'cancel_contract_id',
		'certificate_card' => 'cancel_certificate_card',
		'name' => 'cancel_name',
		'bank_name' => 'cancel_bank_name',
		'loan_bank_number' => 'cancel_loan_bank_number',
		'mobile_number' => 'cancel_mobile_number',
		'match_status' => 'cancel_match_status',
		'contract_status' => 'cancel_contract_status',
		'customer_remark' => 'cancel_customer_remark',
		'remark' => 'cancel_remark',
		'time' => 'cancel_time',
		'audit_time' => 'cancel_audit_time',
		'status' => 'cancel_status',
    	'state' => 'cancel_state',
    );
    protected $_link = array();

}
