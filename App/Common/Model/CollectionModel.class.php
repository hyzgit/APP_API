<?php
namespace Common\Model;

abstract class CollectionModel extends BasicModel {

	protected $tablePrefix = 'edition_';

	protected function _initialize() {
		parent::_initialize();
	}

	protected $_auto = array();
	protected $_scope = array();

	protected $_validate = array(
			array('collection_mode','require','【类型】不能为空',self::MUST_VALIDATE,'both'),
			array('collection_key','require','【标识】不能为空',self::MUST_VALIDATE,'both'),
	);

	protected $_map = array(
			'id' => 'collection_id',
			'mode' => 'collection_mode',
			'key' => 'collection_key',
			'data' => 'collection_data',
			'time' => 'collection_time',
	);
	protected $_link = array();

}
