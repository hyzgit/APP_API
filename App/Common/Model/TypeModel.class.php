<?php

namespace Common\Model;

abstract class TypeModel extends BasicModel
{
	protected $tablePrefix = 'edition_';

	protected function _initialize()
	{
		parent::_initialize();
	}

	protected $_validate = array(
			array('type_name','require','【名称】不能为空',self::MUST_VALIDATE),
			array('type_state','require','【状态】不能为空',self::MUST_VALIDATE),
			array('type_order','require','【排序】不能为空',self::MUST_VALIDATE),
	);

	protected $_auto = array();

	protected $_scope = array();

	protected $_map = array(
			'id' => 'type_id',
			'name' => 'type_name',
			'desc' => 'type_desc',
			'state' => 'type_state',
			'order' => 'type_order',
	);

	protected $_link = array();
}