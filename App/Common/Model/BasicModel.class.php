<?php
namespace Common\Model;

use Think\Model\RelationModel;

abstract class BasicModel extends RelationModel
{
	protected function _initialize()
	{}
	
	protected $connection;
	
	protected $_validate = array();
	
	protected $_auto = array();
	
	protected $_scope = array();
	
	protected $_map = array();
	
	protected $_link = array();
	
	/**
	 * 强化排序支持
	 * @see \Think\Model::__call
	 * @param string $order
	 * @return \Think\Model
	 */
	final public function order($order = '')
	{
		parent::order($order);
		
		return $this;
	}
	
	/**
	 * 更新：包括新增及修改[以主键为判断依据]
	 * @param array $data
	 * @param array $options
	 * @param boolean $replace
	 * @return boolean|mixed
	 */
	final protected function updateRecord($data = array(),$options = array(),$replace = false)
	{
		if( $data === false )
		{
			return false;
		}
		
		if( array_key_exists($this->getPk(),$data) === false )
		{
			$result = $this->add($data,$options,$replace);
		}
		else
		{
		    $result = $this->save($data,$options);
		}
		
		if( $result === 0 )
		{
			$this->error = '没有更新任何记录';
			
			return false;
		}
		
		return $result === false ? false : $result ;
	}
	
	/**
	 * 修改操作[以条件为判断依据]
	 * @param array $where
	 * @param array $data
	 * @return boolean|mixed
	 */
	final protected function updateRecordByParm($where = array(),$data = array())
	{
		if( $data === false )
		{
			return false;
		}
		
		return $this->where($where)->save($data);
	}
	
	/**
	 * 物理删除
	 * @param array $options
	 * @return boolean|mixed
	 */
	private function deleteRecordReal($options = array())
	{
		$data = $this->delete($options);
		
		if( $data === 0 )
		{
			$this->error = '没有删除任何记录';
			
			return false;
		}
		
		return $data;
	}
	
	/**
	 * 逻辑删除
	 * @param array $options
	 * @param array $data
	 * @return boolean|mixed
	 */
	private function deleteRecordLogic($options = array(),$data = array())
	{
		if(is_numeric($options) || is_string($options))
		{
			$options = array(
					'where' => array(
							$this->getPk() => strpos($options,',') ? array('IN', $options) : $options,
					),
			);
		}
		
		return $this->save($data,$options);
	}
	
	/**
	 * 删除：包括物理删除、逻辑删除
	 * @param array|number $options
	 * @param boolean $real
	 * @param array $data
	 * @return boolean|mixed
	 */
	final protected function deleteRecord($options = array(),$real = true,$data = array())
	{
		return $real ? $this->deleteRecordReal($options) : $this->deleteRecordLogic($options,$data);
	}
	
	/**
	 * 获取单挑记录，如：$this->where()->order()->join()->getItem();
	 * 作用：替换find()方法，提供后期可扩展性
	 * @param unknown $options
	 * @return mixed
	 */
	final protected function getItem($options = array())
	{
		return $this->find($options);
	}
	
	/**
	 * 获取全部记录，不分页，如：$this->where()->order()->join()->getItems();
	 * 作用：替换select()方法，提供后期可扩展性
	 * @param unknown $options
	 * @return mixed
	 */
	final protected function getItems($options = array())
	{
		return $this->select($options);
	}
	
	/**
	 * 公用获取多行记录且支持分页
	 * @param number $totalRows
	 * @param string $pageShow
	 * @param number $pageSize
	 * @param array $parameter
	 * @return mixed
	 */
	final protected function pagination($totalRows,&$pageShow = null,$pageSize = 10,$parameter=array())
	{
	    return $this->limit(get_page($totalRows,$pageShow,$pageSize,$parameter));
	}
	
	//LOCATE(",%s,",contract_partition)
}