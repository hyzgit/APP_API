<?php
namespace Common\Model;

abstract class LevelModel extends BasicModel
{
	protected $tablePrefix = 'core_';
	
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	protected $_validate = array(
			// 新增
			array('level_pid','require','【所属分类】不能为空',self::MUST_VALIDATE,'regex',self::MODEL_INSERT),
			// 新增、修改
			array('level_name','require','【标题】不能为空',self::MUST_VALIDATE,'regex',self::MODEL_BOTH),
	);
	
	protected $_auto = array();
	
	protected $_scope = array();
	
	protected $_map = array(
			'id' => 'level_id',
			'pid' => 'level_pid',
			'name' => 'level_name',
			'data' => 'level_data',
			'desc' => 'level_desc',
			'default' => 'level_default',
			'order' => 'level_order',
			'state' => 'level_state',
	);
	
	protected $_link = array();
}