<?php

namespace Common\Model;

abstract class RefundModel extends BasicModel {

    protected $tablePrefix = 'pay_';

    protected function _initialize() {
        parent::_initialize();
    }

    protected $_auto = array();
    protected $_scope = array();
    protected $_map = array(
    		'id' => 'refund_id',
    		'contract_id' => 'refund_contract_id',
    		'certificate_card' => 'refund_certificate_card',
    		'name' => 'refund_name',
    		'mobile_number' => 'refund_mobile_number',
    		'bank_name' => 'refund_bank_name',
    		'loan_bank_number' => 'refund_loan_bank_number',
    		'amount' => 'refund_amount',
    		'contract_status' => 'refund_contract_status',
    		'remark' => 'refund_remark',
    		'customer_remark' => 'refund_customer_remark',
    		'time' => 'refund_time',
    		'audit_time' => 'refund_audit_time',
    		'complete_time' => 'refund_complete_time',
    		'status' => 'refund_status',
    		'state' => 'refund_state',
    );
    protected $_link = array();

}
