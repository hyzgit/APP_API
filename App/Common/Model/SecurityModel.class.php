<?php

namespace Common\Model;

abstract class SecurityModel extends BasicModel {

    protected $tablePrefix = 'edition_';
    Protected $autoCheckFields = false;

    protected function _initialize() {
        parent::_initialize();
    }

    protected $_validate = array();
    protected $_auto = array();
    protected $_scope = array();
    protected $_map = array();
    protected $_link = array();
}
