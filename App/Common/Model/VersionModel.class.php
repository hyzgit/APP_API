<?php

namespace Common\Model;

abstract class VersionModel extends BasicModel
{
	protected $tablePrefix = 'edition_';

	protected function _initialize()
	{
		parent::_initialize();
	}

	protected $_validate = array(
			array('version_type','require','【业务类型】不能为空',self::MUST_VALIDATE),
			array('version_release','require','【版本号】不能为空',self::MUST_VALIDATE),
			array('version_code','require','【内部版本】不能为空',self::MUST_VALIDATE),
			array('version_key','require','【密钥】不能为空',self::MUST_VALIDATE),
			array('version_message','0,20','【提示信息】长度少于20个字符',self::MUST_VALIDATE,'length',self:: MODEL_BOTH),
			array('version_description','0,200','【描述】长度少于200个字符',self::MUST_VALIDATE,'length',self:: MODEL_BOTH),
			array('version_platform','require','【设备名称】不能为空',self::MUST_VALIDATE),
	);

	protected $_auto = array();

	protected $_scope = array();

	protected $_map = array(
			'id' => 'version_id',
			'name' => 'version_name',
			'release' => 'version_release',
			'code' => 'version_code',
			'key'=>'version_key',
			'download' => 'version_download',
			'message' => 'version_message',
			'description' => 'version_description',
			'platform' => 'version_platform',
			'type_id' => 'version_type_id',
			'type'=>'version_type',
			'notice'=>'version_notice',
			'status'=>'version_status'
	);

	protected $_link = array();
}