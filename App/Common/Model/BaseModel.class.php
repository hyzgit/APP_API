<?php
namespace Common\Model;

abstract class BaseModel extends ConstantModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	protected $_validate = array();
	
	protected $_auto = array();
	
	protected $_scope = array();
	
	protected $_map = array();
	
	protected $_link = array();
}