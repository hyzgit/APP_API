<?php

namespace Common\Model;

abstract class PaymentModel extends BasicModel {

    protected $tablePrefix = 'edition_';

    protected function _initialize() {
        parent::_initialize();
    }

    protected $_validate = array(
        array('payment_loan_type', 'require', '【贷款类型】必须选择', self::MUST_VALIDATE),
        array('payment_commodity_type', 'require', '【商品分类】必须选择', self::MUST_VALIDATE),
        array('payment_value', 'require', '【首付值】必填', self::MUST_VALIDATE),
        array('payment_stages', 'require', '【分期数】必须选择', self::MUST_VALIDATE),
        array('payment_type', 'require', '【首付类型】必须选择', self::MUST_VALIDATE),
        array('payment_type,payment_value', 'checkPaymentValue', '【首付值】关闭时必须为0，按比例必须大于0，小于1，固定额度必须为数字', self::MUST_VALIDATE, 'callback'),
    );
    protected $_auto = array();
    protected $_scope = array();
    protected $_map = array(
        'id' => 'payment_id',
        'loan_type' => 'payment_loan_type',
        'commodity_type' => 'payment_commodity_type',
        'stages' => 'payment_stages',
        'type' => 'payment_type',
        'value' => 'payment_value',
        'data' => 'payment_data',
        'order' => 'payment_order',
        'status' => 'payment_status',
    );
    protected $_link = array();

    protected function checkPaymentValue($value) {
        switch ($value['payment_type']) {
            case 'SCALE':
                return is_numeric($value['payment_value']) && $value['payment_value'] > 0 && $value['payment_value'] < 1;
                break;
            case 'QUOTA':
                return is_numeric($value['payment_value']) && $value['payment_value'] > 0;
                break;
            default:
                return is_numeric($value['payment_value']) && $value['payment_value'] == 0;
        }
    }

}
