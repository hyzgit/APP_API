<?php
namespace Common\Model;

abstract class RoleModel extends BasicModel
{
	protected $tablePrefix = 'core_';
	
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	protected $_validate = array(
			array('role_name','require','【标题】不能为空',self::MUST_VALIDATE),
			array('role_raw','require','【权限】至少选择一项',self::MUST_VALIDATE),
	);
	
	protected $_auto = array();
	
	protected $_scope = array();
	
	protected $_map = array(
			'id' => 'role_id',
			'name' => 'role_name',
			'desc' => 'role_desc',
			'order' => 'role_order',
			'raw' => 'role_raw',
			'data' => 'role_data',
			'state' => 'role_state',
	);
	
	protected $_link = array();
}