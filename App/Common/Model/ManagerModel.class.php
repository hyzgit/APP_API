<?php
namespace Common\Model;

abstract class ManagerModel extends BasicModel
{
	protected $tablePrefix = 'core_';
	
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	protected $_validate = array(
			// 新增
			array('manager_usernick','require','【昵称】不能为空',self::MUST_VALIDATE,'regex',self::MODEL_INSERT),
			array('manager_username','require','【用户名】不能为空',self::MUST_VALIDATE,'regex',self::MODEL_INSERT),
			array('manager_username','4,30','【用户名】长度必须为4-30',self::MUST_VALIDATE,'length',self::MODEL_INSERT),
			array('manager_username','','【用户名】已存在',self::MUST_VALIDATE,'unique',self::MODEL_INSERT),
			array('manager_userpass','require','【密码】不能为空',self::MUST_VALIDATE,'regex',self::MODEL_INSERT),
			array('manager_userpass','4,30','【密码】长度必须为4-40',self::MUST_VALIDATE,'length',self::MODEL_INSERT),
			array('password','require','【确认密码】不能为空',self::MUST_VALIDATE,'regex',self::MODEL_INSERT),
			array('password','manager_userpass','两次密码不相同',self::MUST_VALIDATE,'confirm',self::MODEL_INSERT),

			// 重置密码
			array('manager_id','require','系统错误',self::MUST_VALIDATE,'regex','resetpassword'),
			array('manager_userpass','require','【密码】不能为空',self::MUST_VALIDATE,'regex','resetpassword'),
			array('manager_userpass','4,30','【密码】长度必须为4-40',self::MUST_VALIDATE,'length','resetpassword'),
			array('password','manager_userpass','两次密码不相同',self::MUST_VALIDATE,'confirm','resetpassword'),
			
			// 修改密码密码
			array('manager_id','require','系统错误',self::MUST_VALIDATE,'regex','password'),
			array('manager_userpass','require','【密码】不能为空',self::MUST_VALIDATE,'regex','password'),
			array('manager_userpass','4,30','【密码】长度必须为4-40',self::MUST_VALIDATE,'length','password'),
			array('password','manager_userpass','两次密码不相同',self::MUST_VALIDATE,'confirm','password'),
	);
	
	protected $_auto = array();
	
	protected $_scope = array();
	
	protected $_map = array(
			'id' => 'manager_id',
			'usernick' => 'manager_usernick',
			'username' => 'manager_username',
			'userpass' => 'manager_userpass',
			'usersalt' => 'manager_usersalt',
			'last_ip' => 'manager_last_ip',
			'last_time' => 'manager_last_time',
			'register_ip' => 'manager_register_ip',
			'register_time' => 'manager_register_time',
			'role_id' => 'manager_role_id',
			'role_name' => 'manager_role_name',
			'role_raw' => 'manager_role_raw',
			'role_data' => 'manager_role_data',
			'state' => 'manager_state',
	);
	
	protected $_link = array();
}