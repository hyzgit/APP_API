<?php
namespace Common\Model;

abstract class LocationModel extends BasicModel {

    protected $tablePrefix = 'edition_';

    protected function _initialize() {
        parent::_initialize();
    }

    protected $_auto = array();
    protected $_scope = array();
    
	protected $_validate = array(
    		array('location_store_code','require','【商户编号】不能为空',self::MUST_VALIDATE,'both'),
    		array('location_store_name','require','【商户名称】不能为空',self::MUST_VALIDATE,'both'),
    		array('location_longitude','require','【经度】不能为空',self::MUST_VALIDATE,'both'),
    		array('location_latitude','require','【纬度】不能为空',self::MUST_VALIDATE,'both'),
    );
    
    protected $_map = array(
        'id' => 'location_id',
		'store_code' => 'location_store_code',
		'store_name' => 'location_store_name',
		'longitude' => 'location_longitude',
		'latitude' => 'location_latitude',
		'definition' => 'location_definition',
		'lock' => 'location_lock',
		'state' => 'location_state',
    );
    protected $_link = array();

}
