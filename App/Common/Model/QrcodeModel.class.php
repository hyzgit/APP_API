<?php
namespace Common\Model;

abstract class QrcodeModel extends BasicModel
{
	protected $tablePrefix = 'edition_';
	
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	protected $_validate = array(
			array('qrcode_storecode','require','【商铺编码】不能为空',self::MUST_VALIDATE),
			
			array('qrcode_storename','require','【商铺名称】不能为空',self::MUST_VALIDATE),
			
			array('qrcode_username','require','【业务员姓名】不能为空',self::MUST_VALIDATE),
	);
	
	protected $_auto = array();
	
	protected $_scope = array();
	
	protected $_map = array(
			'id' => 'qrcode_id',
			'name' => 'qrcode_name',
			'storecode' => 'qrcode_storecode',
			'username' => 'qrcode_username',
			'storename' => 'qrcode_storename',
			'commoditybrand' => 'qrcode_commoditybrand',
			'commoditymodel' => 'qrcode_commoditymodel',
			'commodityprice' => 'qrcode_commodityprice',
			'downpayment' => 'qrcode_downpayment',
			'loantype' => 'qrcode_loantype',
			'commoditytype' => 'qrcode_commoditytype',
			'status' => 'qrcode_status',			
	);
	
	protected $_link = array();
}