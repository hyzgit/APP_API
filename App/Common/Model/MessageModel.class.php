<?php

namespace Common\Model;

abstract class MessageModel extends BasicModel
{
	protected $tablePrefix = 'edition_';

	protected function _initialize()
	{
		parent::_initialize();
	}

	protected $_validate = array(
    );
	
	protected $_auto = array();
	
	protected $_scope = array();
	
	protected $_map = array(
			'id' => 'message_id',
			'mobile' => 'message_mobile',
			'template' => 'message_template',
			'content' => 'message_content',
			'raw' => 'message_raw',
			'state' => 'message_state',
			'time' => 'message_time',
			'create_time' => 'message_create_time',
	);
	
	protected $_link = array();

}
