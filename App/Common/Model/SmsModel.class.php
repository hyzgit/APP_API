<?php

namespace Common\Model;

abstract class SmsModel extends BasicModel
{

    protected $tablePrefix = 'edition_';

    protected function _initialize()
    {
        parent::_initialize();
    }

    protected $_validate = array();
    
    protected $_auto = array();
    
    protected $_scope = array();
    
    protected $_map = array(
			'id' => 'sms_id',
			'report_id' => 'sms_report_id',
			'service_number' => 'sms_service_number',
			'mobile' => 'sms_mobile',
			'code' => 'sms_code',
			'state' => 'sms_state',
			'time' => 'sms_time',
			'create_time' => 'sms_create_time',
	);
    
    protected $_link = array();

}
