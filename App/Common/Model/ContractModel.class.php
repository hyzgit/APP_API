<?php

namespace Common\Model;

abstract class ContractModel extends BasicModel {

	protected $tablePrefix = 'pay_';

	protected function _initialize() {
		parent::_initialize();
	}

	protected $_auto = array();
	protected $_scope = array();
	
	protected $_validate = array(
			array('contract_number','require','【合同编号】不能为空',self::EXISTS_VALIDATE),
	);
	
	protected $_map = array(
		'id' => 'contract_id',
		'number' => 'contract_number',
		'amount' => 'contract_amount',
		'total_money' => 'contract_total_money',
		'time' => 'contract_time',
		'name' => 'contract_name',
		'cardid' => 'contract_cardid',
		'loan_type' => 'contract_loan_type',
		'commodity_type' => 'contract_commodity_type',
		'brand' => 'contract_brand',
		'type' => 'contract_type',
		'down_pay' => 'contract_down_pay',
		'transaction_id' => 'contract_transaction_id',
		'transaction_raw' => 'contract_transaction_raw',
		'status' => 'contract_status',
	);
	protected $_link = array();

}
