<?php

namespace Common\Model;

abstract class BankModel extends BasicModel {

    protected $tablePrefix = 'edition_';

    protected function _initialize() {
        parent::_initialize();
    }
    
    protected $_validate = array(
    		array('bank_channel','require','【渠道】不能为空',self::MUST_VALIDATE),
    		array('bank_title','require','【银行名称】不能为空',self::MUST_VALIDATE),
    		array('bank_code','require','【银行编号】不能为空',self::MUST_VALIDATE),
    );

    protected $_auto = array();
    
    protected $_scope = array();
    
    protected $_map = array(
        'id' => 'bank_id',
        'channel' => 'bank_channel',
        'title' => 'bank_title',
    	'code' => 'bank_code',
        'order' => 'bank_order',
    	'state' => 'bank_state',
    );
    
    protected $_link = array();

}
