<?php
// 扩展配置
return array(
		'file' => array(
				'config' => array(
						'rootPath' => '../upload',
						'savePath'=>'/upload/',
						'saveName'=>array('uniqid',''),
						'autoSub'=>true,
						'subName'=>array('date','Ymd'),
						'exts'=>array('jpg', 'gif', 'png', 'jpeg'),
				),
				'driver' => null,
				'driverConfig' => null,
				'domain' => 'http://image.loan.me.yunduanchongqing.com',
		),
		'umeditor' => array(
				'config' => array(
						'rootPath' => '../upload',
						'savePath'=>'/upload/',
						'saveName'=>array('uniqid',''),
						'autoSub'=>true,
						'subName'=>array('date','Ymd'),
						'exts'=>array('jpg', 'gif', 'png', 'jpeg'),
				),
				'driver' => null,
				'driverConfig' => null,
				'domain' => 'http://image.loan.me.yunduanchongqing.com',
		),
);