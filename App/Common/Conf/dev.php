<?php
// 状态配置
return array(
		// 加载扩展配置文件
		'LOAD_EXT_CONFIG' => array(
				'config.custom',
				'dev/config.db',
				'config.domain',
				'config.global',
				'config.rule',
				'dev/config.upload',
				'dev/dev',
		),
);