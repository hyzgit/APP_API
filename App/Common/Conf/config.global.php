<?php
// 扩展配置
return array(
		// 设置项目URL地址不区分大小写
		'URL_CASE_INSENSITIVE' => false,
		// 检查文件的大小写仅对Windows平台有效
		'APP_FILE_CASE' => true,
		
		// 设置URL模式为重写模式
		'URL_MODEL' => '2',
		
		// 操作方法后缀
		'ACTION_SUFFIX' => 'Action',
		
		// 分页
		'VAR_PAGE' => 'page',
		// 伪静态后缀
		'URL_HTML_SUFFIX' => 'do',
		// 默认参数过滤方法
		'DEFAULT_FILTER' => 'trim',
		
		'TMPL_DENY_ACCESS' => 'Common@Access/deny',
		
		'TMPL_ACTION_ERROR' => 'Common@Access/error',
		
		'TMPL_ACTION_SUCCESS' => 'Common@Access/success',
);