<?php
// 扩展配置
return array(
		// 数据库类型
		'DB_TYPE' => 'mysql',
		// 服务器地址
		'DB_HOST' => 'localhost',
		// 端口
		'DB_PORT' => 3306,
		// 用户名
		'DB_USER' => 'root',
		// 密码
		'DB_PWD' => 'root',
		// 数据库名
		'DB_NAME' => 'edition',
		// 数据库表前缀
		'DB_PREFIX' => 'edition_',
);