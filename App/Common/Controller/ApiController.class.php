<?php
namespace Common\Controller;

use Think\Controller\RestController;

abstract class ApiController extends RestController
{
	protected function _initialize()
	{}
}