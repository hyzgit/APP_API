<?php
/**
 * 生成随机数
 * @param number $length
 * @return number
 */
function generate_number($length = 6)
{
	return rand(pow(10,($length-1)), pow(10,$length)-1);
}

/**
 * 生成随机字符串
 * @param number $length
 * @param string $chars
 * @return string
 */
function generate_string($length = 6,$chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
{
	$chars = str_split($chars);
	
	$chars = array_map(function($i) use($chars){return $chars[$i];},array_rand($chars,$length));
	
	return implode($chars);
}
/**
 * 生成数字订单编号
 * @return string
 */
function generate_number_order()
{
	return date('YmdHis') . mt_rand(10000000,99999999);
}

/**
 * 密码生成
 * @param unknown $input
 * @param string $salt
 * @param string $algo
 */
function password($input,$salt = null,$algo = 'md5')
{
	return $algo($algo($input).$salt);
}
/**
 * 密码验证
 * @param string $input
 * @param string $password
 * @param string $salt
 * @param string $algo
 * @return boolean
 */
function password_check($input,$password,$salt = null,$algo = 'md5')
{
	return $algo($algo($input).$salt)==$password;
}
/**
 * 数组为空根据关联进行拷贝
 * @param array $data
 * @param array $map
 * @return array
 */
function completion($data = array(),$map = array())
{
	foreach ($map as $key => $value)
	{
		if( $data[$key] === "" )
		{
			$data[$key] = $map[$key];
		}
	}
	
	return $data;
}

/**
 * 签名方法
 * @param array $data 代签名数组
 * @param string $salt 处理机制
 * @return string
 */
function get_sign($data = array(),$salt = 'md5,strtolower')
{
	ksort($data);
	
	$data = http_build_query($data);
	
	foreach (explode(',', $salt) as $item)
	{
	    $data = $item($data);
	}
	
	return $data;
}
/**
 * 屏蔽显示字符串,支持电话和邮件的隐藏
 * @param string $input
 * @param number $before
 * @param number $after
 * @param string $type tel,mail,text...
 * @param string $shield
 * @return unknown|Ambigous <string, mixed>
 */
function hidden($input,$before = 4,$after = 3,$type = 'tel|text',$shield = '*')
{
    if(empty($input)){return $input;}
    // 邮件特殊处理
    if($type == 'mail')
    {
        list($input,$suffix) = explode('@', $input);
    }
    // 解决长度不足问题
    if(strlen($input) <= $before + $after)
    {
        $input = substr($input, 0, $before-1) . $shield . substr($input, -$after);
    }
    
    // 需要显示的隐藏元素个数
    $len = strlen($input) - $before - $after;
    
    $input = substr_replace($input, str_repeat($shield, $len), $before, $len);
    
    return empty($suffix) ? $input : $input . '@' . $suffix;
}
/**
 * 多维数组合并为一维数字
 * @param unknown $multi
 * @param string $prefix
 * @return multitype:unknown
 */
function multi2single($multi = array(),$prefix = '')
{
	static $result = array();
	
	foreach($multi as $key => $item)
	{
		if( is_array($item) )
		{
			multi2single($item,$prefix.$key);
		}else{
			$result[$prefix.$key]=$item;
		}
	}
	
	return $result;
}