<?php
/**
 * 获取静态资源
 * @param string $file
 * @return string
 */
function H($file = '')
{
	$domain = C('assets.domain');
	
	$domain = is_array($domain) ? $domain[array_rand($domain)] : sprintf('%s://%s/',I('server.REQUEST_SCHEME'),I('server.HTTP_HOST')) ;
	
	return sprintf('%s%s',$domain,$file);
}
/**
 * 长会话管理函数
 * @param string|array $name
 * @param mixed $value
 * @return mixed
 */
function psession($name = '', $value = '')
{
	return session($name,$value);
}
/**
 * 后台管理界面左侧导航激活状态判定
 * @param string $controllers
 * @return string
 */
function active($controllers='')
{
	$controllers = explode(',', strtolower($controllers));
	
	$controller = sprintf('%s/',strtolower(__ACTION__));
	
	foreach ($controllers as $item)
	{
		if( strpos($controller, "/{$item}/") !== false )
		{
			return "active";
		}
	}
}
/**
 * 获取时间戳
 * @return number
 */
function get_time()
{
	return time();
}
/**
 * 日期修改器
 * @param string $date
 * @param string $format
 * @param string $default
 * @return string
 */
function get_format_date($date = null,$format = 'Y-m-d H:i:s',$default = '--')
{
	return empty($date) ? $default : date($format,$date);
}
/**
 * 货币修改器
 * @param number $money
 * @param number $decimals
 * @param string $format
 * @param string $default
 * @return string
 */
function get_format_money($money = 0,$decimals = 2,$format = '￥',$default = '--')
{
	return empty($money) ? $default : $format . number_format($money,$decimals);
}
/**
 * 默认值修改器
 * @param string $condition
 * @param string $format
 * @param string $default
 * @return string
 */
function get_format_default($condition = false,$format = true,$default = '--')
{
	return $condition ? ($format === true ? $condition : $format) : $default;
}
/**
 * 枚举修改器，如：状态、订单状态等
 * @param number $state
 * @param unknown $enum
 * @return string|number
 */
function get_format_state($state = 0,$enum = array())
{
	if( empty($enum) )
	{
	    $enum = array('锁定.text-red','正常');
	}
	
	$enum = array_map(function($item){
		
	    list($text,$class) = explode('.', $item);
	    
	    return sprintf('<span class="%s">%s</span>',$class,$text);
	    
	}, $enum);
	
	return array_key_exists($state, $enum) ? $enum[$state] : $state;
}

/**
 * 快捷地址修改器
 * @param unknown $action
 * @param unknown $parm
 * @param string $key
 * @param string $suffix
 * @param string $domain
 * @return string
 */
function get_format_url($action,$parm,$key = 'id',$suffix=true,$domain=false)
{
	if( is_array($parm) === false )
	{
	    $parm = array($key=>$parm);
	}
	
	return U($action,$parm,$suffix,$domain);
}

/**
 * 枚举修改器，如：是/否
 * @param number $state
 * @param unknown $enum
 * @return string|number
 */
function get_format_notice($state = 0,$enum = array())
{
	if( empty($enum) )
	{
		$enum = array('否.text-red','是');
	}

	$enum = array_map(function($item){

		list($text,$class) = explode('.', $item);
		 
		return sprintf('<span class="%s">%s</span>',$class,$text);
		 
	}, $enum);

		return array_key_exists($state, $enum) ? $enum[$state] : $state;
}

/**
 * 截取长字符串 
 * @param str $str
 * @param int $enum
 * @return string
 */
function get_short_str($str='',$enum = 8,$charset='utf-8')
{
	$length = strLength($str);

	$msg = (int)$length>$enum ? mb_substr($str, 0,(int)$enum,$charset) . '...' : $str;
	
	return $msg;
}

/**
 * 获取字符串中英文混合长度
 * @param $str string 字符串
 * @param $$charset string 编码
 * @return 返回长度，1中文=1位，2英文=1位
 */
function strLength($str,$charset='utf-8'){
	if($charset=='utf-8') $str = iconv('utf-8','gb2312',$str);
	$num = strlen($str);
	$cnNum = 0;
	for($i=0;$i<$num;$i++){
		if(ord(substr($str,$i+1,1))>127){
			$cnNum++;
			$i++;
		}
	}
	$enNum = $num-($cnNum*2);
	$number = ($enNum/2)+$cnNum;
	return ceil($number);
}

/**
 * 签名生成
 * @param unknown $action	地址
 * @param unknown $parm		待签名字符串或数据
 * @param string $salt		加料
 * @param string $sign		签名属性名称
 * @param string $key		待签名字符串属性名称
 * @param string $algo		签名加密方式
 * @param string $suffix
 * @param string $domain
 * @return Ambigous <string, mixed>
 */
function get_format_sign($action,$parm,$salt = true,$sign = 'sign',$key = 'id',$algo = 'md5',$suffix=true,$domain=false)
{
	if( is_array($parm) === false )
	{
		$parm = array($key=>$parm);
	}
	
	if( $salt === true )
	{
		$salt = rand(1000, 9999);
	}
	
	$query = $parm;
	
	$query['salt'] = $salt;
	
	ksort($query);
	
	$query = http_build_query($query);
	
	$parm[$sign] = $algo($query);
	
	return U($action,$parm,$suffix,$domain);
}
/**
 * 签名验证
 * @param unknown $data		待验证数组
 * @param string $salt		加料
 * @param string $sign		签名属性名称
 * @param string $algo		签名加密方式
 * @return boolean
 */
function check_format_sign($data = array(),$salt = true,$sign = 'sign',$algo = 'md5')
{
	if( is_array($data) === false )
	{
		return false;
	}
	
	if( $salt === true )
	{
		$salt = rand(1000, 9999);
	}
	
	$checkSign = $data[$sign];
	
	unset($data[$sign]);
	
	$data['salt'] = $salt;
	
	ksort($data);
	
	$query = http_build_query($data);
	
	return $checkSign == $algo($query) ? true :false;
}
/**
 * 分页
 * @param unknown $totalRows
 * @param unknown $pageShow
 * @param number $limit
 * @param unknown $parameter
 * @return string
 */
function get_page($totalRows,&$pageShow,$limit=10,$parameter=array())
{
	$page = new \Think\Page($totalRows,$limit,$parameter);
	
	$page->setConfig('header','共<span>%TOTAL_ROW%</span>条记录，每页<span>'.$limit.'</span>条,共<span>%TOTAL_PAGE%</span>/<span>%NOW_PAGE%</span>页');
	
	$page->setConfig('first','首页');
	
	$page->setConfig('prev','上一页');
	
	$page->setConfig('next','下一页');
	
	$page->setConfig('last','末页');
	
	$page->setConfig('theme','<ul class="pagination pagination-sm no-margin pull-right"><li>%FIRST%</li><li>%UP_PAGE%</li><li>%DOWN_PAGE%</li><li>%END%</li><li class="active"><a href="javascript:void(0);">%HEADER%</a></li></ul>');
	
	$page->lastSuffix = false;
	
	$pageShow = $page->show();
	
	return $page->firstRow.','.$page->listRows;
}