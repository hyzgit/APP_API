<?php

namespace App\Logic;

use Common\Model\CancelModel;

class CancelLogic extends CancelModel 
{
	protected function _initialize() 
	{
		parent::_initialize();
	}

	public function getCancelData( $data = array() )
	{   
		$raw = array();
		
		$raw = $this->where(array('cancel_contract_id' => $data['contractId'],'cancel_state' => 'NORMAL'))->getItem();
		
		if( empty($raw) === false )
		{
			$result = array(
				'cancel_contract_id' => $raw['cancel_contract_id'],
				'cancel_status' => $raw['cancel_status'],
				'cancel_audit_time'=>$raw['cancel_audit_time'],
				'cancel_remark'=>$raw['cancel_remark'],
			);
			
			$this->error = '请求成功';
			
			return $result;
		}
		else
		{
			$this->error = '未查询到记录数据';
				
			return null;
		}
		
		$this->error = '异常错误';
		
		return false;
	}
	
	public function updateCancel( $data = array() )
	{	
		$data = $this->mapData($data);
	
		$data['time'] = time();
		$data['status'] = 'WAIT';
	
		$data = $this->create($data);
	
		$where = array('cancel_contract_id'=>$data['cancel_contract_id']);
	
		if( empty($this->where($where)->getItem() ) === false )
		{
			$this->error = '撤销申请已提交';
				
			return false;
		}
	
		if ( $this->updateRecord($data) === false )
		{
			return false;
		}
		
		$raw = $this->where($where)->getItem();
		
		$result = array(
				"cancel_contract_id" =>$raw['cancel_contract_id'],
				"cancel_status" => $raw['cancel_status'],
				"cancel_audit_time" => $raw['cancel_audit_time'],
				"cancel_remark" => $raw['cancel_remark']
		);
	
		return $result;
	}
	
	public function mapData( $data = array() )
	{
		$ret = array();
		
		$map = array(
				'contractId' => 'contract_id',
				'name' => 'name',
				'certificateCard' => 'certificate_card',
				'loanBankNumber' => 'loan_bank_number',
				'bankName' => 'bank_name',
				'mobileNumber' => 'mobile_number',
				'status' => 'contract_status',
				'matchStatus' => 'match_status',
				'customerRemark' => 'customer_remark',
		);
		
		foreach ( $data as $key=>$value )
		{
			$ret[$map[$key]] = $value;
		}
		
		return $ret;
	}
	
	public function checkRequire($required = array(),$query = array())
	{
		$diff = array_diff($required,array_keys($query));
	
		if( empty($diff) === false )
		{
			foreach ($diff as $row)
			{
				$this->error .= $row . '不能为空,';
			}
				
			$this->error = trim($this->error,',');
				
			return false;
		}
		return true;
	}
	
	public function getSecretKey() 
	{
		return '72969e04baa8b6e43fae34e1969d56a6';
	}
	
	public function checkSign($data) 
	{
		$clientSign = $data['sign'];
		
		$sign = $this->getSign($data);
		
		return $clientSign === $sign;
	}
	
	public function getSign($data)
	{
		unset($data['sign']);
		
		$sk = $this->getSecretKey();
		
		ksort($data);
		$query = '';
		
		foreach ($data as $key => $item)
		{
			$query .= "&{$key}={$item}";
		}
	
		$query = trim($query,'&');
		$query .= $sk;
		
		return md5($query);
	}
}
