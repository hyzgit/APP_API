<?php

namespace App\Logic;

use Common\Model\SecurityModel;

class SecurityLogic extends SecurityModel {

    protected function _initialize() {
        parent::_initialize();
    }

    public function getSecretKey() {
        return '72969e04baa8b6e43fae34e1969d56a6';
    }

    public function checkSign($data) {
        $clientSign = $data['sign'];
        $sign = $this->getSign($data);
        return $clientSign === $sign;
    }

    public function getSign($data) {
        unset($data['sign']);
        $sk = $this->getSecretKey();
        ksort($data);
        $query = '';
        foreach ($data as $key => $item) {
            $query .= "&{$key}={$item}";
        }
        $query = trim($query,'&');
        $query .= $sk;
        return md5($query);
    }

}
