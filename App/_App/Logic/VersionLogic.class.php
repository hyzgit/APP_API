<?php
namespace App\Logic;

use Common\Model\VersionModel;

class VersionLogic extends VersionModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}

	public function checkVersion($query = array())
	{
		$field = array(
			'version_status'=>'status',
			'version_name'=>'name',
			'version_release'=>'version',
			'version_code'=>'code',
			'version_download'=>'url',
			'version_message'=>'message',
			'version_description'=>'description',
			'version_platform'=>'platform',
			'version_platform'=>'platform',
			'version_key' => 'key',
			'version_notice'=>'notice'
		);
			
		$where = array(
				'version_type' 		=> $query['type'],
				'version_platform'	=> $query['platform'],
				'version_code'		=> $query['code'],
		);
		
		$data = $this->field($field)->where($where)->getItem();
		
		if( $data === false )
		{
			$this->error = '查询异常';
			
			return false;
		}
		
		if(empty($data) === true)
		{
			$data['key'] = '72969e04baa8b6e43fae34e1969d56a6';
			$data['status'] = 0;
			$data['notice'] = 1;
			$data['message'] = '请下载最新版本';
		}
		
		return $data;
	}
	
	public function getSigntrue($data = array())
	{
		ksort($data);
		
		$ckey = $data['key'];
		
		unset($data['key']);
		
		$query = '';
		
		foreach ($data as $key => $item)
		{
			$query .= "&{$key}={$item}";
		}
		
		$query = trim($query,'&');
		
		$query .= $ckey;
		
		return md5($query);
	}
}