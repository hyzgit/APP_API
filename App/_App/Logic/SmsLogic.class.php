<?php

namespace App\Logic;

use Common\Model\SmsModel;

class SmsLogic extends SmsModel
{
	private $num = 4;
	
    protected function _initialize()
    {
        parent::_initialize();
    }
    
    public function getupdataData($data)
    {
    	if( empty($data) )
    	{
    		$this->error = '数据不能为空';
    		
    		return false;
    	}

    	$retdata = array();
    	
    	$data = explode(';', $data);
    	
    	$time = time();
    	
    	foreach ($data as $item)
    	{
    		$val = explode(',', $item);
    		
    		$val = $this->getmapData($val);
    		
    		if( $val == false )
    		{
    			$this->error = '参数错误';
    			 
    			return false;
    		}
    		
    		$val['sms_create_time'] = $time;
    		
    		$retdata[] = $val;
    		
    	}
    	return $retdata;
    }
    
    public function addAlldata($data = array())
    {
    	$data = $this->addAll($data);
    	
    	return $data;
    }
    
    protected function getmapData($data)
    {
    	$key = array('sms_report_id','sms_service_number','sms_mobile','sms_code','sms_state','sms_time');
    	
    	return array_combine($key,$data);
    }
    
}
