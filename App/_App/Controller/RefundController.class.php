<?php

namespace App\Controller;

use App\Logic\RefundLogic;

class RefundController extends ApiController 
{
	private $refund = null;
	
	protected function _initialize() 
	{
		parent::_initialize();

		$this->refund = new RefundLogic();
	}
	
	public function checkAction()
	{
		$raw = array();
		
		$query = I('post.');
		
		// 验签
		if( $this->refund->checkSign($query) === false )
		{				
			$this->ajaxReturn(array('msg'=>'ERROR_SIGN','state'=>0));
		}
		
		// 检查必填参数
		$required = array('package','code','contractId');
		
		if( $this->refund->checkRequire($required,$query) === false )
		{
			$this->ajaxReturn(array('msg'=>$this->refund->getError(),'state'=>0));
		}
		
		$data = $this->refund->getRefundData($query);
		
		if( $data === false )
		{
			$raw = array('msg'=>$this->refund->getError(),'state'=>0,'data'=>null);
		}
		else
		{
			$raw = array('msg'=>$this->refund->getError(),'state'=>1,'data'=>$data);
		}
		
		$this->ajaxReturn($raw);	
	}
	
	public function refundAction()
	{
		$query = I('post.');
		
		$raw = array();
		
		// 验签
		if( $this->refund->checkSign($query) === false )
		{
			$this->ajaxReturn(array('msg'=>'ERROR_SIGN','state'=>0));
		}
		
		// 检查必填参数
		$required = array('package','code');

		if( $this->refund->checkRequire($required,$query) === false )
		{
			$this->ajaxReturn(array('msg'=>$this->refund->getError(),'state'=>0));
		}
		
		$ret = $this->refund->updateRefund($query);
		
		if( $ret === false )
		{
			$raw = array('msg'=>$this->refund->getError(),'state'=>0,'data'=>NULL);
		}
		else
		{
			$raw = array('msg'=>'请求成功','state'=>1,'data'=>$ret);
		}
		
		$this->ajaxReturn($raw);
	}
}
