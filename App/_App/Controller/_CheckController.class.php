<?php
namespace App\Controller;

use App\Logic\VersionLogic;

/**	
 * APP版本信息
 */

class CheckController extends ApiController
{
	private $version = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->version = new VersionLogic();
	}
	
	// APP版本检测统一接口
	public function versionAction()
	{
		$query = array_merge(I('get.'),I('post.'));	
		
		// 检查必填参数
		$required = array('type','code','platform');
		
		if( $this->version->checkRequire($required,$query) === false )
		{
			$this->ajaxReturn(array('msg'=>$this->version->getError(),'state'=>0));
		}
		
		$data = $this->version->checkVersion($query);
		
		if( $data === false )
		{
			return $this->ajaxReturn(array('msg'=>$this->version->getError(),'state'=>0));
		}
		//取得密钥key
		$query['key'] = $data['key'];
		// 验签
		if( $this->version->checkSign($query) === false )
		{
			$this->ajaxReturn(array('msg'=>'ERROR_SIGN','state'=>0));
		}
		
		unset($data['key']);
		return $this->ajaxReturn(array('msg'=>'请求成功','state'=>1,'data'=>$data));
	}

}