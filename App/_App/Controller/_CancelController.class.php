<?php

namespace App\Controller;

use App\Logic\CancelLogic;

class CancelController extends ApiController 
{
	private $cancel = null;
	private $error = null;
	
	protected function _initialize() 
	{
		parent::_initialize();

		$this->cancel = new CancelLogic();
	}
	
	public function checkAction()
	{
		$query = array_merge(I('post.'),I('get.'));
		
		$raw = array(
			'data' => array(),
			'msg' => '请求失败',
			'state' => 0
		);
		
// 		if( $this->cancel->checkSign($query) === false ) 
// 		{
// 			$raw['msg'] = 'ERROR_SIGN';
			
// 			return $this->ajaxReturn($raw);
// 		}
		
// 		$return = $this->cancel->checkPara($query);
		
// 		if( $return === false )
// 		{
// 			$raw['msg'] = $this->cancel->getError();
			
// 			return $this->ajaxReturn($raw);
// 		}
		
// 		$cancel_data = $this->cancel->getCancelData($return);
		
// 		if ( $cancel_data === false ) 
// 		{
// 			$raw['msg'] = $this->cancel->getError();
			
// 			return $this->ajaxReturn($raw);
// 		}

		$cancel_data = array(
				'cancel_status'=>'INIT',
				'cancel_contract_id'=>'2017110212345',
		);

		$raw['msg'] = '请求成功';
		$raw['state'] = 1;
		$raw['data'] = $cancel_data;
		
		return $this->ajaxReturn($raw);
	}
	
	public function cancelAction()
	{
		$cancel_data = array(
				'cancel_status'=>'SUCCESS',
				'cancel_contract_id'=>'2017110212345',
		);
		
		$raw['msg'] = '请求成功';
		$raw['state'] = 1;
		$raw['data'] = $cancel_data;
		
		return $this->ajaxReturn($raw);
	}
}
