<?php
namespace Admin\Widget;

use Think\Controller;

class EditWidget extends Controller
{
	public function umeditorAction($content = null,$name = 'content')
	{
		$content = htmlspecialchars_decode($content);
		
		$this->assign('content',$content);
		
		$this->assign('name',$name);
		
		$this->assign('width','100%');
		
		$this->assign('height','100%');
		
		// Widget 插件必须制定模板地址
		$this->display('Edit/umeditor');
	}
}