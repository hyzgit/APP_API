<?php
/**
 * 权限判断
 * @param string $controllers	待验证权限集，如：true,/Home/Index/index
 */
function permission($controllers = true)
{
	// 获取
	$manager = psession('manager');
	
	$levels = $manager['manager_level_data'];
	
	// 转化为小写
	$levels = strtolower($levels);
	
	// 分拆为数组
	$levels = explode(',', $levels);
	
	// 获取
	if( $controllers === true )
	{
		$controllers = __ACTION__;
	}
	
	// 转化为小写
	$controllers = strtolower($controllers);
	
	// 分拆为数组
	$controllers = explode(',', $controllers);
	
	$callback = false;
	
	foreach ($controllers as $items)
	{
		$items = trim($items,'/');
		
		foreach ($levels as $item)
		{
			$item = trim($item,'/');
			
			if( strpos("/{$item}/", "/{$items}/") === false )
			{
				$callback = false;
			}
			else
			{
				$callback = true;
				
				break;
			}
		}
		
		if( $callback === true )
		{
			break;
		}
	}
	
	return $callback;
}

/**
 * 签名方法
 * @param array $data 待签名数组
 * @param string $salt 处理机制
 * @return string
 */
function getsign($data = array(),$salt = 'md5,strtolower',$key='')
{
	ksort($data);

	$sign = '';

	foreach($data as $k=>$item)
	{
		if($item != "")
		{
			$sign .= $k.'='.$item.'&';
		}
		
	}

	$sign = rtrim($sign, "&").$key;
	
	foreach (explode(',', $salt) as $item)
	{
		$sign = $item($sign);
	}
	
	return $sign;
}

/**
 * 邮件发送函数
*/
function sendMail($to, $title, $content) {
	 
	Vendor('PHPMailer.PHPMailerAutoload');
	$mail = new PHPMailer(); //实例化
// 	var_dump(C('MAIL_PASSWORD'));exit();
	$mail->isSMTP(); // 启用SMTP
	$mail->Host=C('MAIL_HOST'); //smtp服务器的名称（这里以QQ邮箱为例）
	$mail->SMTPAuth = C('MAIL_SMTPAUTH'); //启用smtp认证
	$mail->Username = C('MAIL_USERNAME'); //你的邮箱名
	$mail->Password = C('MAIL_PASSWORD') ; //邮箱密码
	$mail->From = C('MAIL_FROM'); //发件人地址（也就是你的邮箱地址）
	$mail->FromName = C('MAIL_FROMNAME'); //发件人姓名
	$mail->addAddress($to,"尊敬的客户");
	$mail->WordWrap = 50; //设置每行字符长度
	$mail->IsHTML(C('MAIL_ISHTML')); // 是否HTML格式邮件
	$mail->CharSet=C('MAIL_CHARSET'); //设置邮件编码
	$mail->Subject =$title; //邮件主题
	$mail->Body = $content; //邮件内容
	$mail->AltBody = "这是一个纯文本的身体在非营利的HTML电子邮件客户端"; //邮件正文不支持HTML的备用显示
	return($mail->send());
}


?>
