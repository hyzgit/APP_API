<?php

namespace Admin\Logic;

use Common\Model\SmsModel;

class SmsLogic extends SmsModel
{
    protected function _initialize()
    {
        parent::_initialize();
    }
    
    public function getPage($search = array(),&$pageShow = null,$pageSize = 10)
    {
    	$where = empty($search) ? array() : sprintf('LOCATE("%1$s",sms_mobile)',$search);
    
    	$count = $this->where($where)->count();
    
    	$data = $this->where($where)->pagination($count,$pageShow,$pageSize)->order('sms_id DESC')->getItems();
    
    	foreach ($data as $key => &$item)
    	{
    		$state = ($item['sms_state'] === '0' || $item['sms_state'] === 'DELIVRD') ? 0 : 1;
    		$item['state'] = get_format_state($state,array('成功',$item['sms_state'].'.text-red'));
    		$item['detail'] = get_format_url('detail', $item['sms_id']);
    	}
    	 
    	return $data;
    }
    
    public function getEdit($pk = 0)
    {
    	return $this->getItem($pk);
    }
    
}
