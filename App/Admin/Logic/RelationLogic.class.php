<?php
namespace Admin\Logic;

use Common\Model\RelationModel;

class RelationLogic extends RelationModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	public function getPage($search = array(),&$pageShow = null,$pageSize = 10,$parameter = array())
	{
		$where = array();
		
		$where = empty($search['keyword']) ? array() : sprintf('LOCATE("%1$s",relation_contract_id)',$search['keyword']);

		$totalRows = $this->where($where)->count();
	
		$order = 'relation_status DESC,relation_id DESC';
	
		$data = $this->where($where)->pagination($totalRows,$pageShow,$pageSize,$parameter)->order($order)->getItems();
	
		foreach ($data as $key => &$item)
		{
			$item['scenes'] = get_format_state($item['relation_scenes'], array('FIRST'=>'首次网签','SECOND'=>'二次网签'));
			$item['status'] = get_format_state($item['relation_status'], array('DISABLE'=>'锁定.text-red','NORMAL'=>'正常'));
			$item['neturl'] = get_short_str($item['relation_neturl'],80);
			$item['edit'] = get_format_url('edit', $item['relation_id']);
			$item['delete'] = get_format_url('delete', $item['relation_id']);
		}
	
		return $data;
	}
	
	public function getEdit($pk = 0)
	{
		return $this->getItem($pk);
	}
	
	public function updateByPK($data = array())
	{
		$data['status'] = $data['status'] == 1 ? 'NORMAL' : 'DISABLE';
		
		$data['scenes'] = $data['scenes'] == 1 ? 'FIRST' : 'SECOND';
		
		$data = $this->create($data,'both');
		
		return $this->updateRecord($data);
	}
	
	public function deleteByPK($pk = 0)
	{
		return $this->deleteRecord($pk);
	}
		
}