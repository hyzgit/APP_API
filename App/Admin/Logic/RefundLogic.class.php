<?php

namespace Admin\Logic;

use Common\Model\RefundModel;

class RefundLogic extends RefundModel 
{
	protected function _initialize() 
	{
		parent::_initialize();
	}

	public function getPage($search = array(),&$pageShow = null,$pageSize = 10,$parameter=array())
	{
		$sql = $this->getSql($search);
		
		$count =  $this->where($sql)->count();

		$data = $this->where($sql)->pagination($count,$pageShow,$pageSize,$parameter)->order('refund_state DESC,refund_time DESC,refund_id DESC')->getItems();
		
		$contractStatus = $this->getContractStatus();
		
		foreach ($data as $key => &$item)
		{	
			$item['state'] = get_format_state($item['refund_state'], array('DISABLE'=>'锁定.text-red','NORMAL'=>'正常'));
			$item['status'] = get_format_state($item['refund_status'],array('INIT'=>'初始状态','WAIT'=>'待审核','SUCCESS'=>'通过.text-green','REJECT'=>'驳回.text-red','COMPLETE'=>'打款完成.text-blue'));
			$item['contract_status'] = get_format_state($item['refund_contract_status'], $contractStatus);
			$item['time'] = get_format_date($item['refund_time']);
			$item['amount'] = get_format_money($item['refund_amount']/100);
			$item['complete_time'] = get_format_date($item['refund_complete_time']);
			
			$item['detail'] = get_format_url('detail', $item['refund_id']);
			$item['audit'] = get_format_url('audit', $item['refund_id']);
			
			$state = $item['refund_state'] == 'NORMAL' ? '0' : '1';
			$item['lock'] = get_format_url('lock', array('id'=>$item['refund_id'],'state'=>$state));
		}
		
		return $data;
	}
	
	public function getDownload($search = array(),$order = '')
	{	
		$sql = $this->getSql($search);
		
		$data = $this->where($sql)->order('refund_state DESC,refund_time DESC,refund_id DESC')->getItems();
	
		$raw = array();
		// 配置数据准备
		$stateArr = array("DISABLE" => "锁定","NORMAL" => "正常");
	
		$statusArr = $this->getStatus();
			
		$conStatusArr = $this->getContractStatus();
		
		foreach ($data as $key => $item)
		{	
			$raw[] = array(
						'refund_contract_id' =>'\''.$item['refund_contract_id'],
						'refund_name' => $item['refund_name'],
						'refund_certificate_card' =>empty($item['refund_certificate_card']) ? '' : '\''.$item['refund_certificate_card'],
						'refund_mobile_number' => empty($item['refund_mobile_number']) ? '' : '\''.$item['refund_mobile_number'],
						'refund_time' => get_format_date($item['refund_time'],'Y年m月d日 H:i:s'),
						'refund_amount' => $item['refund_amount']/100,
						'refund_complete_time' => get_format_date($item['refund_complete_time'],'Y年m月d日 H:i:s'),
						'refund_contract_status' => $conStatusArr[$item['refund_contract_status']],
						'refund_status' => $statusArr[$item['refund_status']],
						'refund_state' => $stateArr[$item['refund_state']],
					);
		}
		
		return $raw;	
	}
	
	public function getSql( $search = array() )
	{
		$sql = "";
	
		$search['refund_start_time'] = empty($search['refund_start_time']) == false ? $search['refund_start_time']:'2017-01-01';
	
		$search['refund_end_time'] = empty($search['refund_end_time']) == false ? $search['refund_end_time'] : date("Y-m-d H:i:s",time());
	
		$sql .= '(refund_time BETWEEN ' . strtotime($search["refund_start_time"]) . ' AND ' . (strtotime($search["refund_end_time"])+86400) . ")";
	
		if( empty($search['complete_start_time']) && empty($search['complete_end_time']) )
		{
			$sql .= "";
		}
		else
		{
			$search['complete_start_time'] = empty($search['complete_start_time']) == false ? $search['complete_start_time']:'2017-01-01';
	
			$search['complete_end_time'] = empty($search['complete_end_time']) == false ? $search['complete_end_time'] : date("Y-m-d H:i:s",time());
	
			$sql .= ' AND (refund_complete_time BETWEEN ' . strtotime($search["complete_start_time"]) . ' AND ' . (strtotime($search["complete_end_time"])+86400) . ")";
		}
	
		if ( $search['status'] !== 'all' )
		{
			$sql .= " AND refund_status = '" . $search['status'] . "'";
		}
	
		if ( $search['contract_status'] !== 'all' )
		{
			$sql .= " AND refund_contract_status = '" . $search['contract_status'] . "'";
		}
	
		$sql .= empty($search['name']) ? '':' AND ('.sprintf('LOCATE("%1$s",refund_name)',$search['name']).')';
	
		$sql .= empty($search['contract_id']) ? '':' AND ('.sprintf('LOCATE("%1$s",refund_contract_id)',$search['contract_id']).')';
	
		$sql .= empty($search['mobile_number']) ? '':' AND ('.sprintf('LOCATE("%1$s",refund_mobile_number)',$search['mobile_number']).')';
	
		return $sql;
	}

	public function getEdit($pk = 0)
	{
		$item = array();
		
		$join = "__CONTRACT__ ON contract_number = refund_contract_id";
		
		$item = $this->join($join, "LEFT")->getItem($pk);
		
		$contractStatus = $this->getContractStatus();
		
		if( empty($item) === false )
		{
			$item['contract_time'] = get_format_date($item['contract_time']);
			$item['contract_pay_status'] = get_format_state($item['contract_status'],array('未支付','支付成功.text-green','支付失败.text-red'));
			$item['contract_pay_amount'] = get_format_money($item['contract_amount']/100);
			
			$item['state'] = get_format_state($item['refund_state'], array('DISABLE'=>'锁定.text-red','NORMAL'=>'正常'));
			$item['status'] = get_format_state($item['refund_status'],array('INIT'=>'初始状态','WAIT'=>'待审核','SUCCESS'=>'通过.text-green','REJECT'=>'驳回.text-red','COMPLETE'=>'打款完成.text-blue'));
			$item['contract_status'] = get_format_state($item['refund_contract_status'], $contractStatus);
			$item['time'] = get_format_date($item['refund_time']);
			$item['audit_time'] = get_format_date($item['refund_audit_time']);
			$item['complete_time'] = get_format_date($item['refund_complete_time']);
			$item['amount'] = get_format_money($item['refund_amount']/100);
		}
				
		return $item;
	}
	
	public function auditByPK($data = array())
	{
		$item = array();
		
		$item = $this->getItem($data["id"]);
		
		if($item["refund_state"] == "DISABLE")
		{
			$this->error = "锁定状态不能进行审核操作";
		
			return false;
		}
		
		$map = array("0" => "REJECT","1" => "SUCCESS","2" => "COMPLETE");
		
		$data["status"] = $map[$data["status"]];
		
		if($data['status'] == 'COMPLETE')
		{
			$data["complete_time"] = time();
		}
		else
		{
			$data["audit_time"] = time();
		}
		
		$data = $this->create($data);
	
		return $this->updateRecord($data);
	}
	
	public function updateByPK($data = array())
	{
		$data = $this->create($data);
	
		return $this->updateRecord($data);
	}
	
	public function getContractStatus()
	{
		return array(
				'REVOKE'=>'已撤销',
				'DRAFT'=>'草稿',
				'WAIT_UPDATE'=>' 待修改',
				'UPDATEED_AUDIT'=>' 已修改-待审核',
				'FIRST_WAIT'=>' 待初审',
				'WAIT'=>' 待审核',
				'PASSING'=>' 审核中',
				'FIRST_PASSED'=>' 初审通过',
				'PASSED'=>' 审核通过',
				'REJECT'=>' 人工驳回',
				'AUTO_REJECT'=>' 机审驳回',
				'DELIVER'=>' 已发货',
				'REFUNDING'=>' 还款中',
				'SUCCESS'=>' 已完成',
		);
	}
	
	public function getStatus()
	{
		return array("INIT" => "初始化","WAIT" => "待审核","REJECT" => "驳回","SUCCESS" => "通过","COMPLETE" => "打款完成");
	}

}
