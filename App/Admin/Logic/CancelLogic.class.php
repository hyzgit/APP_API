<?php

namespace Admin\Logic;

use Common\Model\CancelModel;

class CancelLogic extends CancelModel 
{
	protected function _initialize() 
	{
		parent::_initialize();
	}

	public function getPage($search = array(),&$pageShow = null,$pageSize = 10,$parameter=array())
	{
		$sql = $this->getSql($search);
		
		$count =  $this->where($sql)->count();
		
		$data = $this->where($sql)->pagination($count,$pageShow,$pageSize,$parameter)->order('cancel_state DESC,cancel_time DESC,cancel_id DESC')->getItems();
		
		$contractStatus = $this->getContractStatus();
		
		$matchStatus = $this->getMatchStatus();
		
		foreach ($data as $key => &$item)
		{	
			$item['state'] = get_format_state($item['cancel_state'], array('DISABLE'=>'锁定.text-red','NORMAL'=>'正常'));
			$item['status'] = get_format_state($item['cancel_status'],array('INIT'=>'初始状态','WAIT'=>'待审核','SUCCESS'=>'通过.text-green','REJECT'=>'驳回.text-red'));
			$item['contract_status'] = get_format_state($item['cancel_contract_status'], $contractStatus);
			$item['match_status'] = get_format_state($item['cancel_match_status'], $matchStatus);
			$item['time'] = get_format_date($item['cancel_time']);
			
			$item['detail'] = get_format_url('detail', $item['cancel_id']);
			$item['audit'] = get_format_url('audit', $item['cancel_id']);
			
			$state = $item['cancel_state'] == 'NORMAL' ? '0' : '1';
			$item['lock'] = get_format_url('lock', array('id'=>$item['cancel_id'],'state'=>$state));
		}
		
		return $data;
	}
	
	public function getDownload($search = array(),$order = '')
	{
		$sql = $this->getSql($search);
	
		$data = $this->where($sql)->order('cancel_state DESC,cancel_time DESC,cancel_id DESC')->getItems();

		$raw = array();
		// 配置数据准备
		$stateArr = array("DISABLE" => "锁定","NORMAL" => "正常");
		
		$statusArr = $this->getStatus();
		
		$conStatusArr = $this->getContractStatus();
			
		$matchStatusArr = $this->getMatchStatus();
	
		foreach ($data as $key => $item)
		{
			$raw[] = array(
					'cancel_contract_id' =>'\''.$item['cancel_contract_id'],
					'cancel_name' => $item['cancel_name'],
					'cancel_certificate_card' =>empty($item['cancel_certificate_card']) ? '' : '\''.$item['cancel_certificate_card'],
					'cancel_mobile_number' => empty($item['cancel_mobile_number']) ? '' : '\''.$item['cancel_mobile_number'],
					'cancel_match_status' => $matchStatusArr[$item['cancel_match_status']],
					'cancel_contract_status' => $conStatusArr[$item['cancel_contract_status']],
					'cancel_time' => get_format_date($item['cancel_time'],'Y年m月d日 H:i:s'),
					'cancel_status' => $statusArr[$item['cancel_status']],
					'cancel_state' => $stateArr[$item['cancel_state']],
			);
		}
		
		return $raw;
	}
	
	public function getSql( $search = array() )
	{
		$sql = "";
		
		$search['start_time'] = empty($search['start_time']) == false ? $search['start_time']:'2017-01-01';
		
		$search['end_time'] = empty($search['end_time']) == false ? $search['end_time'] : date("Y-m-d H:i:s",time());
			
		$sql .= 'cancel_time BETWEEN ' . strtotime($search["start_time"]) . ' AND ' . (strtotime($search["end_time"])+86400);
		
		if ( $search['status'] !== 'all' )
		{
			$sql .= " AND cancel_status = '" . $search['status'] . "'";
		}
		
		if ( $search['contract_status'] !== 'all' )
		{
			$sql .= " AND cancel_contract_status = '" . $search['contract_status'] . "'";
		}
		
		$sql .= empty($search['name']) ? '':' AND ('.sprintf('LOCATE("%1$s",cancel_name)',$search['name']).')';
		
		$sql .= empty($search['contract_id']) ? '':' AND ('.sprintf('LOCATE("%1$s",cancel_contract_id)',$search['contract_id']).')';
		
		$sql .= empty($search['mobile_number']) ? '':' AND ('.sprintf('LOCATE("%1$s",cancel_mobile_number)',$search['mobile_number']).')';
		
		return $sql;
	}
	
	public function getEdit($pk = 0)
	{
		$item = $this->getItem($pk);
		
		$contractStatus = $this->getContractStatus();
		
		$matchStatus = $this->getMatchStatus();
		
		if( empty($item) === false )
		{
			$item['state'] = get_format_state($item['cancel_state'], array('DISABLE'=>'锁定.text-red','NORMAL'=>'正常'));
			$item['status'] = get_format_state($item['cancel_status'],array('INIT'=>'初始状态','WAIT'=>'待审核','SUCCESS'=>'通过.text-green','REJECT'=>'驳回.text-red'));
			$item['contract_status'] = get_format_state($item['cancel_contract_status'], $contractStatus);
			$item['match_status'] = get_format_state($item['cancel_match_status'], $matchStatus);
			$item['time'] = get_format_date($item['cancel_time']);
			$item['audit_time'] = get_format_date($item['cancel_audit_time']);
		}
				
		return $item;
	}
	
	public function auditByPK($data = array())
	{
		$item = $this->getItem($data["id"]);
		
		if($item["cancel_state"] == "DISABLE")
		{
			$this->error = "锁定状态不能进行审核操作";
		
			return false;
		}
	
		if($item["cancel_status"] == "SUCCESS" || $item["cancel_status"] == "REJECT")
		{
			$this->error = "此条记录已经审核";
			
			return false;
		}
		
		$map = array("0" => "REJECT","1" => "SUCCESS");
	
		$data["status"] = $map[$data["status"]];
		$data["audit_time"] = time();
		
		$data = $this->create($data);
	
		return $this->updateRecord($data);
	}

	public function updateByPK($data = array()) 
	{
		$data = $this->create($data);
		
		return $this->updateRecord($data);
	}
	
	public function getContractStatus()
	{
		return array(
				'REVOKE'=>'已撤销',
				'DRAFT'=>'草稿',
				'WAIT_UPDATE'=>' 待修改',
				'UPDATEED_AUDIT'=>' 已修改-待审核',
				'FIRST_WAIT'=>' 待初审',
				'WAIT'=>' 待审核',
				'PASSING'=>' 审核中',
				'FIRST_PASSED'=>' 初审通过',
				'PASSED'=>' 审核通过',
				'REJECT'=>' 人工驳回',
				'AUTO_REJECT'=>' 机审驳回',
				'DELIVER'=>' 已发货',
				'REFUNDING'=>' 还款中',
				'SUCCESS'=>' 已完成',								
		);
	}
	
	public function getMatchStatus()
	{
		return array(
				'INIT'=>' 初始状态',
				'WAIT'=>' 待校对',
				'SUCCESS'=>' 已校对',
				'REJECT'=>' 驳回',
				'WAIT_COMPL'=>' 待补全',
		);
	}
	
	public function getStatus()
	{
		return array("INIT" => "初始化","WAIT" => "待审核","REJECT" => "驳回","SUCCESS" => "通过");
	}

}


