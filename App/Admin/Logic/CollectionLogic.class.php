<?php

namespace Admin\Logic;

use Common\Model\CollectionModel;

class CollectionLogic extends CollectionModel 
{
	protected function _initialize() 
	{
		parent::_initialize();
	}

	public function getPage($where = array(),&$pageShow = null,$pageSize = 10,$parameter=array())
	{
		if( !empty($where) )
		{
			$where = array(
					'collection_key' => array('like','%' . $where . '%'),
					'collection_mode' => array('like','%' . $where . '%'),
					'_logic' => 'OR'
			);
		}
		
		$count =  $this->where($where)->count();
		
		$this->order('collection_time DESC,collection_id DESC');
		
		$data = $this->where($where)->pagination($count,$pageShow,$pageSize,$parameter)->getItems();
		
		foreach ($data as $key => &$item)
		{
			$item['delete'] = get_format_url('delete', $item['collection_id']);
			$item['detail'] = get_format_url('detail', $item['collection_id']);
			$item['edit'] = get_format_url('edit', $item['collection_id']);
		}
		
		return $data;
	}
	
	public function getEdit($pk = 0)
	{
		$item = $this->getItem($pk);
				
		return $item;
	}

	public function updateCollection($data = array()) 
	{
		$data['time'] = date('Y-m-d H:i:s',time());
		
		$data = $this->create($data,'both');
		
		if( $data === false )
		{
			return false;
		}
		
		return $this->updateRecord($data);
	}
	
	public function deleteByPK($pk = 0)
	{
		return $this->deleteRecord($pk);
	}

}


