<?php
namespace Admin\Logic;

use Common\Model\QrcodeModel;

class QrcodeLogic extends QrcodeModel
{
	private $key;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->key = C('KEY');
	}
	
	public function getPage($where = array(),&$pageShow = null,$pageSize = 10,$parameter = array())
	{
		$totalRows = $this->where($where)->count();
	
		$order = 'qrcode_status DESC,qrcode_id DESC';
	
		$data = $this->where($where)->pagination($totalRows,$pageShow,$pageSize,$parameter)->order($order)->getItems();
	
		foreach ($data as $key => &$item)
		{
			$item['status'] = get_format_state($item['qrcode_status']);
			$item['edit'] = get_format_url('edit', $item['qrcode_id']);
			$item['qrcode'] = get_format_url('qrcode', $item['qrcode_id']);
			$item['delete'] = get_format_url('delete', $item['qrcode_id']);
		}
		
		return $data;
	}
	
	public function updateQrcode($data = array())
	{
		$data = $this->create($data);
		
		if( $data === false )
		{
			return false;
		}
		
		return $this->updateByPK($data);
	}
	
	public function getEdit($options = array())
	{
		return $this->getItem($options);
	}
	
	public function getQrcodedata($data = array())
	{
		
		$info = array(
				'storeCode'=>$data['qrcode_storecode'],
				'userName' => $data['qrcode_username'],
				'commodityBrand' => $data['qrcode_commoditybrand'],
				'commodityModel' => $data['qrcode_commoditymodel'],
				'commodityPrice' => $data['qrcode_commodityprice'],
				'downPayment' => $data['qrcode_downpayment'],
				'loanType' => $data['qrcode_loantype'],
				'commodityType' => $data['qrcode_commoditytype'],
		);
	
		$info = base64_encode($this->getQrcodeinfo($info));
		
		return $info;
	}
	

	public function getQrcodeinfo($data = array())
	{
		
		foreach($data as $k=>$v)
		{
			if($v === ''|| $v == null)
			{
				unset($data[$k]);
			}
		}
	
		$data['sign']=getsign($data,'md5',$this->key);
		
		return json_encode($data);
	}
	
	public function getLoanType() {
		return array(
				'成人消费贷A'=>'成人消费贷A',
				'成人消费贷B'=>'成人消费贷B',
				'成人消费贷C'=>'成人消费贷C',
				'成人消费贷D'=>'成人消费贷D',
				'成人消费贷E'=>'成人消费贷E',
				'成人消费贷F'=>'成人消费贷F',
				'成人消费贷G'=>'成人消费贷G',
				'成人消费贷H'=>'成人消费贷H',
				'学生手机租赁A'=>'学生手机租赁A',
				'学生手机租赁B'=>'学生手机租赁B',
				'学生手机租赁C'=>'学生手机租赁C',
				'学生手机租赁高端A'=>'学生手机租赁高端A',
				'学生手机租赁低端B'=>'学生手机租赁低端B',
				'成人零利率'=>'成人零利率',
				'成人低息手机分期A'=>'成人低息手机分期A',
				'成人低息手机分期AB'=>'成人低息手机分期AB',
				'成人低息手机分期C'=>'成人低息手机分期C',
				'学生医美分期O'=>'学生医美分期O',
				'成人医美分期O'=>'成人医美分期O',
				'成人运营商融合业务O'=>'成人运营商融合业务O',
				'成人车险分期A'=>'成人车险分期A',
		);
	}
	
	public function getCommodityType() {
		return array(
				'手机'=>'手机',
				'电脑'=>'电脑',
				'教育培训'=>'教育培训',
				'旅游'=>'旅游',
				'家具'=>'家具',
				'家电'=>'家电',
				'家装装修'=>'家装装修',
				'家装建材'=>'家装建材',
				'3C数码（非手机）'=>'3C数码（非手机）',
				'摩托车'=>'摩托车',
				'电动车/自行车'=>'电动车/自行车',
				'乐器'=>'乐器',
				'手机租赁'=>'手机租赁',
				'医美'=>'医美',
				'车险'=>'车险',
				'CTCC/SC/MY/99/1200/24/50'=>'CTCC/SC/MY/99/1200/24/50',
				'CTCC/SC/MY/129/1440/24/60'=>'CTCC/SC/MY/129/1440/24/60',
				'CTCC/SC/MY/169/1920/24/80'=>'CTCC/SC/MY/169/1920/24/80',
				'CTCC/SC/MY/199/2400/24/100'=>'CTCC/SC/MY/199/2400/24/100',
				'CTCC/SC/MY/229/2640/24/110'=>'CTCC/SC/MY/229/2640/24/110',
				'CTCC/SC/MY/299/3360/24/140'=>'CTCC/SC/MY/299/3360/24/140',
				'CTCC/SC/MY/329/3840/24/160'=>'CTCC/SC/MY/329/3840/24/160',
				'CTCC/SC/MY/99/480/24/20'=>'CTCC/SC/MY/99/480/24/20',
				'CTCC/SC/MY/169/816/24/34'=>'CTCC/SC/MY/169/816/24/34',
				'CTCC/SC/MY/199/960/24/40'=>'CTCC/SC/MY/199/960/24/40',
				'CTCC/SC/MY/299/1440/24/60'=>'CTCC/SC/MY/299/1440/24/60',  
				'CTCC/SC/MY/299/2880/24/120'=>'CTCC/SC/MY/299/2880/24/120',
				'CUCC/SC/MY/56/672/24/28'=>'CUCC/SC/MY/56/672/24/28',
				'CUCC/SC/MY/76/912/24/38'=>'CUCC/SC/MY/76/912/24/38',
				'CUCC/SC/MY/106/1272/24/53'=>'CUCC/SC/MY/106/1272/24/53',
				'CUCC/SC/MY/136/1632/24/68'=>'CUCC/SC/MY/136/1632/24/68',
				'CUCC/SC/MY/166/1992/24/83'=>'CUCC/SC/MY/166/1992/24/83',
				'CUCC/SC/MY/196/2352/24/98'=>'CUCC/SC/MY/196/2352/24/98',
				'CUCC/SC/MY/296/3552/24/148'=>'CUCC/SC/MY/296/3552/24/148',
				'CUCC/SC/MY/68/816/24/34'=>'CUCC/SC/MY/68/816/24/34',
				'CUCC/SC/MY/98/1128/24/47'=>'CUCC/SC/MY/98/1128/24/47',
				'CUCC/SC/MY/158/1752/24/73'=>'CUCC/SC/MY/158/1752/24/73',
				'CTCC/SC/MY/59/720/24/30'=>'CTCC/SC/MY/59/720/24/30',
				'CTCC/SC/MY/79/960/24/40'=>'CTCC/SC/MY/79/960/24/40',
		);
	}
	
	public function updateByPK($data = array())
	{
		$data = $this->create($data);
		
		return $this->updateRecord($data);
	}
	
	public function deleteByPK($pk = 0)
	{
		return $this->deleteRecord($pk);
	}
	
	public function getSecretKey()
	{
		return '72969e04baa8b6e43fae34e1969d56a6';
	}
	
	public function checkSign($data)
	{
		$clientSign = $data['sign'];
		 
		$sign = $this->getSign($data);
		 
		return $clientSign === $sign;
	}
	
	public function getSign($data)
	{
		unset($data['sign']);
		 
		$sk = $this->getSecretKey();
		 
		ksort($data);
		$query = '';
		 
		foreach ($data as $key => $item)
		{
			$query .= "&{$key}={$item}";
		}
	
		$query = trim($query,'&');
		$query .= $sk;
	
		return md5($query);
	}
	
}