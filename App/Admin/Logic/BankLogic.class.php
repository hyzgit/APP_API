<?php
namespace Admin\Logic;

use Common\Model\BankModel;

class BankLogic extends BankModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	public function getPage($keyword = '',&$pageShow = null,$pageSize = 10,$parameter = array())
	{
		$where = array();
		
		if( empty($keyword) === false )
		{
			$where = array(
					'bank_title' => array('LIKE', "%{$keyword}%"),
					'bank_code' => array('LIKE',"%{$keyword}%"),
					'_logic' => 'OR'
			);
		}

		$totalRows = $this->where($where)->count();
	
		$order = 'bank_state DESC,bank_order ASC,bank_id DESC';
	
		$data = $this->where($where)->pagination($totalRows,$pageShow,$pageSize,$parameter)->order($order)->getItems();
	
		foreach ($data as $key => &$item)
		{
			$item['channel'] = get_format_state($item['bank_channel'], $this->getMapChannel());
			$item['state'] = get_format_state($item['bank_state'], $this->getMapState());
			$item['edit'] = get_format_url('edit', $item['bank_id']);
			$item['delete'] = get_format_url('delete', $item['bank_id']);
		}
	
		return $data;
	}
	
	public function getEdit($pk = 0)
	{
		return $this->getItem($pk);
	}
	
	public function updateByPK($data = array())
	{
		$data['state'] = $data['state'] == 1 ? 'NORMAL' : 'DISABLE';
		
		$data = $this->create($data);
		
		return $this->updateRecord($data);
	}
	
	public function deleteByPK($pk = 0)
	{
		return $this->deleteRecord($pk);
	}
	
	public function getMapChannel()
	{
		return array(
				"DEFAULT" => "默认",
				"YIJIFU" => "易极付",
		);
	}
	
	public function getMapState()
	{
		return array(
				'DISABLE'=>'禁用.text-red',
				'NORMAL'=>'正常'				
		);
	}
		
}