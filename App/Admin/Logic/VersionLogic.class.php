<?php
namespace Admin\Logic;

use Common\Model\VersionModel;
class VersionLogic extends VersionModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	public function getListByParm($where = array(),$order = 'version_status DESC,version_id ASC')
	{
		$data = $this->where($where)->order($order)->select();
	
		return $data;
	}
	
	public function updateVersion($data = array())
	{
		$map = array(
				'status' => 0,
				'notice' => 0,
		);
		
		$data = completion($data,$map);

		return $this->updateByPK($data);
	}
	
	//根据参数修改记录
	public function updateByParm($where = array(),$parm = array())
	{
		return $this->updateRecordByParm($where,$parm);
	}
	
	public function getEdit($pk = 0)
	{
		return $this->getItem($pk);
	}
	
	public function getPage($where = array(),&$pageShow = null,$pageSize = 10,$parameter = array())
	{
		$totalRows = $this->where($where)->count();
	
		$order = 'version_type_id DESC,version_platform ASC,version_code DESC,version_id ASC';
	
		$data = $this->where($where)->pagination($totalRows,$pageShow,$pageSize,$parameter)->order($order)->getItems();
		
		foreach ($data as $key => &$item)
		{
			$item['status'] = get_format_state($item['version_status']);
			$item['notice'] = get_format_notice($item['version_notice']);
			$item['edit'] = get_format_url('edit', $item['version_id']);
			$item['delete'] = get_format_url('delete', $item['version_id']);
			$item['description'] = get_short_str($item['version_description'],16,'utf-8');
			$item['message'] = get_short_str($item['version_message'],8,'utf-8');
		}
		
		return $data;
	}
	
	public function updateByPK($data = array())
	{
		$data = $this->create($data);
		return $this->updateRecord($data);
	}
	
	public function deleteByPK($pk = 0)
	{
		return $this->deleteRecord($pk);
	}
		
}