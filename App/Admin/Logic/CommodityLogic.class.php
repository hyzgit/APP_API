<?php
namespace Admin\Logic;

use Common\Model\CommodityModel;

class CommodityLogic extends CommodityModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	public function getPage($where = array(),&$pageShow = null,$pageSize = 10,$parameter = array())
	{
		$totalRows = $this->where($where)->count();
	
		$order = 'commodity_status DESC,commodity_order ASC,commodity_id DESC';
	
		$data = $this->where($where)->pagination($totalRows,$pageShow,$pageSize,$parameter)->order($order)->getItems();
	
		foreach ($data as $key => &$item)
		{
			$item['total_money'] = get_format_money($item['commodity_total_money']);
			$item['down_payment'] = get_format_money($item['commodity_down_payment']);
			$item['reduce_price'] = get_format_money($item['commodity_reduce_price']);
			$item['status'] = get_format_state($item['commodity_status'], array('DISABLE'=>'锁定.text-red','NORMAL'=>'正常'));
			$item['edit'] = get_format_url('edit', $item['commodity_id']);
			$item['delete'] = get_format_url('delete', $item['commodity_id']);
		}
	
		return $data;
	}
	
	public function getEdit($pk = 0)
	{
		return $this->getItem($pk);
	}
	
	public function updateByPK($data = array())
	{
		$data['status'] = $data['status'] == 1 ? 'NORMAL' : 'DISABLE';
		
		$map = array(
				'order' => 0,
		);
		
		$data = completion($data,$map);
		
		$data = $this->create($data,'both');
		
		return $this->updateRecord($data);
	}
	
	public function deleteByPK($pk = 0)
	{
		return $this->deleteRecord($pk);
	}
		
}