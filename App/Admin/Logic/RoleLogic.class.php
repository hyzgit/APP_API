<?php
namespace Admin\Logic;

use Common\Model\RoleModel;

class RoleLogic extends RoleModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	public function getListByParm($where = array(),$order = 'role_state DESC,role_order ASC,role_id ASC')
	{
		$data = $this->where($where)->order($order)->select();
		
		return $data;
	}
	
	/**
	 * 根据当前记录更新权限数据的默认选项
	 * @param unknown $role
	 * @param unknown $level
	 * @return unknown
	 */
	public function getLevelDefaultByRole($role = array(),$level = array())
	{
		$role = explode(',', $role['role_raw']);
		
		foreach ($level as $keys => &$items)
		{
			foreach ($items['level'] as $key => &$item)
			{
				$item['level_default'] = in_array($item['level_id'], $role) ? true :false;
			}
		}
		
		return $level;
	}
	
	public function updateRole($data = array())
	{
		$data['data'] = implode(array_values($data['raw']), ',');
		
		$data['data'] = trim($data['data'],',');
		
		$data['raw'] = implode(array_keys($data['raw']), ',');
		
		$data['raw'] = trim($data['raw'],',');
		
		$map = array(
				'order' => 1,
				'state' => 0,
		);
		
		$data = completion($data,$map);
		
		return $this->updateByPK($data);
	}
	
	public function getEdit($pk = 0)
	{
		return $this->getItem($pk);
	}
	
	public function getPage($where = array(),&$pageShow = null,$pageSize = 10,$parameter = array())
	{
		$totalRows = $this->where($where)->count();
		
		$order = 'role_state DESC,role_order ASC,role_id ASC';
		
		$data = $this->where($where)->pagination($totalRows,$pageShow,$pageSize,$parameter)->order($order)->getItems();
		
		foreach ($data as $key => &$item)
		{
			$item['desc'] = get_format_default($item['role_desc']);
			$item['state'] = get_format_state($item['role_state']);
			$item['edit'] = get_format_url('edit', $item['role_id']);
			$item['delete'] = get_format_url('delete', $item['role_id']);
		}
		
		return $data;
	}
	
	public function updateByPK($data = array())
	{
		$data = $this->create($data);
		
		return $this->updateRecord($data);
	}
	
}