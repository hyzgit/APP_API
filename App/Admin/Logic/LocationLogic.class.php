<?php

namespace Admin\Logic;

use Common\Model\LocationModel;

class LocationLogic extends LocationModel 
{
	protected function _initialize() 
	{
		parent::_initialize();
	}

	public function getPage($where = array(),&$pageShow = null,$pageSize = 10,$parameter=array())
	{
		if( !empty($where) )
		{
			$where = array(
					'location_store_name' => array('like','%' . $where . '%'),
					'location_store_code' => array('like','%' . $where . '%'),
					'_logic' => 'OR'
			);
		}
		
		$count =  $this->where($where)->count();
		
		$this->order('location_state DESC,location_lock DESC,location_id DESC');
		
		$data = $this->where($where)->pagination($count,$pageShow,$pageSize,$parameter)->getItems();
		
		foreach ($data as $key => &$item)
		{	
			$item['state'] = get_format_state($item['location_state'], array('DISABLE'=>'禁用.text-red','NORMAL'=>'正常.text-green'));
			$item['lock'] = get_format_state($item['location_lock'],array('YES'=>'是','NO'=>'否'));
			
			$item['delete'] = get_format_url('delete', $item['location_id']);
			$item['edit'] = get_format_url('edit', $item['location_id']);
		}
		
		return $data;
	}
	
	public function getEdit($pk = 0)
	{
		$item = $this->getItem($pk);
				
		return $item;
	}

	public function updateLocation($data = array()) 
	{
		$data['state'] = $data['state'] === '1' ? 'NORMAL' : 'DISABLE';
		$data['lock'] = $data['lock'] === '1' ? 'YES' :'NO';
		
		$map = array(
				'definition' => 0,
				'distance' => 0,
		);
		
		$data = completion($data,$map);
		
		$data = $this->create($data,'both');
		
		if( $data === false )
		{
			return false;
		}
		
		return $this->updateRecord($data);
	}
	
	public function deleteByPK($pk = 0)
	{
		return $this->deleteRecord($pk);
	}

}


