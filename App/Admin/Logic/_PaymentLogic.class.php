<?php

namespace Admin\Logic;

use Common\Model\PaymentModel;

class PaymentLogic extends PaymentModel {

    protected function _initialize() {
        parent::_initialize();
    }

    public function getPage($where = array(), &$pageShow = null, $pageSize = 10, $parameter = array()) {
        $totalRows = $this->where($where)->count();
        $order = 'payment_status DESC,payment_order ASC,payment_id ASC';
        $data = $this->where($where)->pagination($totalRows, $pageShow, $pageSize, $parameter)->order($order)->getItems();
        $type = $this->getType();
        foreach ($data as $key => &$item) {
            $item['status'] = get_format_state($item['payment_status']);
            $item['type'] = $type[$item['payment_type']];
            $item['edit'] = get_format_url('edit', $item['payment_id']);
            $item['delete'] = get_format_url('delete', $item['payment_id']);
        }

        return $data;
    }

    public function updatePayment($data = array()) {
        $map = array(
            'order' => 1,
            'status' => 0,
        );
        $data = completion($data, $map);

        return $this->updateByPK($data);
    }

    public function getEdit($pk = 0) {
        return $this->getItem($pk);
    }

    public function updateByPK($data = array()) {
        $data = $this->create($data);
        return $this->updateRecord($data);
    }

    public function deleteByPK($pk = 0) {
        return $this->deleteRecord($pk);
    }

    //获取贷款类型
    public function getLoanType() {
        return array(
            '贷款类型A',
            '贷款类型B',
            '贷款类型C',
        );
    }

    //获取商品分类
    public function getCommodityType() {
        return array(
            '商品分类A',
            '商品分类B',
        );
    }

    //获取分期数
    public function getStages() {
        $result = array();
        for ($i = 1; $i <= 24; $i++) {
            $result[] = $i;
        }
        return $result;
    }

    //获取首付类型
    public function getType() {
        return array(
            'CLOSURE' => '关闭',
            'SCALE' => '按比例',
            'QUOTA' => '按额度',
        );
    }

}
