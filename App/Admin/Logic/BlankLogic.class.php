<?php
namespace Admin\Logic;

use Common\Model\BlankModel;

class BlankLogic extends BlankModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	/**
	 * 列表
	 * @param unknown $where
	 * @param string $order
	 * @return mixed
	 */
	public function getList($where = array(),$order = 'blank_state DESC,blank_order ASC,blank_id ASC')
	{
		$data = $this->where($where)->order($order)->getItem();
		
		foreach ($data as $key => &$item)
		{
			$item['state'] = get_format_state($item['blank_state']);
		}
		
		return $data;
	}
	
	/**
	 * 分页
	 * @param unknown $where
	 * @param string $pageShow
	 * @param number $pageSize
	 * @param unknown $parameter
	 * @return mixed
	 */
	public function getPage($where = array(),&$pageShow = null,$pageSize = 10,$parameter=array())
	{
		$totalRows = $this->where($where)->count();
		
		$order = 'blank_state DESC,blank_id DESC';
		
		$data = $this->where($where)->pagination($totalRows,$pageShow,$pageSize,$parameter)->order($order)->getItems();
		
		foreach ($data as $key => &$item)
		{
			$item['state'] = get_format_state($item['blank_state']);
		}
		
		return $data;
	}
	
	/**
	 * 读取一条数据
	 * @access 编辑(edit)、详情(detail)
	 * @method getEdit
	 * @param number $pk
	 * @return mixed
	 */
	public function getEdit($pk = 0)
	{
		return $this->getItem($pk);
	}
	
	/**
	 * 按主键更新
	 * @method update[Test]ByPK
	 * @param unknown $data
	 * @return mixed
	 */
	public function updateTestByPK($data = array())
	{
		$data = $this->create($data);
		
		return $this->updateRecord($data);
	}
	
	/**
	 * 按条件更新
	 * @method update[Test]ByParm
	 * @param unknown $data
	 * @return mixed
	 */
	public function updateTestByParm($data = array())
	{
		$data = $this->create($data);
		
		return $this->updateRecordByParm($data);
	}
	
	/**
	 * 按主键删除
	 * @method deleteByPK
	 * @param number $pk
	 * @return mixed
	 */
	public function deleteByPK($pk = 0)
	{
		// 物理删除
		return $this->deleteRecord($pk);
		
		// 逻辑删除
		$data = array(
				'blank_delete' => true,
		);
		
		return $this->deleteRecord($pk,false,$data);
	}
	
	/**
	 * 按条件删除
	 * @method deleteByParm
	 * @param unknown $options
	 * @return mixed
	 */
	public function deleteByParm($options = array())
	{
		// 物理删除
		return $this->deleteRecord($options);
		
		// 逻辑删除
		$data = array(
				'blank_delete' => true,
		);
		
		return $this->deleteRecord($options,false,$data);
	}
}