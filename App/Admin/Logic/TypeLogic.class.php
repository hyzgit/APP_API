<?php
namespace Admin\Logic;

use Common\Model\TypeModel;
class TypeLogic extends TypeModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	public function getListByParm($where = array(),$order = 'type_state DESC,type_id ASC')
	{
		$data = $this->where($where)->order($order)->select();
	
		return $data;
	}
	
	public function updateType($data = array())
	{
		$map = array(
				'state' => 0,
				'order' => 1,
		);
		
		$data = completion($data,$map);
		
		return $this->updateByPK($data);
	}
	
	public function getPage($where = array(),&$pageShow = null,$pageSize = 10,$parameter = array())
	{
		$totalRows = $this->where($where)->count();
	
		$order = 'type_state DESC,type_order ASC,type_id ASC';
	
		$data = $this->where($where)->pagination($totalRows,$pageShow,$pageSize,$parameter)->order($order)->getItems();
		
		foreach ($data as $key => &$item)
		{
			$item['state'] = get_format_state($item['type_state']);
			$item['edit'] = get_format_url('edit', $item['type_id']);
			$item['delete'] = get_format_url('delete', $item['type_id']);
		}
		
		return $data;
	}

	public function getEdit($pk = 0)
	{
		return $this->getItem($pk);
	}
	
	public function updateByPK($data = array())
	{
		$data = $this->create($data);
		return $this->updateRecord($data);
	}
	
	public function deleteByPK($pk = 0)
	{
		return $this->deleteRecord($pk);
	}

		
}