<?php
namespace Admin\Controller;

class IndexController extends CommonController
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	public function indexAction()
	{
		return $this->display();
	}
}