<?php
namespace Admin\Controller;

abstract class CommonController extends BaseController
{
	protected $profile = array();
	
	protected function _initialize()
	{
		parent::_initialize();
		
		// 登录验证
		$manager = psession('manager');
		
		// 类内调用
		$this->profile = $manager;
		
		// 视图调用
		$this->assign('profile',$manager);
		
		if( isset($manager['manager_id']) === false )
		{
			redirect(U('Account/login',array('msg'=>'3001')));
		}
		
		// 权限验证
		if( C('AUTHENTICATION') )
		{
			if( permission(true) === false )
			{
				return $this->denyAccess();
			}
		}
	}
	
	private function denyAccess($message = '无权访问',$waitSecond = 10)
	{
		if( IS_AJAX )
		{
			$data['info'] = $message;
			
			$data['status'] = 0;
			
			$data['url'] = 'javascript:history.go(-1);';
			
			$this->ajaxReturn($data);
		}
		
		$this->assign('error',$message);
		
		$this->assign('waitSecond',$waitSecond);
		
		$this->assign('jumpUrl',"javascript:history.back(-1);");
		
		$this->display(C('TMPL_DENY_ACCESS'));
		
		exit();
	}
}