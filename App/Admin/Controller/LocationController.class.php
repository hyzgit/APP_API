<?php
namespace Admin\Controller;

use Admin\Logic\LocationLogic;

class LocationController extends CommonController
{
	private $location = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
	   $this->location = new LocationLogic();
	}
	
	public function pageAction()
	{
		$page  = null;
		$data  = array();
		
		$query = I('get.keyword',null,'trim,htmlspecialchars');
		
		$data = $this->location->getPage($query,$page,$this->pageSize);
	
		$this->assign('data',$data);
		
		$this->assign('keyword',$query);
		
		$this->assign('page',$page);
		
		$this->display();
	}
	
	public function formAction()
	{	
		return $this->display();
	}

	public function editAction()
	{
		$id = I('get.id', 0, 'intval');
		
		$data = $this->location->getEdit($id);
		
		$this->assign('data', $data);

		return $this->display();
	}

	public function saveAction() 
	{
		$data = I('post.');

		if ( $this->location->updateLocation($data) === false ) 
		{
			return $this->error($this->location->getError(), null, true);
		}
		
		return $this->success('操作成功', U('page'), true);
	}

	public function deleteAction()
	{
		$id = I('get.id', 0, 'intval');

		if ( $this->location->deleteByPk($id) === false ) 
		{
			return $this->error($this->location->getError(), null, true);
		}

		return $this->success('删除成功', U('page'), true);
	}
}