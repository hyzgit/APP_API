<?php

namespace Admin\Controller;

use Admin\Logic\MessageLogic;

class MessageController extends CommonController 
{
	private $message = null;

	protected function _initialize() 
	{
		parent::_initialize();

		$this->message = new MessageLogic();
	}
	
	public function pageAction()
	{
		$page = null;
		
		$search = I('get.search',null,'trim,htmlspecialchars');
		
		$data = $this->message->getPage($search,$page);
		
		$this->assign('search',$search);
		
		$this->assign('data',$data);
		
		$this->assign('page',$page);
		
		return $this->display();
	}
	
	public function detailAction()
	{
		$id = I('get.id',0,'intval');
		 
		$data = $this->message->getDetail($id);
		 
		$this->assign('data',$data);
		 
		return $this->display();
	}
	
	public function formAction()
	{
		$template = $this->message->getTemplate();
		
		$this->assign('template',$template);
		
		return $this->display();
	}
	
	public function sendAction()
	{
		$data = I('post.');
		
		$data = $this->message->setSendData($data);
		
		if( $data === false )
		{
			return $this->error($this->message->getError(), null, true);
		}
		
		if( $this->message->sendData($data) === false )
		{
			return $this->error($this->message->getError(), null, true);
		}
		
		return $this->success('操作成功');
	}
}
