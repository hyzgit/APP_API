<?php
namespace Admin\Controller;

use Admin\Logic\VersionLogic;
use Admin\Logic\TypeLogic;

/**	
 * APP版本信息
 * @author yoko
 */
class VersionController extends CommonController
{
	private $version = null;
	
	private $type = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->version = new VersionLogic();
		
		$this->type = new TypeLogic();
	}
	
	public function formAction()
	{
		$type = $this->type->getListByParm(array('type_state'=>1));
		
		$this->assign('type',$type);
		
		return $this->display();
	}
	
	public function editAction()
	{
		$id = I('get.id',0,'intval');

		$type = $this->type->getListByParm(array('type_state'=>1));
		
		$data = $this->version->getEdit($id);
		
		$this->assign('data',$data);

		$this->assign('type',$type);
		
		return $this->display();
	}
	
	public function saveAction()
	{
		$data = I('post.');

		$type = $this->type->getEdit($data['type_id']);
		
		$data['type'] = $type['type_name'];
		$data['type_id'] = $type['type_id'];
		$data['name'] = $type['type_desc'];
		
		if( $this->version->updateVersion($data) === false )
		{
			return $this->error($this->version->getError(),null,true);
		}
		
		return $this->success('操作成功',U('page'),true);
	}
	
	public function deleteAction()
	{
		$id = I('get.id',0,'intval');
		
		if( $this->version->deleteByPk($id) === false )
		{
			return $this->error($this->version->getError(),null,true);
		}
		
		return $this->success('删除成功',U('page'),true);
	}
	
	public function pageAction()
	{
		$page = null;

		$data = $this->version->getPage(array(),$page,$this->pageSize);
		
		$this->assign('data',$data);
		$this->assign('page',$page);
		
		return $this->display();
	}
}