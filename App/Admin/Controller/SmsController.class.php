<?php

namespace Admin\Controller;

use Admin\Logic\SmsLogic;

class SmsController extends CommonController 
{

    private $sms = null;

    protected function _initialize() 
    {
        parent::_initialize();

        $this->sms = new SmsLogic();
    }
    
	public function pageAction()
	{
	    $page = null;
	    
	    $search = I('get.search',null,'trim,htmlspecialchars');
	    
	    $data = $this->sms->getPage($search,$page);
	    
	    $this->assign('search',$search);
	    
	    $this->assign('data',$data);
	    
	    $this->assign('page',$page);
	    
		return $this->display();
	}
	
	public function detailAction()
	{
		$id = I('get.id',0,'intval');
		 
		$data = $this->sms->getEdit($id);
		 
		$this->assign('data',$data);
		 
		return $this->display();
	}
	
	
}
