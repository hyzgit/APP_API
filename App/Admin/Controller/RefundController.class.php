<?php
namespace Admin\Controller;

use Admin\Logic\RefundLogic;

class RefundController extends CommonController
{
	private $refund = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
	   $this->refund = new RefundLogic();
	}
	
	public function pageAction()
	{
		$page  = null;
		$query = array();
		$data  = array();
		
		$query = I('get.');
		
		if( empty($query) === false )
		{
			$data = $this->refund->getPage($query,$page,$this->pageSize);
		
			$this->assign('data',$data);
				
			$this->assign('search',$query);
				
			$this->assign('page',$page);
				
			return $this->display();
		}
		else
		{
			$data['status_list'] = $this->refund->getStatus();
				
			$data['contract_status_list'] = $this->refund->getContractStatus();
		
			$data['refund_start_time'] = date("Y-m-d",time()-86400);
		
			$data['refund_end_time'] = date("Y-m-d",time()-86400);
		
			$this->assign('data',$data);
		
			return $this->display('search');
		}
	}
	
	public function detailAction()
	{
		$id = I('get.id',0,'intval');
	
		$data = $this->refund->getEdit($id);
	
		$this->assign('data',$data);
	
		return $this->display();
	}
	
	public function auditAction()
	{
		$id = I('get.id',0,'intval');
	
		$data = $this->refund->getEdit($id);
	
		$this->assign('data',$data);
	
		return $this->display();
	}
	
	public function auditdoAction()
	{
		$data = I('post.');
	
		if( $this->refund->auditByPK($data) === false )
		{
			return $this->error($this->refund->getError(),null,true);
		}
	
		return $this->success('操作成功',true);
	}
	
	public function lockAction()
	{
		$data['id'] = I('get.id',0,'intval');
	
		$data['state'] = I('get.state',0,'intval') == 0 ? 'DISABLE' : 'NORMAL';
	
		if( $this->refund->updateByPK($data) === false )
		{
			return $this->error($this->refund->getError(),null,true);
		}
	
		return $this->success('操作成功');
	}
	
	public function downloadAction()
	{
		$search = I('get.');
		
		$data = $this->refund->getDownLoad($search);
		
		$header_data = array(
				'合同编号',
				'姓名',
				'身份证号',
				'手机号',
				'退款提交时间',
				'退款金额(元)',
				'打款完成时间',
				'合同状态',
				'审核状态',
				'状态',
		);
				
		$this->export_csv_2($data,$header_data,sprintf('%s.csv','退款记录' . date('YmdHis')));
	}
	
	public function export_csv_2($data = [], $header_data = [], $file_name = '')
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='.$file_name);
		header('Cache-Control: max-age=0');
		$fp = fopen('php://output', 'a');
		if (!empty($header_data)) {
			foreach ($header_data as $key => $value) {
				$header_data[$key] = iconv('utf-8', 'gbk', $value);
			}
			fputcsv($fp, $header_data);
		}
		$num = 0;
		//每隔$limit行，刷新一下输出buffer，不要太大，也不要太小
		$limit = 100000;
		//逐行取出数据，不浪费内存
		$count = count($data);
		if ($count > 0) {
			for ($i = 0; $i < $count; $i++) {
				$num++;
				//刷新一下输出buffer，防止由于数据过多造成问题
				if ($limit == $num)
				{
					ob_flush();
					flush();
					$num = 0;
				}
				$row = $data[$i];
				foreach ($row as $key => $value) {
					$row[$key] = iconv('utf-8', 'gbk', $value);
				}
				fputcsv($fp, $row);
			}
		}
		fclose($fp);
	}
}