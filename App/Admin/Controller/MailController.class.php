<?php
namespace Admin\Controller;

class MailController extends UnCommonController
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	public function pageAction()
	{
		return $this->display();
	}
	
	public function sendMailAction()
	{
		return parent::stmpSendAction();
	}
	
	public function sendAction()
	{
		$email = '1126237473@qq.com';
		$title = 'test method 2';  //标题
		$content = 'test content 2';  //邮件内容
		$result =  sendMail($email,$title,$content); //直接调用发送即可

		if(empty($result)){
			echo 'error';
		}
		else
		{
			echo 'success';
		}
	}
}