<?php
namespace Admin\Controller;

use Admin\Logic\CommodityLogic;

class CommodityController extends CommonController 
{

	private $commodity = null;

	protected function _initialize() 
	{
		parent::_initialize();

		$this->commodity = new CommodityLogic();
	}
	
	public function pageAction()
	{
		$page = null;
	
		$data = $this->commodity->getPage(array(),$page,$this->pageSize);
	
		$this->assign('data',$data);
	
		$this->assign('page',$page);
	
		return $this->display();
	}
	
	public function formAction()
	{	
		return $this->display();
	}

	public function editAction()
	{
		$id = I('get.id', 0, 'intval');
		
		$data = $this->commodity->getEdit($id);
		
		$this->assign('data', $data);

		return $this->display();
	}

	public function saveAction() 
	{
		$data = I('post.');

		if ($this->commodity->updateByPK($data) === false) 
		{
			return $this->error($this->commodity->getError(), null, true);
		}
		
		return $this->success('操作成功', U('page'), true);
	}

	public function deleteAction()
	{
		$id = I('get.id', 0, 'intval');

		if ($this->commodity->deleteByPk($id) === false) 
		{
			return $this->error($this->commodity->getError(), null, true);
		}

		return $this->success('删除成功', U('page'), true);
	}

}
