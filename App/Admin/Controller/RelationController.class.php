<?php
namespace Admin\Controller;

use Admin\Logic\RelationLogic;

class RelationController extends CommonController 
{

	private $relation = null;

	protected function _initialize() 
	{
		parent::_initialize();

		$this->relation = new RelationLogic();
	}
	
	public function pageAction()
	{
		$page = null;
		
		$where['keyword'] = I('get.keyword',null,'trim,htmlspecialchars');
		
		$data = $this->relation->getPage($where,$page,$this->pageSize);
	
		$this->assign('keyword',$where['keyword']);
		
		$this->assign('data',$data);
	
		$this->assign('page',$page);
	
		return $this->display();
	}
	
	public function formAction()
	{	
		return $this->display();
	}

	public function editAction()
	{
		$id = I('get.id', 0, 'intval');
		
		$data = $this->relation->getEdit($id);
		
		$this->assign('data', $data);

		return $this->display();
	}

	public function saveAction() 
	{
		$data = I('post.');

		if ($this->relation->updateByPK($data) === false) 
		{
			return $this->error($this->relation->getError(), null, true);
		}
		
		return $this->success('操作成功', U('page'), true);
	}

	public function deleteAction()
	{
		$id = I('get.id', 0, 'intval');

		if ($this->relation->deleteByPk($id) === false) 
		{
			return $this->error($this->relation->getError(), null, true);
		}

		return $this->success('删除成功', U('page'), true);
	}

}
