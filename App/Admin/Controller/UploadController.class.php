<?php
namespace Admin\Controller;

class UploadController extends CommonController
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	public function fileAction()
	{
		return $this->display();
	}
	
	public function saveFileAction()
	{
		return parent::saveFileAction();
	}
	
	public function umeditorAction()
	{
		return parent::umeditorAction();
	}
}