<?php
namespace Admin\Controller;

use Admin\Logic\CancelLogic;

class CancelController extends CommonController
{
	private $cancel = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
	   $this->cancel = new CancelLogic();
	}
	
	public function pageAction()
	{
		$page  = null;
		$query = array();
		$data  = array();
		
		$query = I('get.');
		
		if( empty($query) === false )
		{
			$data = $this->cancel->getPage($query,$page,$this->pageSize);
		
			$this->assign('data',$data);
			
			$this->assign('search',$query);
			
			$this->assign('page',$page);
			
			return $this->display();
		}
		else 
		{	
			$data['status_list'] = $this->cancel->getStatus();
			
			$data['contract_status_list'] = $this->cancel->getContractStatus();
				
			$data['start_time'] = date("Y-m-d",time()-86400);
				
			$data['end_time'] = date("Y-m-d",time()-86400);
		
			$this->assign('data',$data);
				
			return $this->display('search');
		}
	}
	
	public function detailAction()
	{
		$id = I('get.id',0,'intval');
	
		$data = $this->cancel->getEdit($id);
	
		$this->assign('data',$data);
	
		return $this->display();
	}
	
	public function auditAction()
	{
		$id = I('get.id',0,'intval');
	
		$data = $this->cancel->getEdit($id);
	
		$this->assign('data',$data);
	
		return $this->display();
	}
	
	public function auditdoAction()
	{
		$data = array();
		
		$data = I('post.');
	
		if( $this->cancel->auditByPK($data) === false )
		{
			return $this->error($this->cancel->getError(),null,true);
		}
	
		return $this->success('操作成功');
	}
	
	public function lockAction()
	{
		$data['id'] = I('get.id',0,'intval');
	
		$data['state'] = I('get.state',0,'intval') == 0 ? 'DISABLE' : 'NORMAL';
	
		if( $this->cancel->updateByPK($data) === false )
		{
			return $this->error($this->cancel->getError(),null,true);
		}
	
		return $this->success('操作成功');
	}
	
	public function downloadAction()
	{
		$search = I('get.');
		
		$data = $this->cancel->getDownLoad($search);
		
		$header_data = array(
				'合同编号',
				'姓名',
				'身份证号',
				'手机号',
				'校对状态',
				'合同状态',
				'撤销提交时间',
				'审核状态',
				'状态',
		);
	
		$this->export_csv_2($data,$header_data,sprintf('%s.csv','撤销记录' . date('YmdHis')));
	}
	
	public function export_csv_2($data = [], $header_data = [], $file_name = '')
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='.$file_name);
		header('Cache-Control: max-age=0');
		$fp = fopen('php://output', 'a');
		if (!empty($header_data)) {
			foreach ($header_data as $key => $value) {
				$header_data[$key] = iconv('utf-8', 'gbk', $value);
			}
			fputcsv($fp, $header_data);
		}
		$num = 0;
		//每隔$limit行，刷新一下输出buffer，不要太大，也不要太小
		$limit = 100000;
		//逐行取出数据，不浪费内存
		$count = count($data);
		if ($count > 0) {
			for ($i = 0; $i < $count; $i++) {
				$num++;
				//刷新一下输出buffer，防止由于数据过多造成问题
				if ($limit == $num) {
					ob_flush();
					flush();
					$num = 0;
				}
				$row = $data[$i];
				foreach ($row as $key => $value) {
					$row[$key] = iconv('utf-8', 'gbk', $value);
				}
				fputcsv($fp, $row);
			}
		}
		fclose($fp);
	}
}