<?php
namespace Admin\Controller;

use Admin\Logic\QrcodeLogic;
use Think\Crypt\Driver\Base64;

class QrcodeController extends CommonController
{
	private $qrcode = null;
	//容错级别
	private $level = 3;
	//生成图片大小
    private $size = 8; 
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->qrcode = new QrcodeLogic();
	}
	
	public function pageAction()
	{
		$page = null;
		
		$data = $this->qrcode->getPage(array(),$page,$this->pageSize);
		
		$this->assign('data',$data);
		
		$this->assign('page',$page);
	
		return $this->display();
	}
	
	public function formAction()
	{
		$loan_type = $this->qrcode->getLoanType();
		
		$commodity_type = $this->qrcode->getCommodityType();
		
		$this->assign('loan_type',$loan_type);
		
		$this->assign('commodity_type',$commodity_type);
		
		
		return $this->display();
	}
	
	public function editAction()
	{
		$id = I('get.id',0,'intval');
		
		$loan_type = $this->qrcode->getLoanType();
		
		$commodity_type = $this->qrcode->getCommodityType();
		
		$data = $this->qrcode->getEdit($id);
		
		$this->assign('data',$data);
		
		$this->assign('loan_type',$loan_type);
		
		$this->assign('commodity_type',$commodity_type);
		
		return $this->display();
	}
	
	public function saveAction()
	{
		$data = I('post.');
		
		if( $this->qrcode->updateQrcode($data) === false )
		{
			return $this->error($this->qrcode->getError(),null,true);
		}
	
		return $this->success('操作成功',U('page'),true);
	}
	
	public function qrcodeAction()
	{
		$id = I('get.id');
		
		$data = $this->qrcode->getEdit($id);
		
		$data['sign'] = $this->qrcode->getSign($data);
		
		$this->assign('data',$data);
		
		return $this->display();
	}	
	
	public function qrcodeimgAction()
	{
		$id = I('get.id');

		$info = $this->qrcode->getEdit($id);
		
		$info['sign'] = I('get.sign');
		
		if($this->qrcode->checkSign($info)===false) 
		{
			return false;
		}  
		
		$info = $this->qrcode->getQrcodedata($info);
		
		$info = base64_decode($info);
		
		Vendor('phpqrcode.phpqrcode');
			
		//生成二维码图片
		$object = new \QRcode();
			
		$png = $object->png($info, false, $this->level, $this->size, 2);
		
	}
	
	public function deleteAction()
	{
		$id = I('get.id',0,'intval');
	
		if( $this->qrcode->deleteByPk($id) === false )
		{
			return $this->error($this->qrcode->getError(),null,true);
		}
	
		return $this->success('删除成功',U('page'),true);
	}
	
}