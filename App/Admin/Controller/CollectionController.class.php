<?php
namespace Admin\Controller;

use Admin\Logic\CollectionLogic;

class CollectionController extends CommonController
{
	private $colletion = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->colletion = new CollectionLogic();
	}
	
	public function pageAction()
	{
		$page = null;
		$data = array();
		
		$query = I('get.keyword',null,'trim,htmlspecialchars');
		
		$data = $this->colletion->getPage($query,$page,$this->pageSize);
	
		$this->assign('data',$data);
		
		$this->assign('keyword',$query);
		
		$this->assign('page',$page);
		
		$this->display();
	}
	
	public function detailAction()
	{
		$id = I('get.id', 0, 'intval');
	
		$data = $this->colletion->getEdit($id);
	
		$this->assign('data', $data);
	
		return $this->display();
	}
	
	public function formAction()
	{	
		return $this->display();
	}

	public function editAction()
	{
		$id = I('get.id', 0, 'intval');
		
		$data = $this->colletion->getEdit($id);
		
		$this->assign('data', $data);

		return $this->display();
	}

	public function saveAction() 
	{
		$data = I('post.');

		if ( $this->colletion->updateCollection($data) === false ) 
		{
			return $this->error($this->colletion->getError(), null, true);
		}
		
		return $this->success('操作成功', U('page'), true);
	}

	public function deleteAction()
	{
		$id = I('get.id', 0, 'intval');

		if ( $this->colletion->deleteByPk($id) === false ) 
		{
			return $this->error($this->colletion->getError(), null, true);
		}

		return $this->success('删除成功', U('page'), true);
	}
}