<?php
namespace Admin\Controller;

use Admin\Logic\TypeLogic;
use Admin\Logic\VersionLogic;
/**	
 * 业务类型
 * @author yoko
 */
class TypeController extends CommonController
{
	
	private $type = null;
	
	private $version = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->type = new TypeLogic();
		
		$this->version = new VersionLogic();
	}
	
	public function formAction()
	{	
		return $this->display();
	}
	
	public function editAction()
	{
		$id = I('get.id',0,'intval');
		
		$data = $this->type->getEdit($id);
		
		$this->assign('data',$data);
		
		return $this->display();
	}
	
	public function saveAction()
	{
		$data = I('post.');
		
		if( $this->type->updateType($data) === false )
		{
			return $this->error($this->type->getError(),null,true);
		}
		
		$this->version->updateByParm($where = array('version_type_id'=>$data['id']),$parm = array('version_name'=>$data['desc']));
		
		return $this->success('操作成功',U('page'),true);
	}
	
	public function deleteAction()
	{
		$id = I('get.id',0,'intval');
		
		if( $this->type->deleteByPk($id) === false )
		{
			return $this->error($this->type->getError(),null,true);
		}
		
		return $this->success('删除成功',U('page'),true);
	}
	
	public function pageAction()
	{
		$page = null;

		$data = $this->type->getPage(array(),$page,$this->pageSize);
		
		$this->assign('data',$data);
		$this->assign('page',$page);
		
		return $this->display();
	}
}