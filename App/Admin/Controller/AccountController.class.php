<?php
namespace Admin\Controller;

use Admin\Logic\ManagerLogic;

class AccountController extends UnCommonController
{
	private $manager = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->manager = new ManagerLogic();
	}
	
	public function indexAction()
	{
		redirect(U('login',I('get.')));
	}
	
	/**
	 * 请求方法路由
	 */
	public function loginAction()
	{
		return $this->methodRoute();
	}
	
	protected function getLoginAction()
	{
		$message = I('get.msg');
		
		if( empty($message) == false )
		{
			$this->message = $this->getStateByCode($message);
		}
		
		return $this->display();
	}
	
	protected function postLoginAction()
	{
		$data = I('post.');
		
		if( $this->manager->login($data) === false )
		{
			$this->error($this->manager->getError(),null,true);
		}
		
		return $this->success('登录成功',U('Index/index'),true);
	}
	
	public function logoutAction()
	{
		psession('manager',null);
		
		return redirect(U('Account/login',array('msg'=>'2001')));
	}
}