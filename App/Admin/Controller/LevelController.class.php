<?php
namespace Admin\Controller;

use Admin\Logic\LevelLogic;

class LevelController extends CommonController
{
	private $level = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->level = new LevelLogic();
	}
	
	public function formAction()
	{
		$level = $this->level->getListByParm(array('level_pid'=>0,'level_state'=>1));
		
		$this->assign('level',$level);
		
		return $this->display();
	}
	
	public function editAction()
	{
		$id = I('get.id',0,'intval');
		
		$level = $this->level->getListByParm(array('level_pid'=>0,'level_state'=>1));
		
		$data = $this->level->getEdit($id);
		
		$this->assign('level',$level);
		
		$this->assign('data',$data);
		
		return $this->display();
	}
	
	public function saveAction()
	{
		$data = I('post.');
		
		if( $this->level->updateLevel($data) === false )
		{
			return $this->error($this->level->getError(),null,true);
		}
		
		return $this->success('操作成功',U('page'),true);
	}
	
	public function deleteAction()
	{
		$id = I('get.id',0,'intval');
		
		if( $this->level->deleteByPk($id) === false )
		{
			return $this->error($this->level->getError(),null,true);
		}
		
		return $this->success('删除成功',U('page'),true);
	}
	
	public function pageAction()
	{
		$data = $this->level->getList();
		
		$this->assign('data',$data);
		
		return $this->display();
	}
	
}