<?php

namespace Admin\Controller;

use Admin\Logic\PaymentLogic;

/** 	
 * 首付设置
 */
class PaymentController extends CommonController {

    private $payment = null;

    protected function _initialize() {
        parent::_initialize();
        $this->payment = new PaymentLogic();
    }

    public function formAction() {
        $this->assign('loanType', $this->payment->getLoanType());
        $this->assign('commodityType', $this->payment->getCommodityType());
        $this->assign('stages', $this->payment->getStages());
        $this->assign('type', $this->payment->getType());
        return $this->display();
    }

    public function editAction() {
        $id = I('get.id', 0, 'intval');
        $data = $this->payment->getEdit($id);
        $this->assign('data', $data);
        $this->assign('loanType', $this->payment->getLoanType());
        $this->assign('commodityType', $this->payment->getCommodityType());
        $this->assign('stages', $this->payment->getStages());
        $this->assign('type', $this->payment->getType());
        return $this->display();
    }

    public function saveAction() {
        $data = I('post.');

        if ($this->payment->updatePayment($data) === false) {
            return $this->error($this->payment->getError(), null, true);
        }
        return $this->success('操作成功', U('page'), true);
    }

    public function deleteAction() {
        $id = I('get.id', 0, 'intval');

        if ($this->payment->deleteByPk($id) === false) {
            return $this->error($this->payment->getError(), null, true);
        }

        return $this->success('删除成功', U('page'), true);
    }

    public function pageAction() {
        $page = null;
        $data = $this->payment->getPage(array(), $page, $this->pageSize);
        $this->assign('data', $data);
        $this->assign('page', $page);
        return $this->display();
    }

}
