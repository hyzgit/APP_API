<?php

namespace Admin\Controller;

use Admin\Logic\ContractLogic;

/** 	
 * 合同支付管理
 */
class ContractController extends CommonController 
{

	private $contract = null;

	protected function _initialize() 
	{
		parent::_initialize();

		$this->contract = new ContractLogic();
	}

	public function editAction() 
	{
		$id = I('get.id', 0, 'intval');
		
		$data = $this->contract->getEdit($id);
		
		$data['time'] = get_format_date($data['contract_time']);
		
		$data['amount']  = get_format_money($data['contract_amount']/100);
		
		$data['total_money']  = get_format_money($data['contract_total_money']);
		
		$data['down_pay']  = get_format_money($data['contract_down_pay']);
		
		$this->assign('data', $data);

		return $this->display();
	}

	public function saveAction() 
	{
		$data = I('post.');

		if ($this->contract->updateContract($data) === false) 
		{
			return $this->error($this->contract->getError(), null, true);
		}
		
		return $this->success('操作成功', U('page'), true);
	}

	public function deleteAction() 
	{
		$id = I('get.id', 0, 'intval');

		if ($this->contract->deleteByPk($id) === false) 
		{
			return $this->error($this->contract->getError(), null, true);
		}

		return $this->success('删除成功', U('page'), true);
	}

	public function pageAction() 
	{
		$page = null;
		
		$query = array();
		
		$query = I('get.');
	
		if(empty($query) === false)
		{
			$data = $this->contract->getPage($query, $page, $this->pageSize,$parameter='');
			
			$this->assign('data', $data);
			
			$this->assign('page', $page);
			 
			$status_list = $this->contract->getAllStatus();
			
			$this->assign('status_list',$status_list);
			
			$this->assign('status',$query['status']);
			
			$this->assign('query',$query);
			
			return $this->display('search');
			
		} else {
			
			$data['status_list'] = $this->contract->getAllStatus();
			
			$data['start_time'] = date("Y-m-d",time()-86400);
			
			$data['end_time'] = date("Y-m-d",time()-86400);
			 
			$this->assign('data',$data);
			
			return $this->display();
		}
	}

}
