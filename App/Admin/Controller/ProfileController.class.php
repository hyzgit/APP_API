<?php
namespace Admin\Controller;

use Admin\Logic\ManagerLogic;

class ProfileController extends CommonController
{
	private $manager = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->manager = new ManagerLogic();
	}
	
	/**
	 * 修改密码
	 */
	public function passwordAction()
	{
		return $this->methodRoute();
	}
	
	protected function getPasswordAction()
	{
		$this->assign('data',$this->profile);
		
		return $this->display();
	}
	
	protected function postPasswordAction()
	{
		$data = array(
				'id' => $this->profile['manager_id'],
				'userpass' => I('post.userpass'),
				'password' => I('post.password'),
		);
		
		if( $this->manager->updatePassword($data,'password') === false )
		{
			return $this->error($this->manager->getError(),null,true);
		}
		
		return $this->success('操作成功',U('Index/index'),true);
	}
	
}