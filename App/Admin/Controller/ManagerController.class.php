<?php
namespace Admin\Controller;

use Admin\Logic\RoleLogic;
use Admin\Logic\ManagerLogic;
use Admin\Logic\LevelLogic;

class ManagerController extends CommonController
{
	private $role = null;
	
	private $level = null;
	
	private $manager = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->role = new RoleLogic();
		
		$this->level = new LevelLogic();
		
		$this->manager = new ManagerLogic();
	}
	
	/**
	 * 新增管理员
	 */
	public function formAction()
	{
		$role = $this->role->getListByParm(array('role_state'=>1));
		
		$this->assign('role',$role);
		
		return $this->display();
	}
	
	/**
	 * 更新管理员
	 */
	public function saveAction()
	{
		$data = I('post.');
		
		$role = $this->role->getByRoleId($data['role_id']);
		
		if( empty($role) )
		{
			return $this->error('【所属分组】无效，可能当前分组下无可用权限',null,true);
		}
		
		$data['role_name'] = $role['role_name'];
		
		$data = $this->manager->updateManager($data);
		
		if( $data === false )
		{
			return $this->error($this->manager->getError(),null,true);
		}
		
		return $this->success('操作成功',U('page'),true);
	}
	
	/**
	 * 编辑管理员：昵称、权限
	 */
	public function editAction()
	{
		$id = I('get.id',0,'intval');
		
		$data = $this->manager->getEdit($id);
		
		$role = $this->role->getListByParm(array('role_state'=>1));
		
		$this->assign('role',$role);
		
		$this->assign('data',$data);
		
		return $this->display();
	}
	
	/**
	 * 重置密码
	 */
	public function resetpasswordAction()
	{
		return $this->methodRoute();
	}
	
	protected function getResetpasswordAction()
	{
		$id = I('get.id',0,'intval');
		
		$data = $this->manager->getEdit($id);
		
		$this->assign('data',$data);
		
		return $this->display();
	}
	
	protected function postResetpasswordAction()
	{
		$data = I('post.');
		
		if( password_check($data['pass'], $this->profile['manager_userpass'],$this->profile['manager_usersalt']) === false )
		{
			return $this->error('【密码验证】有误',null,true);
		}
		
		if( $this->manager->updatePassword($data,'resetpassword') === false )
		{
			return $this->error($this->manager->getError(),null,true);
		}
		
		return $this->success('操作成功',U('page'),true);
	}
	
	public function levelAction()
	{
		$this->methodRoute();
		
		$id = I('get.id',0,'intval');
		
		$manager = $this->manager->getEdit($id);
		
		$role = $this->role->getEdit($manager['manager_role_id']);
		
		$level = $this->level->getListByParm(array('level_state'=>1));
		
		// 根据当前用户已有权限（可修改）、所在组别权限（disabled），生成新的权限树
		$level = $this->manager->getListLevelByRoleAndManager($level,$role,$manager);
		
		$this->assign('role',$role);
		
		$this->assign('level',$level);
		
		$this->assign('data',$manager);
		
		return $this->display();
	}
	
	public function postLevelAction()
	{
		$data = array(
				'id' => I('post.id',0,'intval'),
				'role_raw' => I('post.raw'),
		);
		
		if( $this->manager->updateLevel($data) === false )
		{
			return $this->error($this->manager->getError(),null,true);
		}
		
		return $this->success('操作成功',U('Index/index'),true);
	}
	
	/**
	 * 管理员列表
	 */
	public function pageAction()
	{
		$page = null;
		
		$data = $this->manager->getPage(array(),$page,$this->pageSize);
		
		$this->assign('data',$data);
		
		$this->assign('page',$page);
		
		return $this->display();
	}
	
	/**
	 * 删除账户
	 */
	public function deleteAction()
	{
		$id = I('get.id',0,'intval');
		
		return $this->success('请尽量使用锁定账户替代删除账户功能',U('page'),true);
		
		if( $this->manager->deleteByPK($id) === false )
		{
			return $this->error($this->manager->getError(),null,true);
		}
		
		return $this->success('删除成功',U('page'),true);
	}
}