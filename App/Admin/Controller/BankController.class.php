<?php
namespace Admin\Controller;

use Admin\Logic\BankLogic;

class BankController extends CommonController 
{
	private $bank = null;

	protected function _initialize() 
	{
		parent::_initialize();

		$this->bank = new BankLogic();
	}
	
	public function pageAction()
	{
		$page = null;
		
		$keyword = I('get.keyword',null,'trim,htmlspecialchars');
		
		$data = $this->bank->getPage($keyword,$page,$this->pageSize);
	
		$this->assign('keyword',$keyword);
		
		$this->assign('data',$data);
	
		$this->assign('page',$page);
	
		return $this->display();
	}
	
	public function formAction()
	{
		$map['channel'] = $this->bank->getMapChannel();
		
		$this->assign('map',$map);
		
		return $this->display();
	}

	public function editAction()
	{
		$id = I('get.id', 0, 'intval');
		
		$data = $this->bank->getEdit($id);
		
		$map['channel'] = $this->bank->getMapChannel();
		
		$this->assign('map',$map);

		$this->assign('data', $data);

		return $this->display();
	}

	public function saveAction() 
	{
		$data = I('post.');

		if ($this->bank->updateByPK($data) === false) 
		{
			return $this->error($this->bank->getError(), null, true);
		}
		
		return $this->success('操作成功', U('page'), true);
	}

	public function deleteAction()
	{
		$id = I('get.id', 0, 'intval');

		if( $this->bank->deleteByPk($id) === false )
		{
			return $this->error($this->bank->getError(), null, true);
		}

		return $this->success('删除成功', U('page'), true);
	}

}
