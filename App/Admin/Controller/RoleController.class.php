<?php
namespace Admin\Controller;

use Admin\Logic\LevelLogic;
use Admin\Logic\RoleLogic;

class RoleController extends CommonController
{
	private $level = null;
	
	private $role = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->level = new LevelLogic();
		
		$this->role = new RoleLogic();
	}
	
	public function formAction()
	{
		$level = $this->level->getListByParm(array('level_state'=>1));
		
		$this->assign('level',$level);
		
		return $this->display();
	}
	
	public function editAction()
	{
		$id = I('get.id',0,'intval');
		
		$data = $this->role->getEdit($id);
		
		$level = $this->level->getListByParm(array('level_state'=>1));
		
		$level = $this->role->getLevelDefaultByRole($data,$level);
		
		$this->assign('level',$level);
		
		$this->assign('data',$data);
		
		return $this->display();
	}
	
	public function saveAction()
	{
		$data = I('post.');
		
		if( $this->role->updateRole($data) === false )
		{
			return $this->error($this->role->getError(),null,true);
		}
		
		return $this->success('操作成功',U('page'),true);
	}
	
	public function deleteAction()
	{
		$id = I('get.id',0,'intval');
		
		if( $this->level->deleteByPk($id) === false )
		{
			return $this->error($this->level->getError(),null,true);
		}
		
		return $this->success('删除成功',U('page'),true);
	}
	
	public function pageAction()
	{
		$data = $this->role->getPage();
		
		$this->assign('data',$data);
		
		return $this->display();
	}
	
}