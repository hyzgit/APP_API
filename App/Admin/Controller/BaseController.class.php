<?php
namespace Admin\Controller;

use Common\Controller\BasicController;

abstract class BaseController extends BasicController
{
	protected function _initialize()
	{
		parent::_initialize();
	}
	
	public function getStateByCode($code = null)
	{
	    $code = intval($code);
	    
	    $data = array(
	        2001 => '安全退出',
	        3001 => '登录超时',
	    );
	    
	    return empty($data[$code]) ? $code . ':未知错误' : $data[$code];
	}
}