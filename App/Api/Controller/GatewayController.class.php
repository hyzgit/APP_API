<?php
namespace Api\Controller;

/**
 * 接口入口调试demo
 * @author hyz
 *
 */
class GatewayController extends CommonController
{	
	public $headers = array();
	
	public $body = array();
	
	public $required = array('app_id','service','version','data','sign');
	
	public $request_method = array('post','get');
	
	public function indexAction()
	{
		// 得到http请求header信息
		$this->headers = $this->getHeaders();
		
		//TODO 处理body字符集编码$headers['charset']
		if( iconv($this->headers['charset'], "UTF-8", $this->body) === false )
		{
			$this->getResponseData('CHARSET_ERROR');
		}
		
		// 判断是否支持该请求方法
		
		//TODO 判定http请求方法$headers['REQUEST_METHOD']
		switch ($this->headers['REQUEST_METHOD']) {
			case 'GET':
				$this->headers['format'] = 'form';
				
				$this->body = I('get.');				
			break;
			
			case 'POST':
				// 判断参数格式 form、json、xml
				if( isset($this->headers['format']) === false )
				{
					if( $this->setFormat($this->headers) === false )
					{
						$this->getResponseData('FORMAT_ERROR');
					}
				}
				
				// 得到http请求body信息
				if( strtolower($this->headers['format']) == 'form' )
				{
					$this->body = I('post.');
				}
				else
				{
					$this->body = urldecode(file_get_contents('php://input'));
					// php7不支持用法
// 					$this->body = $GLOBALS['HTTP_RAW_POST_DATA'];
					
				}
			
			break;
			
			default:
				$this->body = urldecode(file_get_contents('php://input'));
				// php7不支持用法
// 				$this->body = $GLOBALS['HTTP_RAW_POST_DATA'];
			break;
		}
		// 转化数据格式为数组
		$method = sprintf('%s%s', $this->headers['format'], 'ToArray');
		
		if( method_exists($this, $method) )
		{
			$data = $this->$method($this->body);
		}
		
		if( empty($data) )
		{
			$this->getResponseData('PARAMETER_ERROR');
		}
		
		//TODO 检查必填参数
		if( $this->checkRequire($this->required,$data) === false )
		{
			$this->getResponseData('PARAMETER_INCOMPLETE');
		}
		
		//TODO app_id判断接口调用权限
		
		// 分配调用接口版本,处理业务逻辑
		$service = explode(".", ucwords($data['service'], "."));
		
		$objstr = sprintf('\\%s\\Controller\\%sController', $service[0], $service[1]);
		
		$hanlder = new $objstr();
		
		// 接口方法名组装
		$this->version = str_replace('.', '', $data['version']);
		
		$testMethod = sprintf('%s%s', strtolower($service[2]), $this->version);
		
		if( method_exists($hanlder, $testMethod) )
		{
			//TODO 把所有body参数进行签名
			if( $this->checkSign($data) === false )
			{
				$this->getResponseData('SIGN_UNAUTHENTICATED');
			}
		}
		else
		{
			$this->getResponseData('METHOD_NOT_EXSIT');
		}
		
		$result = $hanlder->$testMethod($data);
		
		$this->getResponseData('EXECUTE_SUCCESS', $result);
	}
	
	public function setFormat($headers = array())
	{
		if( stripos($headers['CONTENT_TYPE'], 'form') )
		{
			$this->headers['format'] = 'form';
		}
		
		if( stripos($headers['CONTENT_TYPE'], 'json') )
		{
			$this->headers['format'] = 'json';
		}
		
		if( stripos($headers['CONTENT_TYPE'], 'xml') )
		{
			$this->headers['format'] = 'xml';
		}
		
		if( empty($this->headers['format']) )
		{
			return false;
		}
		
		return true;
	}
	
	
	
}