<?php
namespace Api\Controller;

use Think\Controller;

/**
 * 接口通用数据处理demo
 * @author hyz
 *
 */
class CommonController extends Controller
{
	protected $version = '1.0';
	
	protected $msg = '';
	
	protected $type = array('json','xml');
	
	protected $statusCode = array(
			'EXECUTE_SUCCESS' => '处理成功',
			'EXECUTE_PROCESSING' => '业务处理中',
			'CHARSET_ERROR' => '字符集转换异常',
			'FORMAT_ERROR' => '不兼容的参数格式',
			'PARAMETER_ERROR' => '参数错误',
			'SIGN_UNAUTHENTICATED' => '签名未通过',
			'METHOD_NOT_EXSIT' => '接口不存在或版本号错误',
			'NOTIFY_URL_NOT_SET' => 'notify_url未设置',
			'PARAMETER_INCOMPLETE' => '参数不全',
	);
	
	public function getHeaders()
	{
		$headers = $_SERVER;
	
		// 区别接口自己设置的头部参数和http请求本身的头部参数
		foreach ($_SERVER as $name => $value)
		{
			if (substr($name, 0, 5) == 'HTTP_')
			{
				$headers[str_replace(' ', '-', strtolower(str_replace('_', ' ', substr($name, 5))))] = $value;
			}
		}
	
		return $headers;
	}
	
	public function getResponseData($code="",$data = array(), $type = 'json')
	{
		$message = empty($this->msg) === false ? $this->msg : $this->statusCode[$code];
		
		if($code == 'EXECUTE_SUCCESS')
		{
			//TODO 获取响应参数签名
			$sign = $this->getResponseSign($data);
			$a = array('a'=>'qwer');
			$data = array_merge($a,$data);
			$this->ajaxReturn(array('code'=>$code,'msg'=>$message,'state'=>true,'data'=>$data,'sign'=>$sign));
		}
		else
		{
			$this->ajaxReturn(array('code'=>$code,'msg'=>$message,'state'=>false));
		}
	}
	
	//TODO 获取响应参数签名
	public function getResponseSign($data = array())
	{
		unset($data['sign']);
		
		$pkey = $this->getSecretKey('publicKey');
		
		ksort($data);
		
		$signStr = '';
		
		foreach ($data as $key => $item)
		{
			$signStr .= "&{$key}={$item}";
		}
		
		$signStr = trim($signStr,'&') . $pkey;
		
		return md5($signStr);
	}
	
	public function getRequstSign($data = array())
	{
		unset($data['sign']);
		
		$skey = $this->getSecretKey('secretKey');
		
		ksort($data);
		
		$signStr = '';
		
		foreach ($data as $key => $item)
		{
			$signStr .= "&{$key}={$item}";
		}
		
		$signStr = trim($signStr,'&') . $skey;
		
		if( strtolower($data['sign_type']) == 'md5' )
		{
			return md5($signStr);
		}
		else
		{
			return false;
		}
	}
	
	// 检查请求参数签名
	public function checkSign($data = array())
	{
		$clientSign = $data['sign'];
		
		$sign = $this->getRequstSign($data);
		
		return $clientSign === $sign;
	}
	
	//TODO 根据app_id取得账户私钥
	public function getSecretKey()
	{
		return '72969e04baa8b6e43fae34e1969d56a6';
	}
	
	//TODO 获取公钥
	public function getPublicKey()
	{
		return '11245e04baa8b6e43fae34e1969d5qw1';
	}
	
	public function checkRequire($required = array(),$query = array())
	{
		$diff = array_diff($required,array_keys($query));
		
		if( empty($diff) === false )
		{
			foreach ($diff as $row)
			{
				$this->msg .= $row . '不能为空,';
			}
	
			$this->msg = trim($this->msg,',');
	
			return false;
		}
		return true;
	}
	
	public function jsonToArray($raws = array())
	{
		return json_decode($raws,true);
	}
	
	public function xmlToArray($raws = array())
	{
		//将XML转为array
		libxml_disable_entity_loader(true);
	
		$data = json_decode(json_encode(simplexml_load_string($raws, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
	
		return $data;
	}
	
	public function formToArray($raws = array())
	{
		return $raws;
	}
	
	//转换xml函数
	public function arrayToXml($data)
	{
		$xml = "<xml>";
		foreach ($data as $key=>$val)
		{
			if (is_numeric($val))
			{
				$xml.="<".$key.">".$val."</".$key.">";
			}
			else
			{
				$xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
			}
		}
		$xml.="</xml>";
		return $xml;
	}
	
}