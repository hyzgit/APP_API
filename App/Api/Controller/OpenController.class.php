<?php
namespace Api\Controller;

/**
 * 接口业务处理版本迭代demo
 * @author hyz
 *
 */
class OpenController extends GatewayController
{
	public function notifyAction()
	{
		file_put_contents("/test.log", "test123");
	}
	
	public function test11($data)
	{
		//TODO 检查data内必填参数
		
		return array(
				'version'=>'v1.1',
				'app_id' => $data['app_id'],
		);
	}
	
	public function test10($data)
	{
		//TODO 检查data内必填参数
		
		return array(
				'version'=>'v1.0',
				'app_id' => $data['app_id'],
		);
	}
	
	public function test12($data)
	{
		//TODO 检查data内必填参数
		
		//TODO 处理异步通知
		
		return array(
				'version'=>'v1.2',
				'app_id' => $data['app_id'],
		);
	}
	
}