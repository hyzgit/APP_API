<?php
namespace App\Logic;

use Common\Model\BankModel;

class BankLogic extends BankModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}

	public function pullBank($query = array())
	{
		$data = array();
		
		$search['bank_channel'] = empty($query['channel']) === false ? strtoupper($query['channel']) : 'DEFAULT';
		
		$search['bank_state'] = 'NORMAL';
		
		$data = $this->where($search)->order('bank_order ASC,bank_id DESC')->getItems();
		
		if( empty($data) === false )
		{
			return $data;
		}
		else
		{
			$this->error = '未查找到数据';
			
			return false;
		}
	}
	
	public function checkRequire($required = array(),$query = array())
	{
		$diff = array_diff($required,array_keys($query));
		
		if( empty($diff) === false )
		{
			foreach ($diff as $row)
			{
				$this->error .= $row . '不能为空,';
			}
			
			$this->error = trim($this->error,',');
			
			return false;
		}
		return true;
	}
	
	public function getSecretKey()
	{
		return '72969e04baa8b6e43fae34e1969d56a6';
	}
	
	public function checkSign($data = array()) 
	{
		$clientSign = $data['sign'];
		
		$sign = $this->getSign($data);
		
		return $clientSign === $sign;
	}
	
	public function getSign($data = array())
	{
		unset($data['sign']);
		
		$string = '';
		
		$skey = $this->getSecretKey();
		
		ksort($data);
		
		foreach ($data as $key => $item) 
		{
			$string .= "&{$key}={$item}";
		}
	
		$string = trim($string,'&');
		
		$string .= $skey;
		
		return md5($string);
	}
}