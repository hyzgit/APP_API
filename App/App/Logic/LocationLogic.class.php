<?php
namespace App\Logic;

use Common\Model\LocationModel;

class LocationLogic extends LocationModel 
{
	protected function _initialize()
	{
		parent::_initialize();
	}

	public function checkLocation( $data = array() )
	{
		$raw = array();

		$where = array(
				'location_store_code'=>$data['storeCode'],
				'location_state'=>'NORMAL',
		);
		
		$raw = $this->where($where)->getItem();
		
		if( empty($raw) === false )
		{
			$distance = $this->getDistance($data['longitude'], $data['latitude'], $raw['location_longitude'], $raw['location_latitude']);

			if( $distance <= $raw['location_definition'] )
			{
				$this->error = "请求成功";
				
				return $raw;
			}
			else
			{
				$this->error = '与商户距离' . intval($distance) . '(米),超出规定范围' . $raw['location_definition'] . "(米)";
				
				//$this->error = '与商户距离超出规定范围' . $raw['location_definition'] . "(米)";
				
				return false;
			}
		}
		else
		{
			$this->error = '未查询到商户记录数据';
				
			return $data;
		}
	}
	
	public function addLocation( $data = array() )
	{
		$data = $this->mapData($data);
		
		$data['state'] = "NORMAL";
		
		$data['definition'] = '2000';
		
		$data['lock'] = 'YES';
		
		$data = $this->create($data,'both');
		
		if( $data === false )
		{
			return false;
		}
		
		$where = array(
				'location_store_code'=>$data['location_store_code'],
				'location_state'=>'NORMAL',
		);
	
		$store = $this->where($where)->getItem();
		
		$this->error = '请求成功';
		
		if( empty($store) === false )
		{
			switch ($store['location_lock'])
			{
				case 'YES':
					$this->error = '商家位置已绑定';
					
					return $store;
				break;
				
				case 'NO':
					$data['location_id'] = $store['location_id'];

					if ( $this->save($data) === false )
					{
						return false;
					}
					
					return $this->where($where)->getItem();
				break;
				
				default:
					$this->error = '商家位置信息有误';
						
					return false;
				break;
			}
		}
		else
		{
			if ( $this->add($data) === false )
			{
				return false;
			}
			
			return $this->where($where)->getItem();
		}
	}
	
	public function mapData( $data = array() )
	{
		$ret = array();
		
		$map = array(
				'storeCode' => 'store_code',
				'storeName' => 'store_name',
				'longitude' => 'longitude',
				'latitude' => 'latitude',
		);
		
		foreach ( $data as $key=>$value )
		{
			$ret[$map[$key]] = $value;
		}
		
		return $ret;
	}
	
	private function getDistance($lng1, $lat1, $lng2, $lat2)
	{
		$radLat1 = deg2rad($lat1);
		$radLat2 = deg2rad($lat2);
		$radLng1 = deg2rad($lng1);
		$radLng2 = deg2rad($lng2);
		
		$a = $radLat1 - $radLat2;
		$b = $radLng1 - $radLng2;
		
		$distance = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
		
		return $distance;
	}
	
	public function checkRequire($required = array(),$query = array())
	{
		$diff = array_diff($required,array_keys($query));
		
		if( empty($diff) === false )
		{
			foreach ($diff as $row)
			{
				$this->error .= $row . '不能为空,';
			}
				
			$this->error = trim($this->error,',');
				
			return false;
		}
		return true;
	}
	
	public function getSecretKey() 
	{
		return '72969e04baa8b6e43fae34e1969d56a6';
	}
	
	public function checkSign($data) 
	{
		$clientSign = $data['sign'];
		
		$sign = $this->getSign($data);

		return $clientSign === $sign;
	}
	
	public function getSign($data)
	{
		unset($data['sign']);
		
		$sk = $this->getSecretKey();
		
		ksort($data);
		$query = '';
		
		foreach ($data as $key => $item)
		{
			$query .= "&{$key}={$item}";
		}
	
		$query = trim($query,'&');
		$query .= $sk;
		
		return md5($query);
	}
}
