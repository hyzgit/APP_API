<?php
namespace App\Logic;

use Common\Model\CommodityModel;

class CommodityLogic extends CommodityModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}

	public function getCommodity($query = array())
	{
		$data = array();
			
		$where = array(
				'commodity_commodity_type' => $query['commodity_type'],
				'commodity_status' => 'NORMAL',
		);
		
		$data = $this->where($where)->getItem();
		
		if( empty($data) === false )
		{
			$ret=array(
					'commodity_commodity_type'=>$data['commodity_commodity_type'],
					'commodity_total_money'=>$data['commodity_total_money'],
					'commodity_stages_num'=>$data['commodity_stages_num'],
					'commodity_down_payment'=>$data['commodity_down_payment'],
					'commodity_reduce_price'=>$data['commodity_reduce_price'],
			);
			
			return $ret;
		}
		
		return false;
	}
	
	public function checkRequire($required = array(),$query = array())
	{
		$diff = array_diff($required,array_keys($query));
		
		if( empty($diff) === false )
		{
			foreach ($diff as $row)
			{
				$this->error .= $row . '不能为空,';
			}
			
			$this->error = trim($this->error,',');
			
			return false;
		}
		return true;
	}
	
	public function getSecretKey()
	{
		return '72969e04baa8b6e43fae34e1969d56a6';
	}
	
	public function checkSign($data = array()) 
	{
		$clientSign = $data['sign'];
		
		$sign = $this->getSign($data);
		
		return $clientSign === $sign;
	}
	
	public function getSign($data = array())
	{
		unset($data['sign']);
		
		$string = '';
		
		$skey = $this->getSecretKey();
		
		ksort($data);
		
		foreach ($data as $key => $item) 
		{
			$string .= "&{$key}={$item}";
		}
	
		$string = trim($string,'&');
		$string .= $skey;
		
		return md5($string);
	}
	
	
}