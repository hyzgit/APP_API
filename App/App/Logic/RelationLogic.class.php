<?php
namespace App\Logic;

use Common\Model\RelationModel;

class RelationLogic extends RelationModel
{
	protected function _initialize()
	{
		parent::_initialize();
	}

	public function getRelation($query = array())
	{
		$data = array();
		
		$ret = array();
		
		$search = array(
				'relation_contract_id' => $query['contract_id'],
				'relation_status' => 'NORMAL',
		);
		
		$search['relation_scenes'] = isset($query['scenes']) ? strtoupper($query['scenes']) : 'FIRST';
		
		$ret['contract_id'] = $query['contract_id'];
		
		$data = $this->where($search)->getItem();
		
		if( empty($data) === false )
		{
			$ret['neturl'] = $data['relation_neturl'];
			
			$ret['scenes'] = $data['relation_scenes'];
			
			return $ret;
		}
		else
		{
			$search['relation_neturl'] = isset($query['neturl']) ? $query['neturl'] : null;
			 
			if( $this->updateRecord($search) !== false )
			{
				$ret['neturl'] = $search['relation_neturl'];

				$ret['scenes'] = $search['relation_scenes'];
				
				return $ret;
			}
			else
			{
				$this->error = '查询参数有误';
				
				return false;
			}
		}
	}
	
	public function pushRelation($query = array())
	{
		$data = array();
		
		$search = array(
				'relation_contract_id' => $query['contract_id'],
				'relation_status' => 'NORMAL',
		);
		
		$search['relation_scenes'] = isset($query['scenes']) ? strtoupper($query['scenes']) : 'SECOND';
		
		$ret = array();
		
		$ret['contract_id'] = $query['contract_id'];
		
		$data = $this->where($search)->getItem();
		
		if( empty($data) === false )
		{
			$ret['neturl'] = $data['relation_neturl'];
				
			$ret['scenes'] = $data['relation_scenes'];
				
			return $ret;
		}
		else
		{
			$search['relation_neturl'] = isset($query['neturl']) ? $query['neturl'] : null;
		
			if( $this->updateRecord($search) === false )
			{
				$this->error = '查询参数有误';
				
				return false;
			}
			else
			{
				$ret['neturl'] = $search['relation_neturl'];
				
				$ret['scenes'] = $search['relation_scenes'];
				
				return $ret;
			}
		}
	}
	
	public function pullRelation($query = array())
	{
		$data = array();
		
		$search = array(
				'relation_contract_id' => $query['contract_id'],
				'relation_status' => 'NORMAL',
		);
		
		$search['relation_scenes'] = isset($query['scenes']) ? strtoupper($query['scenes']) : 'SECOND';
		
		$ret = array();
		
		$ret['contract_id'] = $query['contract_id'];
		
		$data = $this->where($search)->getItem();
		
		if( empty($data) === false )
		{
			$ret['neturl'] = $data['relation_neturl'];
		
			$ret['scenes'] = $data['relation_scenes'];
		
			return $ret;
		}
		else
		{
			// 正式环境接口地址
			$postUri = 'https://api.sibang.org.cn/api/gateway.htm';
			
			$postData = $this->getPostData($query);
			
			// 通过接口 以合同号为依据，查找网签地址
			$result = $this->responseByHttp($postUri,$postData,'POST');
			
			if( $result === false )
			{
				return false;
			}
			
			$result = json_decode($result,true);

			if( $result['code'] == 'EXECUTE_SUCCESS' && $result['resultCode'] == 'EXECUTE_SUCCESS' )
			{
				$search['relation_neturl'] = $result['elecCertiUrl'];
			}
			else
			{
				$this->error = isset($result['resultMessage']) ? $result['resultMessage'] : '合同号不存在';
				
				return false;
			}
			// 保存接口返回数据
			if( $this->updateRecord($search) === false )
			{
				$this->error = '查询参数有误';
				
				return false;
			}
			else
			{
				$ret['neturl'] = $search['relation_neturl'];
				
				$ret['scenes'] = $search['relation_scenes'];
				
				return $ret;
			}
		}
	}
	
	protected function getPostData($query = array())
	{
		$postData = array(
				//  获取进入签约链接服务码
				'service' => 'GET_JZQ_CERTIFICATE_LINK',
				'sign' => '',
				'signType' => 'MD5',
				'source' => 'CONTRACT_CHECK_PASSED',
				'contractId' => $query['contract_id'],
		);
				
		$postData['sign'] = $this->getSign($postData);
		
		return $postData;
	}
	
	protected function responseByHttp($uri = '',$data = array(),$method = '')
	{
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $uri);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		
		curl_setopt($ch, CURLOPT_HEADER, 0);//不返回头部信息
		
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 0);
		
		if( strtoupper($method) == 'POST' )
		{
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POST,TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}
	
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		
		//暂不下载证书
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		
		//执行curl
		$output = curl_exec($ch);
		
		//错误提示
		if( curl_exec($ch) === false )
		{
			$this->error = curl_error($ch);
			
			return false;
		}
		// 检查是否有错误发生
		if( curl_errno($ch) )
		{
			$this->error = curl_error($ch);
			
			return false;
		}
	
		//释放curl句柄
		curl_close($ch);
	
		return $output;
	}
	
	public function checkRequire($required = array(),$query = array())
	{
		$diff = array_diff($required,array_keys($query));
		
		if( empty($diff) === false )
		{
			foreach ($diff as $row)
			{
				$this->error .= $row . '不能为空,';
			}
			
			$this->error = trim($this->error,',');
			
			return false;
		}
		return true;
	}
	
	public function getSecretKey()
	{
		return '72969e04baa8b6e43fae34e1969d56a6';
	}
	
	public function checkSign($data = array()) 
	{
		$clientSign = $data['sign'];
		
		$sign = $this->getSign($data);
		
		return $clientSign === $sign;
	}
	
	public function getSign($data = array())
	{
		unset($data['sign']);
		
		$string = '';
		
		$skey = $this->getSecretKey();
		
		ksort($data);
		
		foreach ($data as $key => $item) 
		{
			$string .= "&{$key}={$item}";
		}
	
		$string = trim($string,'&');
		
		$string .= $skey;
		
		return md5($string);
	}
}