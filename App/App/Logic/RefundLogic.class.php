<?php

namespace App\Logic;

use Common\Model\RefundModel;

class RefundLogic extends RefundModel 
{
	protected function _initialize() 
	{
		parent::_initialize();
	}

	public function getRefundData( $data = array() )
	{   
		$raw = array();
		
		$raw = $this->where(array('refund_contract_id' => $data['contractId'],'refund_state' => 'NORMAL'))->getItem();
		
		if( empty($raw) === false )
		{
			$result = array(
				'refund_contract_id' => $raw['refund_contract_id'],
				'refund_status' => $raw['refund_status'],
				'refund_audit_time'=>$raw['refund_audit_time'],
				'refund_remark'=>$raw['refund_remark'],
				'refund_customer_remark'=>$raw['refund_customer_remark'],
				'refund_complete_time'=>$raw['refund_complete_time'],
				'refund_amount'=>$raw['refund_amount'],
			);
			
			$this->error = '请求成功';
		
			return $result;
		}
		else
		{
			$this->error = '未查询到记录数据';
				
			return null;
		}
		
	}
	
	public function updateRefund( $data = array() )
	{	
		$data = $this->mapData($data);
	
		$data['refund_time'] = time();
		$data['refund_status'] = 'WAIT';
	
		$where = array('refund_contract_id'=>$data['contract_id']);
		
		$item = $this->where($where)->getItem();
	
		if( empty($item) === false )
		{
			$this->error = '退款申请已提交';
				
			return false;
		}
		
		$data = $this->create($data);
		
		if( $this->updateRecord($data) === false )
		{
			return false;
		}
		
		$raw = $this->where($where)->getItem();
		
		$result = array(
				"refund_contract_id" => $raw['refund_contract_id'],
				"refund_status" => $raw['refund_status'],
				"refund_audit_time" => $raw['refund_audit_time'],
				"refund_remark" => $raw['refund_remark'],
				"refund_customer_remark" => $raw['refund_customer_remark'],
				"refund_complete_time" => $raw['refund_complete_time'],
				"refund_amount" => $raw['refund_amount']
		);
		
		return $result;
	}
	
	public function mapData( $data = array() )
	{
		$ret = array();
		
		$map = array(
				'contractId' => 'contract_id',
				'name' => 'name',
				'certificateCard' => 'certificate_card',
				'loanBankNumber' => 'loan_bank_number',
				'bankName' => 'bank_name',
				'mobileNumber' => 'mobile_number',
				'status' => 'contract_status',
				'matchStatus' => 'match_status',
				'customerRemark' => 'customer_remark',
		);
		
		foreach ( $data as $key=>$value )
		{			
			$ret[$map[$key]] = $value;
		}
		
		return $ret;
	}
	
	public function checkRequire($required = array(),$query = array())
	{
		$diff = array_diff($required,array_keys($query));
	
		if( empty($diff) === false )
		{
			foreach ($diff as $row)
			{
				$this->error .= $row . '不能为空,';
			}
				
			$this->error = trim($this->error,',');
				
			return false;
		}
		return true;
	}
	
	public function getSecretKey() 
	{
		return '72969e04baa8b6e43fae34e1969d56a6';
	}
	
	public function checkSign($data) 
	{
		$clientSign = $data['sign'];
		
		$sign = $this->getSign($data);
		
		return $clientSign === $sign;
	}
	
	public function getSign($data)
	{
		unset($data['sign']);
		
		$sk = $this->getSecretKey();
		
		ksort($data);
		$query = '';
		
		foreach ($data as $key => $item)
		{
			$query .= "&{$key}={$item}";
		}
	
		$query = trim($query,'&');
		$query .= $sk;
		
		return md5($query);
	}
}
