<?php
namespace App\Controller;

use App\Logic\SmsLogic;

class SmsController extends ApiController
{
	private $sms = null;
	
	protected function _initialize()
	{
		$this->sms= new SmsLogic();
		
		parent::_initialize();
	}
	
	public function sendSmsAction()
	{
		$raw = I('post.');
		
		$content = "您的验证码是：{$raw['captcha']}。请不要把验证码泄露给其他人。";
		
		$gatewayUrl = 'http://106.ihuyi.cn/webservice/sms.php?method=Submit';
		$postData = array(
				'account'=>"cf_xfq",
				'password'=>"316046d5cbd1d8f48d9c7c7a1a588707",
				'mobile'=>$raw['mobile'],
				'content'=>$content
		);
		
		$curl = new \Org\Net\Http\Curl();
		
		$curl->post($gatewayUrl,$postData);
		
		if ($curl->error)
		{
			$this->response(array('state'=>0,'msg'=>$curl->errorMessage),'json');
		}
		
		libxml_disable_entity_loader(true);
		$respon = json_decode(json_encode(simplexml_load_string($curl->response, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
		
		if($respon['code'] != 2){
			$this->response(array('state'=>0,'msg'=>$respon['msg']),'json');
		}
		
		$this->response(array('state'=>1,'msg'=>'发送成功'),'json');
		
	}
	
	public function reportAction()
	{
		
		$data = I('post.');
		
		$data = $this->sms->getupdataData($data['args']);
		
		if($data == false )
		{
			echo ($this->sms->getError());exit();
		}
		
		$data = $this->sms->addAlldata($data);
		
		$retdata = $data >0 ? 0:'未知错误';
				
		echo($retdata);exit();
		
	}
	
}