<?php

namespace App\Controller;

use App\Logic\PaymentLogic;
use App\Logic\ContractLogic;
use App\Logic\WxPayDataLogic;

/** 	
 * app支付状态检查
 */
class PayController extends ApiController {

    private $payment = null;
    private $contract = null;
    private $wxpay = null;
    private $error = null;
    
    protected function _initialize() {
        parent::_initialize();

        $this->payment = new PaymentLogic();

        $this->contract = new ContractLogic();

        $this->wxpay = new WxPayDataLogic();
    }
    
    // APP订单支付检查
    public function checkAction() {
        $query = array_merge(I('get.'), I('post.'));

        foreach ($query as $key => $value) {
            if($value == '')
            {
                unset($query[$key]);
            }
        }

        $raw = array(
            'data' => array(),
            'msg' => '请求失败',
            'state' => 0
        );

        $required = array('loan_type','commodity_type','stages','contract_id','sign');
        
        $diff = array_diff($required, array_keys($query));
        if (empty($diff) === false) {
            $raw['msg'] = 'ERROR_PARAM';
            return $this->ajaxReturn($raw);
        }

        if($this->payment->checkSign($query)===false) {
            $raw['msg'] = 'ERROR_SIGN';
            return $this->ajaxReturn($raw);
        }

        //获取数据
        $loan_data = $this->payment->getPayData($query);

        $contract_data = $this->contract->getContractData($query['contract_id'],$loan_data);

        if ($contract_data == false || $loan_data == false) {
            $raw['msg'] = 'empty return data';
            return $this->ajaxReturn($raw);
        }

        $raw['msg'] = '请求成功';
        $raw['state'] = 1;
        $raw['data'] = array_merge($loan_data,$contract_data);
        return $this->ajaxReturn($raw);
    }

    //支付结果回调
    public function notifyAction(){
        //app 支付回调
        $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
        // $xml = file_get_contents(dirname(__DIR__) . '/newfile.txt');

        //判断异步接收的xml是否正确
        $data = $this->wxpay->FromXml($xml);

        header("Content-type: text/html; charset=utf-8");

        $xmldata = array('return_code' => 'SUCCESS','return_msg'=>'OK');

        if($data['return_code'] != 'SUCCESS')
        {   
            //xml格式错误
            $xmldata = $data;
        }
        else
        {
            //判断签名是否正确
            $result_data = $data['data'];

            if($this->notifydo($result_data)===false)
            {
                $xmldata = array('return_code' => 'FAIL','return_msg'=>$this->error);
            }

            if($result_data['result_code'] != 'SUCCESS')
            {
                // 失败订单记录日志
                file_put_contents(dirname(__DIR__) . '/wxpay_notify.log','NOTIFY DATA::' . json_encode($result_data) . PHP_EOL . 'RETURN DATA::' . json_encode($xmldata). PHP_EOL,FILE_APPEND);
            }
           
        }
        echo $this->wxpay->ReplyNotify($xmldata);exit();
    }

    protected function notifydo($result_data)
    {
        $sign = $result_data['sign'];
            
        if( $this->wxpay->CheckSign($result_data,$sign) == false )
        {
           $this->error ='签名错误';
            return false;
        }
        //   取得合同编号
        $number = $result_data['attach'];

        $row = $this->contract->getDataByPar($number);

        if(!$row) 
        {
            $this->error ='合同号错误';
            return false;
        } 

        if($row['contract_status'] !== 1) 
        {  
            //支付金额 时间 支付状态
            $dataArr['amount'] = $result_data['total_fee'];
            $dataArr['time']   = strtotime($result_data['time_end']); 
            $dataArr['status'] = $result_data['result_code'] == 'SUCCESS' ? 1 : 2;

            if($this->contract->updateByParm($where = array('contract_number'=>$number),$dataArr) === false)
            {
                $this->error ='未知错误';
                return false;
               
            }                  

        }

        return true;
    }

}
