<?php
namespace App\Controller;

use App\Logic\LocationLogic;

class LocationController extends ApiController
{
	private $location = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->location = new LocationLogic();
	}
	
	public function checkAction()
	{		
		$query = I('post.');
		
		$data = array();
		
		// 验签
		if( $this->location->checkSign($query) === false )
		{
			$this->ajaxReturn(array('msg'=>'ERROR_SIGN','state'=>0));
		}
		
		// 检查必填参数
		$required = array('package','code','storeCode','longitude','latitude');
		
		if( $this->location->checkRequire($required,$query) === false )
		{
			$this->ajaxReturn(array('msg'=>$this->location->getError(),'state'=>0));
		}
		
		$data = $this->location->checkLocation($query);
		
		if( $data === false )
		{
			$raw = array('msg'=>$this->location->getError(),'state'=>0,'data'=>NULL);
		}
		else
		{
			$raw = array('msg'=>$this->location->getError(),'state'=>1,'data'=>$data);
		}
		
		$this->ajaxReturn($raw);	
	}
	
	public function addAction()
	{
		$query = I('post.');
		
		$ret = array();
		
		// 验签
		if( $this->location->checkSign($query) === false )
		{
			$this->ajaxReturn(array('msg'=>'ERROR_SIGN','state'=>0));
		}
		
		// 检查必填参数
		$required = array('package','code');
		
		if( $this->location->checkRequire($required,$query) === false )
		{
			$this->ajaxReturn(array('msg'=>$this->location->getError(),'state'=>0));
		}
		
		$ret = $this->location->addLocation($query);
		
		if( $ret === false )
		{
			$raw = array('msg'=>$this->location->getError(),'state'=>0,'data'=>NULL);
		}
		else
		{
			$raw = array('msg'=>$this->location->getError(),'state'=>1,'data'=>$ret);
		}
		
		$this->ajaxReturn($raw);
	}

}