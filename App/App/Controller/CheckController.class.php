<?php
namespace App\Controller;

use App\Logic\VersionLogic;

/**	
 * APP版本信息
 */

class CheckController extends ApiController
{
	private $version = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->version = new VersionLogic();
	}
	
	// APP版本检测统一接口
	public function versionAction()
	{
		$query 		= array_merge(I('get.'),I('post.'));
		
		$raw = array(
				'data' => array(),
				'msg' => '请求失败',
				'state' => 0,
				'sign' => $this->version->getSigntrue(),
		);
		
		// 检查必填参数
		$required = array('type','code','platform');
		
		if( $this->version->checkRequire($required,$query) === false )
		{
			$raw['msg'] = $this->version->getError();
			
			$this->ajaxReturn($raw);
		}
		
		$data = $this->version->checkVersion($query);
		
		if( $data === false )
		{
			$raw['msg'] = $this->version->getError();
			
			return $this->ajaxReturn($raw);
		}
		
		$raw['msg'] = '请求成功';
		$raw['state'] = 1;
		$raw['sign'] = $this->version->getSigntrue($data);
		$raw['data'] = $data;
		
		return $this->ajaxReturn($raw);		
	}

}