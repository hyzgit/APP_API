<?php
namespace App\Controller;

use App\Logic\CommodityLogic;

class CommodityController extends ApiController
{
	private $commodity = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->commodity = new CommodityLogic();
	}
	
	public function checkAction()
	{
		$query = array_merge(I('get.'),I('post.'));
		
		$raw = array();
		
		// 验签
		if( $this->commodity->checkSign($query) === false )
		{
			$this->ajaxReturn(array('msg'=>'ERROR_SIGN','state'=>0));
		}
		
		// 检查必填参数
		$required = array('package','code','commodity_type');
		if( $this->commodity->checkRequire($required,$query) === false )
		{
			$this->ajaxReturn(array('msg'=>$this->commodity->getError(),'state'=>0));
		}
		
		$data = $this->commodity->getCommodity($query);
		if( $data === false )
		{
			$raw = array('msg'=>'查询参数有误','state'=>1,'data'=>NULL);
		}
		else
		{
			$raw = array('msg'=>'请求成功','state'=>1,'data'=>$data);
		}
		
		$this->ajaxReturn($raw);		
	}

}