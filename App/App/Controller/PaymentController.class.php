<?php

namespace App\Controller;

use App\Logic\PaymentLogic;
use App\Logic\SecurityLogic;

/** 	
 * app付款设置
 */
class PaymentController extends ApiController {

    private $payment = null;
    private $security = null;

    protected function _initialize() {
        parent::_initialize();

        $this->payment = new PaymentLogic();
        $this->security = new SecurityLogic();
    }
    
    // APP首付设置接口
    public function downPaymentAction() {
        $query = array_merge(I('get.'), I('post.'));
        $raw = array(
            'data' => array(),
            'msg' => '请求失败',
            'state' => 0
        );
        $required = array(
            'loan_type',
            'commodity_type',
            'stages',
            'sign',
        );
        $diff = array_diff($required, array_keys($query));
        if (empty($diff) === false) {
            $raw['msg'] = 'ERROR_PARAM';
            return $this->ajaxReturn($raw);
        }
        //验证
        if($this->security->checkSign($query)===false){
            $raw['msg'] = 'ERROR_SIGN';
            return $this->ajaxReturn($raw);
        }
        //获取数据
        $data = $this->payment->getDownPayment($query);
        if ($data == false) {
            $raw['msg'] = 'empty!';
            return $this->ajaxReturn($raw);
        }
        $raw['msg'] = '请求成功';
        $raw['state'] = 1;
        $raw['data'] = $data;
        return $this->ajaxReturn($raw);
    }

}
