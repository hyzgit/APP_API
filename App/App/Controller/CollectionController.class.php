<?php
namespace App\Controller;

use App\Logic\CollectionLogic;

class CollectionController extends ApiController
{
	private $collection = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->collection = new CollectionLogic();
	}
	
	public function collectionAction()
	{
		$query = I('post.');
		
		$ret = array();
		
		// 验签
		if( $this->collection->checkSign($query) === false )
		{
			$this->ajaxReturn(array('msg'=>'ERROR_SIGN','state'=>0));
		}
		
		// 检查必填参数
		$required = array('package','code','mode','key');
		
		if( $this->collection->checkRequire($required,$query) === false )
		{
			$this->ajaxReturn(array('msg'=>$this->collection->getError(),'state'=>0));
		}
		
		$ret = $this->collection->updateCollection($query);
		
		if( $ret === false )
		{
			$raw = array('msg'=>$this->collection->getError(),'state'=>0);
		}
		else
		{
			$raw = array('msg'=>'请求成功','state'=>1);
		}
		
		$this->ajaxReturn($raw);
	}

}