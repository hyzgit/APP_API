<?php
namespace App\Controller;

use App\Logic\BankLogic;

class BankController extends ApiController
{
	private $bank = null;
	
	protected function _initialize()
	{
		parent::_initialize();
		
		$this->bank = new BankLogic();
	}
		
	public function pullAction()
	{
		$query = array_merge(I('get.'),I('post.'));
		
		$raw = array();
		
		// 验签
		if( $this->bank->checkSign($query) === false )
		{
			$this->ajaxReturn(array('msg'=>'ERROR_SIGN','state'=>0));
		}
		
		// 检查必填参数
		$required = array('package','code');
		
		if( $this->bank->checkRequire($required,$query) === false )
		{
			$this->ajaxReturn(array('msg'=>$this->bank->getError(),'state'=>0));
		}
		
		$data = $this->bank->pullBank($query);
		
		if( $data === false )
		{
			$raw = array('msg'=>$this->bank->getError(),'state'=>1,'data'=>NULL);
		}
		else
		{
			$raw = array('msg'=>'请求成功','state'=>1,'data'=>$data);
		}
		
		$this->ajaxReturn($raw);
	}

}