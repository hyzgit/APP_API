<?php
namespace App\Controller;

use Common\Controller\ApiController;

abstract class BaseController extends ApiController
{
	protected function _initialize()
	{
		parent::_initialize();
	}
}