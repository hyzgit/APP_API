<?php

namespace App\Controller;

use App\Logic\CancelLogic;

class CancelController extends ApiController 
{
	private $cancel = null;
	
	protected function _initialize() 
	{
		parent::_initialize();

		$this->cancel = new CancelLogic();
	}
	
	public function checkAction()
	{
		$query = I('post.');

		$raw = array();
		// 验签
		if( $this->cancel->checkSign($query) === false )
		{				
			$this->ajaxReturn(array('msg'=>'ERROR_SIGN','state'=>0));
		}
		
		// 检查必填参数
		$required = array('package','code','contractId');
		
		if( $this->cancel->checkRequire($required,$query) === false )
		{
			$this->ajaxReturn(array('msg'=>$this->cancel->getError(),'state'=>0));
		}
		
		$data = $this->cancel->getCancelData($query);
		
		if( $data === false )
		{
			$raw = array('msg'=>$this->cancel->getError(),'state'=>0,'data'=>$data);
		}
		else
		{
			$raw = array('msg'=>$this->cancel->getError(),'state'=>1,'data'=>$data);
		}
		
		$this->ajaxReturn($raw);	
	}
	
	public function cancelAction()
	{
		$query = I('post.');
		
		$raw = array();
		// 验签
		if( $this->cancel->checkSign($query) === false )
		{
			$this->ajaxReturn(array('msg'=>'ERROR_SIGN','state'=>0));
		}
		
		// 检查必填参数
		$required = array('package','code');

		if( $this->cancel->checkRequire($required,$query) === false )
		{
			$this->ajaxReturn(array('msg'=>$this->cancel->getError(),'state'=>0));
		}
		
		$ret = $this->cancel->updateCancel($query);
		
		if( $ret === false )
		{
			$raw = array('msg'=>$this->cancel->getError(),'state'=>0,'data'=>NULL);
		}
		else
		{
			$raw = array('msg'=>'请求成功','state'=>1,'data'=>$ret);
		}
		
		$this->ajaxReturn($raw);
	}
	
}
