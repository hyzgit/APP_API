<?php
namespace App\Controller;

use App\Logic\MessageLogic;

class MessageController extends ApiController
{
	private $message = null;
	
	protected function _initialize()
	{
		$this->message= new MessageLogic();
		
		parent::_initialize();
	}
	
	public function sendAction()
	{
		$request = I('post.');
		
		$raw = array();
		
		// 验签
		if( $this->message->checkSign($request) === false )
		{
			$this->response(array('msg'=>'ERROR_SIGN','state'=>0),'json');
		}
		
		// 检查必填参数
		$required = array('mobile');
		
		if( $this->message->checkRequire($required,$request) === false )
		{
			$this->response(array('msg'=>$this->message->getError(),'state'=>0),'json');
		}
		
		// 设置请求参数
		$request = $this->message->setRequestData($request);
		
		if( $request === false )
		{
			$this->response(array('msg'=>$this->message->getError(),'state'=>0),'json');
		}
		
		// 保存调用接口前数据
		$ret = $this->message->saveRequestData($request);
		
		if( $request === false )
		{
			$this->response(array('msg'=>$this->message->getError(),'state'=>0),'json');
		}
		
		// 请求短信接口
		$response = $this->message->curlPost($request);
		
		if( $response === false )
		{
			$this->response(array('msg'=>$this->message->getError(),'state'=>0),'json');
		}
		
		// 接口请求成功,更新接口返回数据
		$data = $this->message->saveResponseData($response,$ret);
		
		if( $data === false )
		{
			$raw = array('msg'=>$this->message->getError(),'state'=>0);
		}
		else
		{
			$raw = array('msg'=>$this->message->msg,'state'=>1,'data'=>$data);
		}
		
		$this->response($raw,'json');
	}
	
}