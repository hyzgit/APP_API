<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 短信管理</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- previewThumb -->
		<script src="<?php echo H('assets/plugins/previewThumb/jQuery.previewThumb.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5;">
		<section class="content">
		
		
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">新建短信</h3>
	</div>
	<form class="form-horizontal" action="<?php echo U('send');?>" method="post" rel-action="form" rel-success="reload()">
		<div class="box-body">
			<div class="form-group">
				<label class="col-sm-2 control-label"><span class="text-red">*</span>手机号</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="mobile" placeholder="请输入手机号"  autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"><span class="text-red">*</span>模板</label>
				<div class="col-sm-10">
					<select class="form-control" rel-action="select" name="template">
						<option value="">请选择短信模板</option>
						<?php if(is_array($template)): $i = 0; $__LIST__ = $template;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i; if('code' == $key): ?><option value="<?php echo ($key); ?>" selected><?php echo ($item); ?></option>
						<?php else: ?>
							<option value="<?php echo ($key); ?>"><?php echo ($item); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">内容</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" name="content" placeholder="选择[验证码模板]，不用填写内容。"  autocomplete="off">
				</div>
			</div>
		</div>
		<div class="box-footer">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">发送</button>
				<button type="button" class="btn btn-default" rel-action="dialog-close">关闭</button>
			</div>
		</div>
	</form>
</div>

		
		</section>
	</body>
</html>