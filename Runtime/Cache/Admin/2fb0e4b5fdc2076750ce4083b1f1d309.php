<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 查看支付信息</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- previewThumb -->
		<script src="<?php echo H('assets/plugins/previewThumb/jQuery.previewThumb.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5;">
		<section class="content">
		
		
	<div class="box box-success">
		<div class="box-header with-border">
			<h3 class="box-title">查看支付信息</h3>
		</div>
		<div class="box-body table-responsive">
			<?php if(empty($data)): ?><div class="alert alert-warning alert-dismissible">
				<h4><i class="icon fa fa-warning"></i>暂无任何记录</h4>
				  未查到任何符合要求的记录！
			</div><?php endif; ?>
			<?php if(!empty($data)): ?><table class="table table-bordered table-striped">
				<tr>
					<td class="text-right col-sm-2">合同编号</td>
					<td><?php echo ($data['contract_number']); ?></td>
				</tr>
				<tr>
					<td class="text-right col-sm-2">微信支付单号</td>
					<td><?php echo ($data['contract_transaction_id']); ?></td>
				</tr>
				<tr>
					<td class="text-right">姓名</td>
					<td><?php echo ($data["contract_name"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">身份证号</td>
					<td><?php echo ($data["contract_cardid"]); ?></td>
				</tr>
					<td class="text-right">支付金额</td>
					<td><?php echo ($data["amount"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">支付时间</td>
					<td><?php echo ($data["time"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">贷款类型</td>
					<td><?php echo ($data["contract_loan_type"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">商品类型</td>
					<td><?php echo ($data["contract_commodity_type"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">品牌</td>
					<td><?php echo ($data["contract_brand"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">总金额</td>
					<td><?php echo ($data["contract_total_money"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">首付金额</td>
					<td><?php echo ($data["contract_down_pay"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">支付状态</td>
					<?php if($data["contract_status"] == 0): ?><td>未支付</td><?php endif; ?>
					<?php if($data["contract_status"] == 1): ?><td>支付成功</td><?php endif; ?>
					<?php if($data["contract_status"] == 2): ?><td>支付失败</td><?php endif; ?>
				</tr>
			</table><?php endif; ?>
		</div>
	</div>

		
		</section>
	</body>
</html>