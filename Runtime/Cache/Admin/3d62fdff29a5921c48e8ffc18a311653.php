<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 二维码生成</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- previewThumb -->
		<script src="<?php echo H('assets/plugins/previewThumb/jQuery.previewThumb.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5;">
		<section class="content">
		
		
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">业务信息修改</h3>
	</div>
	<form class="form-horizontal" action="<?php echo U('save');?>" method="post" rel-action="form" rel-success="reload()">
	<div class="box-body">
		<input type="hidden" class="form-control" name="id" value="<?php echo ($data["qrcode_id"]); ?>">
		<div class="form-group">
			<label class="col-sm-2 control-label">名称</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="name" placeholder="请输入名称" value="<?php echo ($data["qrcode_name"]); ?>" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red">*</span>店铺编号</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="storecode" placeholder="请输入店铺编号,例：346210001" value="<?php echo ($data["qrcode_storecode"]); ?>" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red">*</span>店铺名称</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="storename"  placeholder="请输入店铺名称" value="<?php echo ($data["qrcode_storename"]); ?>"  autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red">*</span>业务员姓名</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="username" placeholder="请输入业务员姓名,例：yewuyuan" value="<?php echo ($data["qrcode_username"]); ?>" autocomplete="off">
			</div>
		</div>	
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red"></span>贷款类型</label>
			<div class="col-sm-10">
				<select class="form-control" rel-action="select" name="loantype">
					<option value="">请选择贷款类型</option>
					<?php if(is_array($loan_type)): $i = 0; $__LIST__ = $loan_type;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i; if($key == $data['qrcode_loantype']): ?><option value="<?php echo ($key); ?>" selected><?php echo ($item); ?></option>
						<?php else: ?>
							<option value="<?php echo ($key); ?>"><?php echo ($item); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red"></span>商品类型</label>
			<div class="col-sm-10">
				<select class="form-control" rel-action="select" name="commoditytype">
					<option value>请选择商品类型</option>
					<?php if(is_array($commodity_type)): $i = 0; $__LIST__ = $commodity_type;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i; if($key == $data['qrcode_commoditytype']): ?><option value="<?php echo ($key); ?>" selected='selected'><?php echo ($item); ?></option>
						<?php else: ?>
							<option value="<?php echo ($key); ?>"><?php echo ($item); ?></option><?php endif; endforeach; endif; else: echo "" ;endif; ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red"></span>商品品牌</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="commoditybrand" placeholder="请输入商品品牌" value="<?php echo ($data["qrcode_commoditybrand"]); ?>"  autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red"></span>商品型号</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="commoditymodel" placeholder="请输入商品型号" value="<?php echo ($data["qrcode_commoditymodel"]); ?>"  autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red"></span>商品价格</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="commodityprice" placeholder="请输入商品价格" value="<?php echo ($data["qrcode_commodityprice"]); ?>"  autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red"></span>首付值</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="downpayment" placeholder="请输入首付值" value="<?php echo ($data["qrcode_downpayment"]); ?>"  autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">状态</label>
			<div class="col-sm-10 radio">
				<label>
					<?php if($data["qrcode_status"] == 1): ?><input type="radio" name="status" value="1" checked="checked">正常
					<?php else: ?>
						<input type="radio" name="status" value="1">正常<?php endif; ?>
				</label>
				<label>
					<?php if($data["qrcode_status"] == 0): ?><input type="radio" name="status" value="0" checked="checked">锁定
					<?php else: ?>
						<input type="radio" name="status" value="0">锁定<?php endif; ?>
				</label>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">保存</button>
			<button type="button" class="btn btn-default" rel-action="dialog-close">关闭</button>
		</div>
	</div>
	</form>
</div>

		
		</section>
	</body>
</html>