<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 查询支付信息</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- previewThumb -->
		<script src="<?php echo H('assets/plugins/previewThumb/jQuery.previewThumb.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5;">
		<section class="content">
		
		
<div class="box box-danger">
	<div class="box-header with-border">
		<h3 class="box-title">查询支付信息</h3>
	</div>
		<form class="form-horizontal" action="<?php echo U('notify');?>" method="post" rel-action="form" rel-success="reload()">
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-2 control-label">查询原始数据</label>
					<div class="col-sm-10">
						<textarea class="form-control" name='result' rows="5" readonly><?php echo ($result); ?></textarea>
					</div>
				</div>
				<!-- div class="form-group">
					<label class="col-sm-2 control-label">商户号</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control" name="mch_id" placeholder="请输入商户号" value="<?php echo ($data["mch_id"]); ?>" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">微信支付单号</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control" name="transaction_id" placeholder="请输入微信支付单号" value="<?php echo ($data["transaction_id"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">金额</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control" name="total_fee" placeholder="" value="<?php echo ($data["total_fee"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">支付时间</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control" name="time_end" placeholder="" value="<?php echo ($data["time_end"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">支付状态</label>
					<div class="col-sm-10">
						<input type="text" readonly class="form-control" name="trade_state" placeholder="" value="<?php echo ($data["trade_state"]); ?>"  autocomplete="off">
					</div>
				</div -->
			</div>
			<div class="box-footer">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">数据订正</button>
					<button type="button" class="btn btn-default" rel-action="dialog-close">关闭</button>
				</div>
			</div>
		</form>
</div>

		
		</section>
	</body>
</html>