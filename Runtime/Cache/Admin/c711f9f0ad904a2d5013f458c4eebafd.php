<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 新增权限</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
<script>
$(function(){
	$("#hide").hide();
	$("select").change(function(){
		$(this).val()=="0" ? $("#hide").hide() : $("#hide").show();
	});
});
</script>

		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- previewThumb -->
		<script src="<?php echo H('assets/plugins/previewThumb/jQuery.previewThumb.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5;">
		<section class="content">
		
		
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">新增权限</h3>
	</div>
	<form class="form-horizontal" action="<?php echo U('save');?>" method="post" rel-action="form" rel-success="reload()">
	<div class="box-body">
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red">*</span>所属分类</label>
			<div class="col-sm-10">
				<select class="form-control" rel-action="select" name="pid">
					<option value="0">作为顶级分类</option>
					<?php if(is_array($level)): $i = 0; $__LIST__ = $level;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><option value="<?php echo ($item["level_id"]); ?>"><?php echo ($item["level_name"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
				</select>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red">*</span>标题</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="name" placeholder="请输入标题"  autocomplete="off">
			</div>
		</div>
		<div id="hide">
		<div class="form-group">
			<label class="col-sm-2 control-label"><span class="text-red">*</span>标识</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="data" placeholder="请输入地址：如：/Admin/Acount/password"  autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">默认</label>
			<div class="col-sm-10 radio">
				<label><input type="radio" name="default" value="1">启用</label>
				<label><input type="radio" name="default" value="0" checked="checked">禁用</label>
			</div>
		</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">描述</label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="desc" placeholder="请输入描述"  autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">排序</label>
			<div class="col-sm-10">
				<input type="number" class="form-control" name="order" placeholder="请填写整数，数字越小越靠前" autocomplete="off">
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-2 control-label">状态</label>
			<div class="col-sm-10 radio">
				<label><input type="radio" name="state" value="1" checked="checked">正常</label>
				<label><input type="radio" name="state" value="0">锁定</label>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-default">保存</button>
			<button type="button" class="btn btn-default" rel-action="dialog-close">关闭</button>
		</div>
	</div>
	</form>
</div>

		
		</section>
	</body>
</html>