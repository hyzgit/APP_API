<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 编辑支付信息</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- previewThumb -->
		<script src="<?php echo H('assets/plugins/previewThumb/jQuery.previewThumb.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5;">
		<section class="content">
		
		
<div class="box box-danger">
	<div class="box-header with-border">
		<h3 class="box-title">编辑支付信息</h3>
	</div>
	<?php if(!empty($data)): ?><form class="form-horizontal" action="<?php echo U('save');?>" method="post" rel-action="form" rel-success="reload()">
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-2 control-label"><span class="text-red">*</span>合同编号</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="number" placeholder="请输入合同编号" value="<?php echo ($data["contract_number"]); ?>" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">微信支付单号</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="transaction_id" placeholder="请输入微信支付单号" value="<?php echo ($data["contract_transaction_id"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">姓名</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="name" placeholder="请输入姓名" value="<?php echo ($data["contract_name"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">身份证号</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="cardid" placeholder="请输入身份证号" value="<?php echo ($data["contract_cardid"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">支付金额</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="amount" placeholder="请输入支付金额" value="<?php echo ($data["amount"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">支付时间</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="time" placeholder="请输入支付时间" value="<?php echo ($data["time"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">贷款类型</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="loan_type" placeholder="请输入贷款类型" value="<?php echo ($data["contract_loan_type"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">商品类型</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="commodity_type" placeholder="请输入商品类型" value="<?php echo ($data["contract_commodity_type"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">品牌</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="brand" placeholder="请输入品牌" value="<?php echo ($data["contract_brand"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">总金额</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="total_money" placeholder="请输入总金额" value="<?php echo ($data["contract_total_money"]); ?>"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">首付金额</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="down_pay" placeholder="请输入首付金额" value="<?php echo ($data["contract_down_pay"]); ?>" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">支付状态</label>
					<div class="col-sm-10 radio">
						<label>
							<?php if($data["contract_status"] == '1'): ?><input type="radio" name="status" value="1" checked="checked">支付成功
							<?php else: ?>
								<input type="radio" name="status" value="1">支付成功<?php endif; ?>
						</label>
						<label>
							<?php if($data["contract_status"] == '2'): ?><input type="radio" name="status" value="2" checked="checked">支付失败
							<?php else: ?>
								<input type="radio" name="status" value="2">支付失败<?php endif; ?>
						</label>
						<label>
							<?php if($data["contract_status"] == '0'): ?><input type="radio" name="status" value="0" checked="checked">未支付
							<?php else: ?>
								<input type="radio" name="status" value="0">未支付<?php endif; ?>
						</label>
					</div>
				</div>
			</div>
			<input type="hidden" name="id" value="<?php echo ($data["contract_id"]); ?>">
			<div class="box-footer">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">保存</button>
					<button type="button" class="btn btn-default" rel-action="dialog-close">关闭</button>
				</div>
			</div>
		</form><?php endif; ?>
	<?php if(empty($data)): ?><div class="box-body">
		<div class="alert alert-error alert-dismissible">
			<h4><i class="icon fa fa-warning"></i>暂无记录</h4>
			参数有误
		</div>
	</div><?php endif; ?>
</div>

		
		</section>
	</body>
</html>