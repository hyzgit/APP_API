<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 编辑商户定位信息</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- previewThumb -->
		<script src="<?php echo H('assets/plugins/previewThumb/jQuery.previewThumb.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5;">
		<section class="content">
		
		
<div class="box box-danger">
	<div class="box-header with-border">
		<h3 class="box-title">编辑商户定位信息</h3>
	</div>
	<?php if(!empty($data)): ?><form class="form-horizontal" action="<?php echo U('save');?>" method="post" rel-action="form" rel-success="reload()">
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-2 control-label"><span class="text-red">*</span>商户编号</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="store_code" value="<?php echo ($data["location_store_code"]); ?>" placeholder="请输入商户编号"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><span class="text-red">*</span>商户名称</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="store_name" value="<?php echo ($data["location_store_name"]); ?>" placeholder="请输入商户名称"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><span class="text-red">*</span>经度</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="longitude" value="<?php echo ($data["location_longitude"]); ?>" placeholder="请输入商户定位经度"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label"><span class="text-red">*</span>纬度</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="latitude" value="<?php echo ($data["location_latitude"]); ?>" placeholder="请输入商户定位纬度"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">精确距离</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="definition" value="<?php echo ($data["location_definition"]); ?>" placeholder="请输入精确距离，单位（米）"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">锁定</label>
					<div class="col-sm-10 radio">
						<label>
							<?php if($data["location_lock"] == 'YES'): ?><input type="radio" name="lock" value="1" checked="checked">是
							<?php else: ?>
								<input type="radio" name="lock" value="1">是<?php endif; ?>
						</label>
						<label>
							<?php if($data["location_lock"] == 'NO'): ?><input type="radio" name="lock" value="0" checked="checked">否
							<?php else: ?>
								<input type="radio" name="lock" value="0">否<?php endif; ?>
						</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">状态</label>
					<div class="col-sm-10 radio">
						<label>
							<?php if($data["location_state"] == 'NORMAL'): ?><input type="radio" name="state" value="1" checked="checked">正常
							<?php else: ?>
								<input type="radio" name="state" value="1">正常<?php endif; ?>
						</label>
						<label>
							<?php if($data["location_state"] == 'DISABLE'): ?><input type="radio" name="state" value="0" checked="checked">禁用
							<?php else: ?>
								<input type="radio" name="state" value="0">禁用<?php endif; ?>
						</label>
					</div>
				</div>
			</div>
			<input type="hidden" name="id" value="<?php echo ($data["location_id"]); ?>">
			<div class="box-footer">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">保存</button>
					<button type="button" class="btn btn-default" rel-action="dialog-close">关闭</button>
				</div>
			</div>
		</form><?php endif; ?>
	<?php if(empty($data)): ?><div class="box-body">
		<div class="alert alert-error alert-dismissible">
			<h4><i class="icon fa fa-warning"></i>暂无记录</h4>
			参数有误
		</div>
	</div><?php endif; ?>
</div>

		
		</section>
	</body>
</html>