<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 短信管理</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- previewThumb -->
		<script src="<?php echo H('assets/plugins/previewThumb/jQuery.previewThumb.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5;">
		<section class="content">
		
		
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">短信详情</h3>
	</div>
	<div class="box-body table-responsive">
		<?php if(!empty($data)): ?><table class="table table-bordered table-hover table-striped">
				<tr>
					<td class="text-right col-sm-2">编号</td>
					<td><?php echo ($data["message_id"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">手机号</td>
					<td><?php echo ($data["message_mobile"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">模板</td>
					<td><?php echo ($data["template"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">内容</td>
					<td><?php echo ($data["message_content"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">请求时间</td>
					<td><?php echo ($data["message_create_time"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">完成时间</td>
					<td><?php echo ($data["message_time"]); ?></td>
				</tr>
				<tr>
					<td class="text-right">原始数据</td>
					<td><textarea class="form-control" rows="5" readonly><?php echo ($data["message_raw"]); ?></textarea></td>
				</tr>
				<tr>
					<td class="text-right">状态</td>
					<td>
						<?php echo ($data["state"]); ?>
					</td>
				</tr>
			</table>
			<div class="box-footer">
				<button type="button" class="btn btn-default" rel-action="dialog-close">关闭</button>
			</div>
		<?php else: ?>
			<div class="alert alert-warning alert-dismissible">
				<h4><i class="icon fa fa-warning"></i>暂无任何记录</h4>
				未查到任何符合要求的记录！
			</div><?php endif; ?>
		
	</div>
</div>

		
		</section>
	</body>
</html>