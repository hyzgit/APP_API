<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 方法</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
<script type="text/javascript">
$(function(){
	var wait = document.getElementById('wait'),href = document.getElementById('href').href;
	var interval = setInterval(function(){
		var time = --wait.innerHTML;
		if(time <= 0) {
			location.href = href;
			clearInterval(interval);
		};
	}, 1000);
});
</script>

		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- previewThumb -->
		<script src="<?php echo H('assets/plugins/previewThumb/jQuery.previewThumb.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5;">
		<section class="content">
		
		
<div class="callout callout-warning">
	<h4><?php echo($error); ?></h4>
	<p>页面自动 <a id="href" href="<?php echo($jumpUrl); ?>">跳转</a> 等待时间： <b id="wait"><?php echo($waitSecond); ?></b></p>
</div>

		
		</section>
	</body>
</html>