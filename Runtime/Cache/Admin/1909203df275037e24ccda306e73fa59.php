<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 查询支付信息</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- previewThumb -->
		<script src="<?php echo H('assets/plugins/previewThumb/jQuery.previewThumb.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-blue sidebar-mini" style="background-color:#ecf0f5;">
		<section class="content">
		
		
<div class="box box-danger">
	<div class="box-header with-border">
		<h3 class="box-title">查询支付信息</h3>
	</div>
		<form class="form-horizontal" method="post">
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-2 control-label"><span class="text-red">*</span>商户号</label>
					<div class="col-sm-10 radio">
						<label><input type="radio" name="mch_id" value="1294444501" checked="checked">支付方式1</label>
						<label><input type="radio" name="mch_id" value="1480730502">支付方式2</label>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">微信支付单号</label>
					<div class="col-sm-10">
						<!-- input type="text" class="form-control" name="transaction_id" placeholder="请输入微信支付单号" value="<?php echo ($data["transaction_id"]); ?>"  autocomplete="off"-->
						<input type="text" class="form-control" name="transaction_id" placeholder="请输入微信支付单号" value="4200000006201711134443298021"  autocomplete="off">
					</div>
				</div>
			</div>
			<div class="box-footer">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">查询</button>
					<button type="button" class="btn btn-default" rel-action="dialog-close">关闭</button>
				</div>
			</div>
		</form>
</div>

		
		</section>
	</body>
</html>