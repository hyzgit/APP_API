<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 合同管理</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<script type="text/javascript">
		if(window.console === undefined)
		{
			//console = {
				//log:function(info){return false;}
			//};
		}
		</script>
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
<link rel="stylesheet" type="text/css" href="<?php echo H('assets/plugins/datepicker/datepicker3.css');?>" media="all" />

		
		
<script type="text/javascript" src="<?php echo H('assets/plugins/datepicker/bootstrap-datepicker.js');?>"></script>

		
<script type="text/javascript">
$(function(){
	$('[rel-action=datepicker]').datepicker({
		format:'yyyy-mm-dd',
		todayBtn:true,
		todayHighlight:true,
		autoclose: true,
	});
});
</script>

		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-purple sidebar-mini">
	
		<div class="wrapper">
			<header class="main-header">
				<a href="<?php echo U('Index/index');?>" class="logo">
					<span class="logo-mini"><b>A</b>LT</span>
					<span class="logo-lg"><b>Admin</b>LTE</span>
				</a>
				<nav class="navbar navbar-static-top">
					<a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo H('assets/dist/img/user2-160x160.jpg');?>" class="user-image" alt="User Image">
									<span class="hidden-xs">profile <?php echo ($profile['manager_username']); ?></span>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="<?php echo U('Profile/info');?>"><i class="fa fa-user-secret"></i>个人中心</a></li>
									<li><a href="<?php echo U('Profile/password');?>"><i class="fa fa-unlock-alt"></i>修改密码</a></li>
									<li><a href="<?php echo U('Profile/task');?>"><i class="fa fa-tag"></i>我的任务</a></li>
									<li class="divider"></li>
									<li><a href="<?php echo U('Account/logout');?>"><i class="fa fa-sign-out"></i><span>退出</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<aside class="main-sidebar">
				<section class="sidebar">
					<ul class="sidebar-menu">
						<li class="header">MAIN NAVIGATION</li>
						<li class="treeview <?php echo active('Level,Role,Manager');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-dashboard"></i> <span>账户管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Level/page');?>"><a href="<?php echo U('Level/page');?>"><i class="fa fa-circle-o"></i>权限管理</a></li>
								<li class="<?php echo active('Role/page');?>"><a href="<?php echo U('Role/page');?>"><i class="fa fa-circle-o"></i>角色管理</a></li>
								<li class="<?php echo active('Manager/page');?>"><a href="<?php echo U('Manager/page');?>"><i class="fa fa-circle-o"></i>管理员管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Version');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>版本管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Version/page');?>"><a href="<?php echo U('Version/page');?>"><i class="fa fa-circle-o"></i>版本管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Type');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>业务管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">							
								<li class="<?php echo active('Type/page');?>"><a href="<?php echo U('Type/page');?>"><i class="fa fa-circle-o"></i>业务管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Contract');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>合同管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Contract/page');?>"><a href="<?php echo U('Contract/page');?>"><i class="fa fa-circle-o"></i>合同管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Sms');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>短信推送</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Sms/page');?>"><a href="<?php echo U('Sms/page');?>"><i class="fa fa-circle-o"></i>短信推送</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Qrcode');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>二维码生成</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Qrcode/page');?>"><a href="<?php echo U('Qrcode/page');?>"><i class="fa fa-circle-o"></i>二维码生成</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Commodity');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>商品关联信息</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Commodity/page');?>"><a href="<?php echo U('Commodity/page');?>"><i class="fa fa-circle-o"></i>商品关联信息</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Cancel');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>撤销记录管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Cancel/page');?>"><a href="<?php echo U('Cancel/page');?>"><i class="fa fa-circle-o"></i>撤销记录管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Refund');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>退款记录管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Refund/page');?>"><a href="<?php echo U('Refund/page');?>"><i class="fa fa-circle-o"></i>退款记录管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Relation');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>合同关联信息</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Relation/page');?>"><a href="<?php echo U('Relation/page');?>"><i class="fa fa-circle-o"></i>合同关联信息</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Location');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>商户定位管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Location/page');?>"><a href="<?php echo U('Location/page');?>"><i class="fa fa-circle-o"></i>商家定位管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Collection');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>信息采集管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Collection/page');?>"><a href="<?php echo U('Collection/page');?>"><i class="fa fa-circle-o"></i>信息采集管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Bank');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>银行信息管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Bank/page');?>"><a href="<?php echo U('Bank/page');?>"><i class="fa fa-circle-o"></i>银行信息管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Message');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>短信管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Message/page');?>"><a href="<?php echo U('Message/page');?>"><i class="fa fa-circle-o"></i>短信管理（漫道）</a></li>
							</ul>
						</li>
					</ul>
				</section>
			</aside>

			<div class="content-wrapper">
				<section class="content-header">
					<h1>
						合同管理
						<small>
							
							<a href="javascript:history.go(-1);">返回</a>
						</small>
					</h1>
					<ol class="breadcrumb">
						<li>
							<a href="#"><i class="fa fa-home"></i>后台首页</a>
						</li>
						<li><a href="#">合同管理</a></li>
						<li class="active">合同管理</li>
					</ol>
				</section>
				<section class="content">
					
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">记录筛选</h3>
	</div>
	<div class="box-body table-responsive">
		<form class="form-horizontal" method="get">
		<div class="box-body">
			<div class="form-group">
				<label class="col-sm-2 control-label">支付完成日期</label>
				<div class="col-sm-2">
					<input type="text" class="form-control" rel-action="datepicker" name="start_time" placeholder="请输入支付完成日期"  autocomplete="off">
				</div>
				<div class="col-sm-2">
					<input type="text" class="form-control" rel-action="datepicker" name="end_time" placeholder="请输入支付完成日期"  autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">姓名</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="name" value="<?php echo ($data["contract_name"]); ?>" placeholder="请输入姓名"  autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">身份证号</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="cardid" value="<?php echo ($data["contract_cardid"]); ?>" placeholder="请输入身份证号"  autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">合同编号</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="number" value="<?php echo ($data["contract_number"]); ?>" placeholder="请输入合同编号"  autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">微信支付单号</label>
				<div class="col-sm-4">
					<input type="text" class="form-control" name="transaction_id" value="<?php echo ($data["contract_transaction_id"]); ?>" placeholder="请输入微信支付单号"  autocomplete="off">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">支付状态</label>
				<div class="col-sm-10">
					<select class="form-control" rel-action="select" name="status">
						<option value="all">所有</option>
						<?php if(is_array($data["status_list"])): foreach($data["status_list"] as $key=>$item): ?><option value="<?php echo ($key); ?>"><?php echo ($item); ?></option><?php endforeach; endif; ?>
					</select>
				</div>
			</div>
		<div class="box-footer">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">查询</button>
			</div>
		</div>
		</form>
	</div>
</div>

				</section>
			</div>
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b> 2.3.3
				</div>
				<strong>Copyright &copy; 2016.</strong> All rights reserved.
			</footer>
		</div>
	</body>
</html>