<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 新增角色</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<script type="text/javascript">
		if(window.console === undefined)
		{
			//console = {
				//log:function(info){return false;}
			//};
		}
		</script>
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-purple sidebar-mini">
	
		<div class="wrapper">
			<header class="main-header">
				<a href="<?php echo U('Index/index');?>" class="logo">
					<span class="logo-mini"><b>A</b>LT</span>
					<span class="logo-lg"><b>Admin</b>LTE</span>
				</a>
				<nav class="navbar navbar-static-top">
					<a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo H('assets/dist/img/user2-160x160.jpg');?>" class="user-image" alt="User Image">
									<span class="hidden-xs">profile <?php echo ($profile['manager_username']); ?></span>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="<?php echo U('Profile/info');?>"><i class="fa fa-user-secret"></i>个人中心</a></li>
									<li><a href="<?php echo U('Profile/password');?>"><i class="fa fa-unlock-alt"></i>修改密码</a></li>
									<li><a href="<?php echo U('Profile/task');?>"><i class="fa fa-tag"></i>我的任务</a></li>
									<li class="divider"></li>
									<li><a href="<?php echo U('Account/logout');?>"><i class="fa fa-sign-out"></i><span>退出</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<aside class="main-sidebar">
				<section class="sidebar">
					<ul class="sidebar-menu">
						<li class="header">MAIN NAVIGATION</li>
						<li class="treeview <?php echo active('Level,Role,Manager');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-dashboard"></i> <span>账户管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Level/page');?>"><a href="<?php echo U('Level/page');?>"><i class="fa fa-circle-o"></i>权限管理</a></li>
								<li class="<?php echo active('Role/page');?>"><a href="<?php echo U('Role/page');?>"><i class="fa fa-circle-o"></i>角色管理</a></li>
								<li class="<?php echo active('Manager/page');?>"><a href="<?php echo U('Manager/page');?>"><i class="fa fa-circle-o"></i>管理员管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Version');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>版本管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Version/page');?>"><a href="<?php echo U('Version/page');?>"><i class="fa fa-circle-o"></i>版本管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Type');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>业务管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">							
								<li class="<?php echo active('Type/page');?>"><a href="<?php echo U('Type/page');?>"><i class="fa fa-circle-o"></i>业务管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Contract');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>合同管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Contract/page');?>"><a href="<?php echo U('Contract/page');?>"><i class="fa fa-circle-o"></i>合同管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Sms');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>短信推送</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Sms/page');?>"><a href="<?php echo U('Sms/page');?>"><i class="fa fa-circle-o"></i>短信推送</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Qrcode');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>二维码生成</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Qrcode/page');?>"><a href="<?php echo U('Qrcode/page');?>"><i class="fa fa-circle-o"></i>二维码生成</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Commodity');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>商品关联信息</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Commodity/page');?>"><a href="<?php echo U('Commodity/page');?>"><i class="fa fa-circle-o"></i>商品关联信息</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Cancel');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>撤销记录管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Cancel/page');?>"><a href="<?php echo U('Cancel/page');?>"><i class="fa fa-circle-o"></i>撤销记录管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Refund');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>退款记录管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Refund/page');?>"><a href="<?php echo U('Refund/page');?>"><i class="fa fa-circle-o"></i>退款记录管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Relation');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>合同关联信息</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Relation/page');?>"><a href="<?php echo U('Relation/page');?>"><i class="fa fa-circle-o"></i>合同关联信息</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Location');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>商户定位管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Location/page');?>"><a href="<?php echo U('Location/page');?>"><i class="fa fa-circle-o"></i>商家定位管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Collection');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>信息采集管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Collection/page');?>"><a href="<?php echo U('Collection/page');?>"><i class="fa fa-circle-o"></i>信息采集管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Bank');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>银行信息管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Bank/page');?>"><a href="<?php echo U('Bank/page');?>"><i class="fa fa-circle-o"></i>银行信息管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Message');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>短信管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Message/page');?>"><a href="<?php echo U('Message/page');?>"><i class="fa fa-circle-o"></i>短信管理（漫道）</a></li>
							</ul>
						</li>
					</ul>
				</section>
			</aside>

			<div class="content-wrapper">
				<section class="content-header">
					<h1>
						新增角色
						<small>
							
							<a href="javascript:history.go(-1);">返回</a>
						</small>
					</h1>
					<ol class="breadcrumb">
						<li>
							<a href="#"><i class="fa fa-home"></i>后台首页</a>
						</li>
						<li><a href="#">账户管理</a></li>
						<li class="active">角色管理</li>
					</ol>
				</section>
				<section class="content">
					
<div class="row">
	<form class="form-horizontal" action="<?php echo U('save');?>" method="post" rel-action="form">
	<div class="col-md-3">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3 class="box-title">选择权限</h3>
			</div>
			<div class="box-body table-responsive">
				<table class="table table-striped table-condensed table-bordered">
					<?php if(is_array($level)): $i = 0; $__LIST__ = $level;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?><tr>
							<td><strong><?php echo ($item["level_name"]); ?></strong></td>
						</tr>
						<?php if(is_array($item["level"])): $i = 0; $__LIST__ = $item["level"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$items): $mod = ($i % 2 );++$i;?><tr>
							<td>
								<label class="font-light">
								<?php if($items["level_default"] == 1): ?><input name="raw[<?php echo ($items["level_id"]); ?>]" value="<?php echo ($items["level_data"]); ?>" type="checkbox" checked><?php echo ($items["level_name"]); ?> - <?php echo ($items["level_data"]); ?>
								<?php else: ?>
									<input name="raw[<?php echo ($items["level_id"]); ?>]" value="<?php echo ($items["level_data"]); ?>" type="checkbox"><?php echo ($items["level_name"]); ?> - <?php echo ($items["level_data"]); endif; ?>
								</label>
							</td>
						</tr><?php endforeach; endif; else: echo "" ;endif; endforeach; endif; else: echo "" ;endif; ?>
				</table>
			</div>	
		</div>
	</div>
	<div class="col-md-9">
		<div class="box box-danger">
			<div class="box-header with-border">
				<h3 class="box-title">角色信息</h3>
			</div>
			<div class="box-body">
				<div class="form-group">
					<label class="col-sm-2 control-label"><span class="text-red">*</span>标题</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="name" value="<?php echo ($data["role_name"]); ?>" placeholder="请输入标题"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">描述</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="desc" value="<?php echo ($data["role_desc"]); ?>" placeholder="请输入描述"  autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">排序</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" name="order" value="<?php echo ($data["role_order"]); ?>" placeholder="请填写整数，数字越小越靠前" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">状态</label>
					<div class="col-sm-10 radio">
						<label>
							<?php if($data["role_state"] == 1): ?><input type="radio" name="state" value="1" checked="checked">正常
							<?php else: ?>
								<input type="radio" name="state" value="1">正常<?php endif; ?>
						</label>
						<label>
							<?php if($data["role_state"] == 0): ?><input type="radio" name="state" value="0" checked="checked">锁定
							<?php else: ?>
								<input type="radio" name="state" value="0">锁定<?php endif; ?>
						</label>
					</div>
				</div>
			</div>
			<input type="hidden" name="id" value="<?php echo ($data["role_id"]); ?>">
			<div class="box-footer">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">保存</button>
					<button type="reset" class="btn btn-default">重置</button>
					<button type="button" class="btn btn-default" rel-action="cancel">取消</button>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>

				</section>
			</div>
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b> 2.3.3
				</div>
				<strong>Copyright &copy; 2016.</strong> All rights reserved.
			</footer>
		</div>
	</body>
</html>