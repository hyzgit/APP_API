<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 系统登录</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			Login <b>Admin</b> Manage
		</div>
		<div class="login-box-body">
			<?php if(empty($message)): ?><p class="login-box-msg">管理员登录</p>
			<?php else: ?>
				<p class="login-box-msg text-red"><?php echo ($message); ?></p><?php endif; ?>
			<form action="<?php echo U('login');?>" method="post" rel-action="form">
				<div class="form-group has-feedback">
					<input type="text" name="name" value="" class="form-control" placeholder="请输入用户名" autocomplete="off">
					<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="pass" value="" class="form-control" placeholder="请输入密码">
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<button type="submit" class="btn btn-primary btn-block btn-flat">登录</button>
				</div>
			</form>
			<!--div class="row">
				<div class="col-xs-8">
					<a href="#">注册账户</a>
				</div>
				<div class="col-xs-4 text-right">
					<a href="#">找回密码</a>
				</div>
			</div-->
		</div>
	</div>
</body>

</html>