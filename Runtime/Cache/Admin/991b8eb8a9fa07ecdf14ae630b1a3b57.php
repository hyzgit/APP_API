<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?php echo C('APP_NAME');?> | 商品关联信息列表</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<script type="text/javascript">
		if(window.console === undefined)
		{
			//console = {
				//log:function(info){return false;}
			//};
		}
		</script>
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo H('assets/bootstrap/css/bootstrap.min.css');?>">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/font-awesome/4.5.0/css/font-awesome.min.css');?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/ionicons/2.0.1/css/ionicons.min.css');?>">
		<!-- jQuery 2.2.0 -->
		<script src="<?php echo H('assets/plugins/jQuery/jQuery-2.2.0.min.js');?>"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo H('assets/bootstrap/js/bootstrap.min.js');?>"></script>
		<!-- layer -->
		<script src="<?php echo H('assets/plugins/layer/layer.js');?>"></script>
		
		
		
		
		<!-- select2 -->
		<link rel="stylesheet" href="<?php echo H('assets/plugins/select2/select2.min.css');?>">
		<script src="<?php echo H('assets/plugins/select2/select2.full.min.js');?>"></script>
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/AdminLTE.min.css');?>">
		<!-- AdminLTE Skins -->
		<link rel="stylesheet" href="<?php echo H('assets/dist/css/skins/_all-skins.min.css');?>">
		<!-- AdminLTE App -->
		<script src="<?php echo H('assets/dist/js/app.min.js');?>"></script>
		<script src="<?php echo H('assets/dist/js/admin.js');?>"></script>
	</head>
	<body class="hold-transition skin-purple sidebar-mini">
	
		<div class="wrapper">
			<header class="main-header">
				<a href="<?php echo U('Index/index');?>" class="logo">
					<span class="logo-mini"><b>A</b>LT</span>
					<span class="logo-lg"><b>Admin</b>LTE</span>
				</a>
				<nav class="navbar navbar-static-top">
					<a href="javascript:void(0);" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
					</a>
					<div class="navbar-custom-menu">
						<ul class="nav navbar-nav">
							<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo H('assets/dist/img/user2-160x160.jpg');?>" class="user-image" alt="User Image">
									<span class="hidden-xs">profile <?php echo ($profile['manager_username']); ?></span>
								</a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="<?php echo U('Profile/info');?>"><i class="fa fa-user-secret"></i>个人中心</a></li>
									<li><a href="<?php echo U('Profile/password');?>"><i class="fa fa-unlock-alt"></i>修改密码</a></li>
									<li><a href="<?php echo U('Profile/task');?>"><i class="fa fa-tag"></i>我的任务</a></li>
									<li class="divider"></li>
									<li><a href="<?php echo U('Account/logout');?>"><i class="fa fa-sign-out"></i><span>退出</span></a></li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>
			<aside class="main-sidebar">
				<section class="sidebar">
					<ul class="sidebar-menu">
						<li class="header">MAIN NAVIGATION</li>
						<li class="treeview <?php echo active('Level,Role,Manager');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-dashboard"></i> <span>账户管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Level/page');?>"><a href="<?php echo U('Level/page');?>"><i class="fa fa-circle-o"></i>权限管理</a></li>
								<li class="<?php echo active('Role/page');?>"><a href="<?php echo U('Role/page');?>"><i class="fa fa-circle-o"></i>角色管理</a></li>
								<li class="<?php echo active('Manager/page');?>"><a href="<?php echo U('Manager/page');?>"><i class="fa fa-circle-o"></i>管理员管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Version');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>版本管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Version/page');?>"><a href="<?php echo U('Version/page');?>"><i class="fa fa-circle-o"></i>版本管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Type');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>业务管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">							
								<li class="<?php echo active('Type/page');?>"><a href="<?php echo U('Type/page');?>"><i class="fa fa-circle-o"></i>业务管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Contract');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>合同管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Contract/page');?>"><a href="<?php echo U('Contract/page');?>"><i class="fa fa-circle-o"></i>合同管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Sms');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>短信推送</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Sms/page');?>"><a href="<?php echo U('Sms/page');?>"><i class="fa fa-circle-o"></i>短信推送</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Qrcode');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>二维码生成</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Qrcode/page');?>"><a href="<?php echo U('Qrcode/page');?>"><i class="fa fa-circle-o"></i>二维码生成</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Commodity');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>商品关联信息</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Commodity/page');?>"><a href="<?php echo U('Commodity/page');?>"><i class="fa fa-circle-o"></i>商品关联信息</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Cancel');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>撤销记录管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Cancel/page');?>"><a href="<?php echo U('Cancel/page');?>"><i class="fa fa-circle-o"></i>撤销记录管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Refund');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>退款记录管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Refund/page');?>"><a href="<?php echo U('Refund/page');?>"><i class="fa fa-circle-o"></i>退款记录管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Relation');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>合同关联信息</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Relation/page');?>"><a href="<?php echo U('Relation/page');?>"><i class="fa fa-circle-o"></i>合同关联信息</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Location');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>商户定位管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Location/page');?>"><a href="<?php echo U('Location/page');?>"><i class="fa fa-circle-o"></i>商家定位管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Collection');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>信息采集管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Collection/page');?>"><a href="<?php echo U('Collection/page');?>"><i class="fa fa-circle-o"></i>信息采集管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Bank');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>银行信息管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Bank/page');?>"><a href="<?php echo U('Bank/page');?>"><i class="fa fa-circle-o"></i>银行信息管理</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo active('Message');?>">
							<a href="javascript:void(0);">
								<i class="fa fa-book"></i> <span>短信管理</span> <i class="fa fa-angle-left pull-right"></i>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo active('Message/page');?>"><a href="<?php echo U('Message/page');?>"><i class="fa fa-circle-o"></i>短信管理（漫道）</a></li>
							</ul>
						</li>
					</ul>
				</section>
			</aside>

			<div class="content-wrapper">
				<section class="content-header">
					<h1>
						商品关联信息列表
						<small>
							
							<a href="javascript:history.go(-1);">返回</a>
						</small>
					</h1>
					<ol class="breadcrumb">
						<li>
							<a href="#"><i class="fa fa-home"></i>后台首页</a>
						</li>
						<li><a href="#">商品关联信息</a></li>
						<li class="active">商品关联信息列表</li>
					</ol>
				</section>
				<section class="content">
					
<div class="box box-primary">
	<div class="box-header with-border">
		<h3 class="box-title">商品关联信息列表</h3>
		<div class="box-tools">
			<button type="button" class="btn btn-default margin-r-5" rel-url="<?php echo U('form');?>" rel-action="dialog">
				<i class="fa fa-file-text"></i>
				新增
			</button>
		</div>
	</div>
	<div class="box-body table-responsive">
		<table class="table table-bordered table-striped table-hover">
			<tbody>
				<?php if(!empty($data)): ?><tr>
					<th class="text-center">编号</th>
					<th class="text-center">商品类型</th>
					<th class="text-center">商品总金额</th>
					<th class="text-center">分期数</th>
					<th class="text-center">商品首付值</th>
					<th class="text-center">商品直降金额</th>
					<th class="text-center">状态</th>
					<th class="text-center">操作</th>
				</tr>
				<?php if(is_array($data)): $i = 0; $__LIST__ = $data;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$items): $mod = ($i % 2 );++$i;?><tr>
					<td class="text-center"><?php echo ($items["commodity_id"]); ?></td>
					<td><?php echo ($items["commodity_commodity_type"]); ?></td>
					<td class="text-center"><?php echo ($items["total_money"]); ?></td>
					<td class="text-center"><?php echo ($items["commodity_stages_num"]); ?></td>
					<td class="text-center"><?php echo ($items["down_payment"]); ?></td>
					<td class="text-center"><?php echo ($items["reduce_price"]); ?></td>
					<td class="text-center"><?php echo ($items["status"]); ?></td>
					<td class="text-center">
						<a href="<?php echo ($items["edit"]); ?>" rel-action="dialog">编辑</a>
						<a href="<?php echo ($items["delete"]); ?>" rel-action="delete">删除</a>
					</td>
				</tr><?php endforeach; endif; else: echo "" ;endif; ?>
				<?php else: ?>
					<tr><td>暂无符合条件记录</td></tr><?php endif; ?>
			</tbody>
		</table>
	</div>
	<?php if(!empty($data)): ?><div class="box-footer clearfix">
		<?php echo ($page); ?>
	</div><?php endif; ?>
</div>

				</section>
			</div>
			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b> 2.3.3
				</div>
				<strong>Copyright &copy; 2016.</strong> All rights reserved.
			</footer>
		</div>
	</body>
</html>